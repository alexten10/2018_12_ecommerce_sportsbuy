<?php
/**
 * Dealing with TAX
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------


/**
 * Get GST
 *
 * @param  price: a price endured TAX
 * @return  GST value
 */
function calculateGST($price) {
  return $price * GST_RATE;
}


/**
 * Get PST
 *
 * @param  price: a price endured TAX
 * @return  PST value
 */
function calculatePST($price, $province) {
  global $provincePST;
  global $provinces;
  
  if (in_array($province, $provinces)) {
    return $price * $provincePST[$province];
  } else {
    // Indicates something goes wrong here when province is not found.
    return -1;
  }
}


/**
 * Get calculateShippingCost
 *
 * @param  price: a price endured TAX
 * @return  GST value
 */
function calculateShippingCost($price) {
  return $price * SHIPPING_COST_RATE;
}



?>