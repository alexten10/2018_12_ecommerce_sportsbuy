<!doctype html>

<!--

/**
 * External header include for admin
 * @header.inc.php
 * @course Ecommerece Project, WDD 2018 Jan
 * @author Amarjeet Sharma <Sharmajeetamar31@gmail.com>
 * @created_at 2018-12-01
 **/

-->

<html lang="en">
  <head>
    <title><?=$title?></title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css"  href="styles/desktop.css" />
    
    
    
<!-- Javascript for compatibility with Internet Explorer 8 or lower -->

<!--[if LTE IE 8]>

<h2>Please upgrade your browser!</h2>

  <script> 
      document.createElement('header');
      document.createElement('nav');
      document.createElement('footer');
      document.createElement('article');
      document.createElement('section');
      document.createElement('main');
  </script>

  <style type="text/css">
      header,
      footer,
      main,
      nav
      {
        display: block;
      }
  </style>

<![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

</head>
<body>
