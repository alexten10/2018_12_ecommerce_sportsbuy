<?php
?><!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <!-- favicon for sports buy -->
  <link rel="shortcut icon" href="images/icons/favicon_sportsbuy.png" type="image/png" />
  <title><?php echo $title ?></title>
  
  <!-- #################  START everything for the slider ###############################-->
  <link rel="stylesheet" href="styles/jquery.bxslider.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
  <script src="styles/jquery.bxslider.min.js"></script>
  <script>
    //this plugin is touch friendly, user can swipe through the images
    $(document).ready(function(){
      $('.slider').bxSlider({
        //mode: 'fade', //slide mode is fade, not as horizontal slide by default
        //captions: 'true', //adds captions(text at the bottom of slides) taken from img alt=
        auto: 'true', //slides automatically
        pause: '3000', //pause between slides is 3 seconds
        keyboardEnabled: 'true', //users can use keyboard arrows to change images
        autoControls: 'true', //adds play/pause controls(buttons)
      });//END bxSlider
    });//END .ready
  </script>
  <!-- ###################  END everything for the slider ###############################-->
  
  <link rel="stylesheet" type="text/css" href="styles/desktop.css">
</head>


<body>
  <div id="wrapper">


    <!-- ############################################################################-->
    <!-- ###########################  START header  #################################-->
    <div id="header">
            
      <div id="logo">
        <a href="index.php"><img src="images/logo/sportslogo.png" alt="Logo" /></a>
      </div><!-- logo div ends-->

      <div id="nav">
        <ul>
          <li><a href="index.php" <?php if($active_page == 'home') :?> class="active_page" <?php endif; ?> >Home</a></li>
          <li><a href="products.php" <?php if($active_page == 'products') :?> class="active_page" <?php endif; ?> >Products</a></li>
          <li><a href="about_us.php" <?php if($active_page == 'about_us') :?> class="active_page" <?php endif; ?> >About Us</a></li>
          <li><a href="contact_us.php" <?php if($active_page == 'contact_us') :?> class="active_page" <?php endif; ?> >Contact Us</a></li>
        </ul>
      </div><!-- nav div ends-->


      <!-- ######### START hamburger menu (main nav menu for mobile devices) ########-->
      <!-- by Erik Terwann "pure css hamburger fold-out menu", no javascript-->
      <!-- https://codepen.io/erikterwan/pen/EVzeRP -->
      <nav><!--originally was <nav role="navigation">-->
        <div id="menuToggle">
          <input type="checkbox" /> <!--fake/hidden checkbox is used as a click reciever -->
          
          <!--lines of hamburger icon -->
          <span></span>
          <span></span>
          <span></span>
          
          <ul id="menu">
            <li><a href="index.php" <?php if($active_page == 'home') :?> class="active_page" <?php endif; ?> >Home</a></li>
            <li><a href="products.php" <?php if($active_page == 'products') :?> class="active_page" <?php endif; ?> >Products</a></li>
            <li><a href="about_us.php" <?php if($active_page == 'about_us') :?> class="active_page" <?php endif; ?> >About Us</a></li>
            <li><a href="contact_us.php" <?php if($active_page == 'contact_us') :?> class="active_page" <?php endif; ?> >Contact Us</a></li>
          </ul>
        </div>
      </nav>
      <!-- ######### END hamburger menu ########-->


      <!-- search field -->
      <div id="search">
        <form action="products.php" 
              id="search_form"
              method="get" 
              novalidate="novalidate" 
              autocomplete="off">
              
          <p>
            <input type="text"
                 id="searchbox"
                 class="searchtext" 
                 name="keyword" 
                 maxlength="255" 
                 placeholder="Search" /><!-- name="keyword" is used for $_GET['keyword'] -->
            <input id="search_button" type=image src="images/icons/header_search_magnifier.png" alt="search" />
          </p>
        </form>
      </div><!--END div.search -->


      <!-- utility navigation menu -->
      <div id="utility_nav">
        <?php if(!isset($_SESSION['logged_in'])) : ?>
          <ul>
            <li><a href="login.php" <?php if($active_page == 'login') :?> class="active_page" <?php endif; ?> >Log In &nbsp;<span class="util_nav_pipe">|</span></a></li>
            <li><a href="register.php" <?php if($active_page == 'register') :?> class="active_page" <?php endif; ?> >Register &nbsp;<span class="util_nav_pipe">|</span></a></li>
            <li><a href="index.php?p=checkout.php" <?php if($active_page == 'cart') :?> class="active_page" <?php endif; ?> ><img src="images/icons/header_utility_nav_cart.png" alt="cart" /></a></li>
          </ul>
        <?php else : ?>
          <ul>
            <li><a href="logout.php" <?php if($active_page == 'logout') :?> class="active_page" <?php endif; ?> >Log Out &nbsp;<span class="util_nav_pipe">|</span></a></li>
            <li><a href="profile.php" <?php if($active_page == 'profile') :?> class="active_page" <?php endif; ?> >Profile &nbsp;<span class="util_nav_pipe">|</span></a></li>
            <li><a href="index.php?p=checkout.php" <?php if($active_page == 'cart') :?> class="active_page" <?php endif; ?> ><img src="images/icons/header_utility_nav_cart.png" alt="cart" /></a></li>
          </ul>
        <?php endif; ?>
      </div><!-- utility div ends-->
      
    </div><!-- header ends -->
    <!-- ###########################  END header  #################################-->
    <!-- ############################################################################-->

