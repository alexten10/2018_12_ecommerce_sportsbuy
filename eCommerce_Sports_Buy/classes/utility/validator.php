<?php


/**
 * Admin Products
 * @validator.php
 * @course Ecommerce Project, WDD 2018 Jan
 * @author Amarjeet Sharma <Sharmajeetamar31@gmail.com>
 * @created_at 2018-12-01
 **/


namespace classes\utility;


class validator
{
  // our errors array
  private $errors = [];
  
  
  
  
  
  ############################  GENERAL VALIDATION    ###########################################
  
  /**
  *validate required fields in $_POST
  *@param String $field_name - name of field to validate
  */
  public function required($field_name)
  {
    $label = strtoupper(str_replace('_', ' ', $field_name));//all string is in uppercase
    if(empty($_POST[$field_name])) {
      $this->errors[$field_name] = "'{$label}' is a required field";
    }//END if
  }//END function required()
  
  
  
  /**
  *validate fields in $_POST for legal characters
  *@param String $field_name - name of field to validate
  * used to validate first_name, last_name, billing_city, shipping_city in customers table
  * used to validate, category_name, brand_name in other tables
  */
  public function validateForSymbols($field_name)
  {
    $label = ucwords(str_replace('_', ' ', $field_name));//first letters of each word is uppercase
    $pattern = '/^[A-z]([A-z\,\.\'\-\s]?){1,99}$/'; //length is allowed between 1 and 99 chars
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must contain only legal names in English";
    }//END if
  }//END function validateForSymblols()
  
  ############################  END GENERAL VALIDATION    #########################################
  
  
  
  /**
  *validate fields in $_POST for legal characters
  *@param String $field_name - name of field to validate
  * validate brand names, product names
  */
  public function validateForBrand($field_name)
  {
    $label = ucwords(str_replace('_', ' ', $field_name));//first letters of each word is uppercase
    $pattern = '/^[A-z0-9]([A-z\,\.\'\-\s]?){1,99}$/'; //length is allowed between 1 and 99 chars
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must contain only legal names in English";
    }//END if
  }//END function validateForSymblols()
  
  
  
  
  ///////////////////////////   NEW CUSTOMER REGISTRATION VALIDATION    //////////////////////////
  
  /**
  *validate email field in $_POST for proper format
  *@param String $field_name - name of field to validate
  */
  public function validateEmail($field_name)
  {
    if(!filter_var(($_POST[$field_name]), FILTER_VALIDATE_EMAIL)) {
      $this->errors[$field_name] = "Please enter a valid email address";
    }//END if
  }//END function validateEmail()
  
  
  
  /**
  *validate phone_number field in $_POST for North American format
  *@param String $field_name - name of field to validate
  * accepts formats:
  * 2041112233
  * +1 204 111-1111
  * + 1 204 111-1111
  * 204.111.1111
  * 204-111-1111
  * +1-204-111-1111
  */
  public function validatePhone($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^\s?\+?\s?[0-9]?[\-\.\s]?\(?([0-9]{3})\)?[\-\.\s]?([0-9]{3})[\-\.\s]?([0-9]{4})\s?$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must contain valid phone number, eg. '+1 204 111-1111'";
    }//END if
  }//END function validatePhone()
  
  
  
  /**
  *validate billing_street, shipping_street fields in $_POST for proper format
  *@param String $field_name - name of field to validate
  * accepts numbers and letters, commas, dots, single quotes, dashes, spaces
  * max length is 255 chars (1char for the first [] + max 254 for the second [])
  */
  public function validateStreetSymbols($field_name)
  {
    $label = ucfirst($field_name);//field name ("street") has nothing to replace
    $pattern = '/^[A-z0-9][A-z0-9\,\.\'\-\s]{1,254}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must contain only legal street number and names in English";
    }//END if
  }//END function validateStreetSymbols()
  
  
  
  /**
  *validate billing_postal_code, shipping_postal_code fields in $_POST for Canadian format
  *@param String $field_name - name of field to validate
  */
  public function validatePostalCode($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[A-z][0-9][A-z]\s?[0-9][A-z][0-9]$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must contain valid Canadian postal code, eg. 'A1A 1A1'";
    }//END if
  }//END function validatePostalCode()
  
  
  
  /**
  *validate password field in $_POST for maximum length
  *@param String $field_name - name of field to validate
  *@param int $max_length - number of max length
  */
  function maxLength($field_name, $max_length)
  {
    $label = ucfirst($field_name);
    if(strlen($_POST[$field_name]) > $max_length) {
      $this->errors[$field_name] = "'{$label}' field must be at maximum {$max_length} characters long";
    }//END if
  }//END function maxLength()
    
  
  
  /**
  *validate password field in $_POST for minimum length
  *@param String $field_name - name of field to validate
  *@param int $min_length - number of min length
  */
  function minLength($field_name, $min_length)
  {
    $label = ucfirst($field_name);
    if(strlen($_POST[$field_name]) < $min_length) {
      $this->errors[$field_name] = "'{$label}' field must be at least {$min_length} characters long";
    }//END if
  }//END function minLength()
    
  
  
  /**
  *validate password field in $_POST for password strength 
  *@param String $field_name - name of field to validate
  */
  public function validatePasswordStrength($field_name)
  {
    $label = ucfirst($field_name);
    // '<' and '>' are excluded form allowed special characters
    $pattern = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[\!\@\#\$\%\^\&\*\(\)\-\_\=\+\/\\\|\}\{\[\]\'\"\:\;\?\.\,\`\~])/'; 
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must contain at least 1 upper case letter, 1 lower case letter,
                                    1 digit, 1 spcial character (except <strong><</strong> and <strong>></strong>)";
    }//END if
  }//END function validatePasswordStrength()
  
  
  
  /**
  *validate password and password_confirm fields in $_POST to match each other
  *@param String $field_name - password field
  *@param string $compare_field - password_confirm field
  */
  public function passwordsMatch($field_name, $compare_field)
  {
    if($_POST[$field_name] != $_POST[$compare_field]) {
      $this->errors[$field_name] = "Passwords do not match! Please enter again.";
    }//END if
  }//END function passwordsMatch()
  
  ///////////////////////////   END NEW CUSTOMER REGISTRATION VALIDATION    //////////////////////////
  
  
  
  
  
  
  
  ################  CREDIT CARD VALIDATION ################################################################
  
  /**
  *validate credit_card field in $_POST for 16 digits number
  *@param INT $field_name - name of field to validate
  */
  public function validateCreditCard($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[0-9]{16}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must have 16 digit card number without spaces";
    }//END if
  }//END function validateCreditCard()
  
  
  
  /**
  *validate Card Expiry field in $_POST for 4 digits
  *@param INT $field_name - name of field to validate
  */
  public function validateCardExpiry($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[0-9]{4}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must have 4 digit card expiry date without spaces";
    }//END if
  }//END function validateCardExpiry()
  
  
  /**
  *validate CVV (credit card cvv) field in $_POST for 3 digits
  *@param INT $field_name - name of field to validate
  */
  public function validateCvv($field_name)
  {
    $label = strtoupper(str_replace('_', ' ', $field_name));
    $pattern = '/^[0-9]{3}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must have 3 digit card CVV";
    }//END if
  }//END function validateCvv()
  
  ####################   END CREDIT CARD VALIDATION  ####################################################################
  
  
  
  
  
  
  
  
  
  ///////////////////    NEW PRODUCT VALIDATION        /////////////////////////////////////////////////////////////////
  
  /**
   *validate weight field in $POST for float number
   *@param string $field_name - name of field to validate
   *accepts 0.1; 0.12; 333.22; 55555
   */
  public function validateWeight($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[0-9]{1,6}?$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must be in grams, no more than 999999";
    }//END if
  }//END validateWeight
  
  
  
  /**
   *validate SKU field in $POST for float number
   *@param string $field_name - name of field to validate
   *accepts integers between 2 and 20 digits long
   */
  public function validateSku($field_name)
  {
    $label = strtoupper(str_replace('_', ' ', $field_name));
    $pattern = '/^[0-9]{2,10}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must be between 2 and 10 digits long";
    }//END if
  }//END validateSku
  
  
  
  /**
  *validate inputs in $_POST for general text, except < > characters
  *@param String $field_name - name of field to validate
  * validate description textarea, instruction textarea with max length of 5000 characters
  */
  public function validateGeneralText($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[^\<\>]{1,5000}$/';//< and > are not allowed
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field cannot have <strong><</strong> and <strong>></strong> characters";
    }//END if
  }//END function validateGeneralText()
  
  
  
  /**
  *validate price field in $_POST for price format
  *@param String $field_name - name of field to validate
  *accepts 3, 3.00, 0.99 etc
  */
  public function validatePrice($field_name)
  {
    $label = ucfirst(str_replace('_', ' ', $field_name));
    $pattern = '/^[0-9]{1,4}(\.[0-9]{2})?$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field can have only price format numbers. max ex.: 9999.18";
    }//END if
  }//END function validatePrice()
  
  
  
  /**
  *validate units_in_stock field in $_POST for integer
  *@param String $field_name - name of field to validate
  */
  public function validateUnitsInStock($field_name)
  {
    $label = ucwords(str_replace('_', ' ', $field_name));
    $pattern = '/^[0-9]{5}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field can have only up to 5 digits";
    }//END if
  }//END function validateUnitsInStock()


/**
  *validate field for integer
  *@param String $field_name - name of field to validate
  */
  public function validateNumber($field_name)
  {
    $label = ucwords(str_replace('_', ' ', $field_name));
    $pattern = '/^[0-9]+$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field can have only numbers";
    }//END if
  }//END function validateUnitsInStock()
  
  
  /**
  *validate field for integer with limit
  *@param String $field_name - name of field to validate
  */
  public function validateUnits($field_name)
  {
    $label = ucwords(str_replace('_', ' ', $field_name));
    $pattern = '/^[0-9]{2,10}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field must be between 2 and 10 digits";
    }//END if
  }//END function validateUnitsInStock()
  
  
  /**
  *validate image name in $_POST for allowed characters
  *@param String $field_name - name of field to validate
  * validate filenames with extensions (ex. image_1.jpg)
  */
  public function validateImage($field_name)
  {
    $label = ucwords(str_replace('_', ' ', $field_name));
    $pattern = '/^[A-z0-9\.\_]+[\.][a-z]{3,4}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field can have only letters, numbers, underscores, and periods. ex.: coffee_1.jpg";
    }//END if
  }//END function validateImage()
  
  ///////////////////    END NEW PRODUCT VALIDATION        /////////////////////////////////////////////////////////////////
  
    /**
  *validate billing apartment number, appartment fields in $_POST for proper format
  *@param String $field_name - name of field to validate
  * accepts numbers and letters
  * max length is 8 chars
  */
  public function validateAppartmentNumber($field_name)
  {
    $label = ucfirst($field_name);
    $pattern = '/^[A-z0-9]{1,8}$/';
    if(preg_match($pattern, $_POST[$field_name], $matches) === 0) { //if no matches found
      $this->errors[$field_name] = "'{$label}' field can contain only 8 characters (letters and numbers)";
    }//END if
  }//END function validateStreetSymbols()

  
  
  
  
  
  
  /**
  *@return Array errors - array of error messages
  */
  public function errors()
  {
    return $this->errors;
  }//END function errors()

  
  
  
}//END class Validator


























