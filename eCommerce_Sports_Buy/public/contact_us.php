<?php

require '../config.php';
require '../functions.php';

$title = 'Contact Us';
$active_page = 'contact_us';

?>

<?php include '../includes/header.inc.php'; ?>

    <!-- container starts -->
    <div id="container">
      
      <!-- content starts -->
      <div id="content">

        <!-- contact_info starts -->
        <div id="contact_info">
          <h1>Online Store Customer Service</h1>
          <p>We welcome and value your feedback. If you have any comments or questions regarding Sports Buy, please fill out the online feedback form below and we will get back to you within 2 business days.</p>
        
          <p><strong>Hours of Operation</strong></p>
            <p>Monday - Friday: 8 AM - 9 PM (ET)</p>
            <p>Saturday - Sunday: 9 AM - 5 PM (ET)</p>
        </div><!-- contact_info ends -->


        <!-- contact_form starts -->
        <div id="contact_form">

          <!-- form_box starts -->
          <div id="form_box">

            <!-- form starts -->
            <form method="post" action="#"
            id="personal_details"
            name="personal_details"
            autocomplete="on">

            <fieldset>
              <legend>Stay in touch for Newsletter</legend>
              <div>
                <p>
                  <label for="first_name">
                    <span class="required">*</span>First Name</label>
                  <input type="text" 
                         name="first_name" 
                         id="first_name"
                         maxlength="25"
                         size="30"
                         placeholder="Type your First Name"
                         required />   
                </p>

                 <p>
                  <label for="last_name">
                    <span class="required">*</span>Last Name</label>
                  <input type="text" 
                         name="last_name" 
                         id="last_name"
                         maxlength="25"
                         size="30"
                         placeholder="Type your Last Name"
                         required />   
                </p>

                <p>
                  <label for="emailid"><span class="required">*</span>Email id</label>
                  <input type="email" 
                         name="emailid" 
                         id="emailid"
                         maxlength="20"
                         size="20"
                         placeholder="Type your Email Address" 
                         required />   
                </p>

                <p>
                  <input type="submit" 
                         name="submit" 
                         id="submit" 
                         value="Submit" />
                </p>
              </div>
            </fieldset>
            </form><!-- form ends -->


          </div>
        </div>

        <div class="clear_both"></div>

        <div id="contact_us_map">
          <img src="images/map/map.png" alt="map_image" />
        </div>


      </div><!-- content ends -->
    

    </div><!-- Container ends -->


<?php include '../includes/footer.inc.php'; ?>






















    