<?php
/**
 * Main MVC page
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Retrieve the configuration
require('../config.php');

/**
 * Redirect to a login page when user goes to a invalid place
 *
 * @return  none
 */
function homepage() {
  // There is no valid link
  header("Location: home.php");
  exit();      
}

/**
 * Redirect to a login page when user goes to a invalid place
 *
 * @return  none
 */
function notfoundpage() {
  // There is no valid link
  header("Location: 404.html");
  exit();      
}


/**
 * Generate a randomized hash number
 *
 * @return  none
 */
function getRandomHashNumber() {
  // See http://www.php.net/mt_rand for why I choose this function to get a better randomized number
  return md5($_SERVER['REMOTE_ADDR'] . uniqid(mt_rand(), true));
}

// Just create csrf token for the first time
if (isset($_SESSION['csrf'])) {
  // Get csrf key back from the session
  $csrfkey = $_SESSION['csrf'];
} else {
  //Generate a secret key
  $csrfkey = getRandomHashNumber();
  $_SESSION['csrf'] = $csrfkey;
}

// Is the current request is available on our site? except itself
if (strpos($currentpage, 'index.php') === 0) {
  // There is no valid page ? Redirect the home page
  homepage();
} if (empty($currentpage)) {
  // There is no valid page ? Redirect the home page
  homepage();
} else {
  $links = array('checkout.php', 'checkoutcomplete.php', 'detail.php');

    // Check direct pages such as login, registration
  if (in_array($currentpage, $links)) {
    require(CONTROL_CONFIG_PATH.$currentpage);
  } else {
    notfoundpage();
  }
}

?>