<?php

require '../config.php';
require '../functions.php';

$title = 'Profile';
$active_page = 'profile';

//var_dump($_SESSION); //to check what data is comming to the page

//if no customer is loggedin (no $_SESSION['logged_in'] found)
if(!isset($_SESSION['logged_in'])) {
  header('Location: login.php'); //redirect to login.php page
  die;//always die after redirection
}


//if logged in success, set a greeting message
if(!empty($_SESSION['success'])){//check if $_SESSION['success'] has value (except 0, NULL, false, empty string)
  $flash_message_success = $_SESSION['success'];
  unset($_SESSION['success']);//delete $_SESSION['success']
} 
//if profile success updated, set a success confirm message
elseif (!empty($_SESSION['profile_edited'])){//check if $_SESSION['success'] has value (except 0, NULL, false, empty string)
  $flash_message_success = 'Your profile is updated!';
  unset($_SESSION['profile_edited']);//delete $_SESSION['success']
}


$customer = getCustomerById($dbh, $_SESSION['customer_id']); //get customer info by customer_id
//var_dump($customer);


?>
<?php include '../includes/header.inc.php'; ?>





    <div id="container">
      <h1><?=$title?></h1>
        
      <?php if(!empty($flash_message_success)) echo "<h2 id=\"flash_message_success\">$flash_message_success</h2>"; ?>
        
        <div id="customer_info">
          <h2>Customer Information:</h2>
          
          <ul>
            <li><strong>First Name:</strong> <?=$customer['first_name']?></li>
            <li><strong>Last Name:</strong> <?=$customer['last_name']?></li>
            <li><strong>Email:</strong> <?=$customer['email']?></li>
            <li><strong>Phone:</strong> <?=$customer['phone_number']?></li>
            <li><strong>Country:</strong> <?=$customer['country']?></li>
            
            <li><strong>Billing Province:</strong> <?=$customer['billing_province']?></li>
            <li><strong>Billing City:</strong> <?=$customer['billing_city']?></li>
            <li><strong>Billing Street:</strong> <?=$customer['billing_street']?></li>
            <?php if ($customer['billing_apartment_number'] !== '') : ?><li><strong>Billing Apartment:</strong> <?=$customer['billing_apartment_number']?></li><?php endif; ?>
            <li><strong>Billing Postal Code:</strong> <?=$customer['billing_postal_code']?></li>
            
            <li><strong>Shipping Province:</strong> <?=$customer['shipping_province']?></li>
            <li><strong>Shipping City:</strong> <?=$customer['shipping_city']?></li>
            <li><strong>Shipping Street:</strong> <?=$customer['shipping_street']?></li>
            <?php if(!empty($customer['shipping_apartment_number'])) : ?><li><strong>Shipping Apartment:</strong> <?=$customer['shipping_apartment_number']?></li><?php endif; ?>
            <li><strong>Shipping Postal Code:</strong> <?=$customer['shipping_postal_code']?></li>
          </ul>
          
          <a href="profile_edit.php?customer_id=<?php echo $customer['customer_id']; ?>"><p class="edit_customer_button">Edit profile information</p></a>
          <br />
          <a href="password_edit.php?customer_id=<?php echo $customer['customer_id']; ?>"><p class="edit_customer_button">Edit password</p></a>
          <br />
          <a href="index.php?p=checkout.php"><p class="edit_customer_button">Go to cart</p></a>
        </div><!--END div#user_info-->
      
    </div><!-- Container ends -->







   <?php include '../includes/footer.inc.php'; ?>