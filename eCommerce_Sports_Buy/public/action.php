<?php
/**
 * Action Page
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Retrieve the configuration
require('../config.php');
require(CONFIG_PATH.'taxes.php');
require(CONFIG_PATH.'helper.php');
require(MODEL_CONFIG_PATH.'database.php');
require(MODEL_CONFIG_PATH.'product.db.php');

// Loading a validator class
use classes\utility\checker;

// 5bx-gateway-client
use classes\utility\pagerange5bx;


// Get csrf key from session
$csrfkey = $_SESSION['csrf'];

// ------------------------------------------------------------------------


// getAvailQuantity function
if (isset($_POST['getPST']) && isset($_POST['ammount']) && isset($_POST['province'])) {
    $ammount = floatval($_POST['ammount']);
    $province = $_POST['province'];
    $pst = calculatePST($ammount, $province);
    echo $pst;
}


// ------------------------------------------------------------------------

// getAvailQuantity function
if (isset($_POST['getAvailQuantity']) && isset($_POST['pid'])) {
  $productid = $_POST['pid'];
  // Get the product info
  $product = retrieveProductInfo($productid);    

  if (count($product)>0) {
    $availquantity = $product['units_in_stock'];
    // Is there any quantity in cart?
    if (isset($_SESSION["cartitem"][$productid]["quantity"])) {
      // the available quantity after deduction
      $availquantity -= $_SESSION["cartitem"][$productid]["quantity"];
      $availquantity = adjustIncorrectUnitsInStock($availquantity);
    }
    // Here is the available quantity in a product
    echo $availquantity;
  }
}


// ------------------------------------------------------------------------

// addCart function
if (isset($_POST['addCart']) && isset($_POST['pid']) && isset($_POST['quantity'])) {
  $productid = $_POST['pid'];
  $quantity = $_POST['quantity'];
  // Is valid id?
  if (isnumeric($productid) && isnumeric($quantity)) {
    $quantity = intval($quantity);
    
    if ($quantity > 0) {
      // Get the product info
      $product = retrieveProductInfo($productid);    
    
      if (count($product)>0) {
        // Reach PAX limit?
        $unitsinstock = $product['units_in_stock'];
        if ($quantity > $unitsinstock) {
          //echo "Error: The ordered quantity ($quantity) &gt; the maximum ($unitsinstock)";
          echo "Error: You can order only up to $unitsinstock items.";
        } else {
          // Setup a brand new cart item
          $cartItems = array('title' => $product['product_name'],
              'weight' => $product['weight'],
              'price' => $product['price'],
              'weight' => $product['weight'],
              // This field is used when user goes to checkout
              'image' => $product['thumbnail_image'],
              'quantity' => $quantity);
                   
          if (empty($_SESSION["cartitem"])) {
              $_SESSION["cartitem"][$productid] = $cartItems;
              //echo "OK! Your Cart has been created and filled with FIRST $quantity {$product['product_name']} product.";
              echo "$quantity {$product['product_name']} added in the Shopping Cart.";
          } else {

            if (in_array($productid, array_keys($_SESSION["cartitem"]))) {
                if (empty($_SESSION["cartitem"][$productid]["quantity"])) {
                    $_SESSION["cartitem"][$productid]["quantity"] = 0;
                }
                $total = $_SESSION["cartitem"][$productid]["quantity"] + $quantity;

                // Reach Cart limit?
                if ($total > $unitsinstock) {
                  //echo "Error: Ordered total (NEW $quantity + LAST {$_SESSION["cartitem"][$productid]["quantity"]}) &gt; the maximum ($unitsinstock)";
                  echo "Error: Ordered total (Your ordered $quantity items plus {$_SESSION["cartitem"][$productid]["quantity"]} in cart) &gt; the maximum ($unitsinstock)";
                } else {
                  // Update as normally
                  $_SESSION["cartitem"][$productid]["quantity"] += $quantity;
                  //echo "OK! Your Cart has been received $quantity {$product['product_name']} product.";
                  echo "$quantity {$product['product_name']} added in the Shopping Cart.";
                }

            } else {

                // This is the first time when this product is added
                $_SESSION["cartitem"][$productid] = $cartItems;
                //echo "OK! Your Cart has been added FIRST $quantity {$product['product_name']} product.";
                echo "$quantity {$product['product_name']} added in the Shopping Cart.";
                //dumpList($_SESSION["cartitem"]);
            }

            // Checkpoint should be here.
          }

        }

      }    
    } else {
      echo "Error: The ordered quantity should NOT be ZERO";
    }

  }
}

// ------------------------------------------------------------------------

// removefromcart function
if (isset($_POST['removefromcart']) && isset($_POST['pid'])) {
  $productid = $_POST['pid'];
  // Everything is OK
  if (!empty($_SESSION["cartitem"]) && isnumeric($productid)) { 
    $productid = intval($productid);
    echo "Product of {$_SESSION["cartitem"][$productid]['title']} has been removed from the cart.";
    unset($_SESSION["cartitem"][$productid]);
  }
}

// ------------------------------------------------------------------------

// updatecart function
if (isset($_POST['updatecart']) && isset($_POST['pid']) && isset($_POST['quantity'])) {
  $productid = $_POST['pid'];
  $quantity = $_POST['quantity'];
  // Everything is OK
  if (!empty($_SESSION["cartitem"]) && isnumeric($productid) && isnumeric($productid)) { 
    $productid = intval($productid);
    $quantity = intval($quantity);
    
    if ($quantity > 0) {
      // Get the product info
      $product = retrieveProductInfo($productid);    

      if (count($product)>0) {
        // Reach in stock limit?
        $unitsinstock = $product['units_in_stock'];
        if (in_array($productid, array_keys($_SESSION["cartitem"]))) {
            if (!empty($_SESSION["cartitem"][$productid]["quantity"])) {

            // Reach Cart limit?
            if ($quantity > $unitsinstock) {
              //echo "Error: The ordered quantity ($quantity) &gt; the maximum ($unitsinstock)";
              echo "Error: You can order only up to $unitsinstock items.";
            } else {
              // Update as normally
              $_SESSION["cartitem"][$productid]["quantity"] = $quantity;
              //echo "OK! Your Cart has been received $quantity {$product['product_name']} product.";
              echo "$quantity {$product['product_name']} updated in the Shopping Cart.";
            }
          }
        }
      }
    }
    
  }
}


// ------------------------------------------------------------------------

// cartcount function
if (isset($_POST['cartcount'])){
  // There is no Cart
  if (empty($_SESSION["cartitem"])) {
    echo 0;
  } else {
    $orderedquantity = 0;
    
    foreach ($_SESSION["cartitem"] as $row) {
      if (isset($row['quantity'])) {
        $orderedquantity += $row['quantity'];
      }
    }
    
    echo $orderedquantity;
  }
}

// ------------------------------------------------------------------------

// cartcheckout function
if (isset($_POST['cartcheckout'])){
  // There is no Cart
  if (empty($_SESSION["cartitem"])) {
    echo 'There is no item in your cart. Please go to the shopping page and add some items in the cart.';
  } else {
    //dumpList($_SESSION["cartitem"]);
    $productcontent = '';
    $amounttotal = 0;
    $pst = 0;
    $gst = 0;
    $shippingcost = 0;
    $grandtotal = 0;
    
    // Assume that PST could not be calculated cause no province supplied.
    $isPSTCalculated = false;
    
    // If shipping province is supplied, get PST from it
    if (isset($_POST['billing_province'])){
      $province = $_POST['billing_province'];
      $isPSTCalculated = true;
    }
    
    foreach ($_SESSION["cartitem"] as $key => $row) {
      $productid = $key;
      $title = $row['title'];
      $price = $row['price'];
      $total = $price * $row['quantity'];
      $amounttotal += $total;
      if ($isPSTCalculated)
        $pst += calculatePST($total, $province);
      $gst += calculateGST($total);
      $shippingcost += calculateShippingCost($total);
      $grandtotal += $total + $pst + $gst + $shippingcost;
      
      $productcontent .= <<<EOT
        <div class='row'>
          <div class='col-md-2'><img src='images/product_thumbnail_images/{$row['image']}' alt='$title' width='60' height='40'></div>
          <div class='col-md-2 text-info'>$title</div>
          <div class='col-md-2'><input class='form-control price' type='text' size='10' data-id='$productid' id='price-$productid' value='$price' disabled></div>
          <div class='col-md-2'><input class='form-control qty' type='text' size='10' data-id='$productid' id='qty-$productid' value='{$row['quantity']}'></div>
          <div class='col-md-2'><input class='total form-control price' type='text' size='10' data-id='$productid' id='amt-$productid' value='$total' disabled></div>
          <div class='col-md-2'>
          <a href='#' data-id='$productid' class='btn btn-danger update'><span class='glyphicon glyphicon-check'></span></a>
          &nbsp;
          <a href='#' data-id='$productid' class='btn btn-danger remove'><span class='glyphicon glyphicon-trash'></span></a>
          </div>
        </div>
EOT;
    }
    
    // Round those currency values up 
    
    $amounttotal = round($amounttotal ,2);
    $pst = round($pst ,2);
    $gst = round($gst ,2);
    $shippingcost = round($shippingcost ,2);
    $grandtotal = round($grandtotal ,2);
    
    $productcontent .= <<<EOT
        <div><br/></div>
        <div class='row'>
          <div class='col-md-3'>
              <b>Sub Total: &nbsp;$<span id='amounttotal'>$amounttotal</span></b>
          </div>
          <div class='col-md-2'>
              <b>PST: &nbsp;$<span id='pst'>$pst</span></b>
          </div>
          <div class='col-md-2'>
              <b>GST: &nbsp;$<span id='gst'>$gst</span></b>
          </div>
          <div class='col-md-2'>
              <b>Ship Cost: &nbsp;$<span id='shippingcost'>$shippingcost</span></b>
          </div>
          <div class='col-md-3'>
              <b>Grand Total: &nbsp;$<span id='grandtotal'>$grandtotal</span></b>
          </div>
        </div>
EOT;
    
    echo $productcontent;
  }

}

// ------------------------------------------------------------------------

function doPaymentCheckOut() {
  // Supply the user payment information
  $paymentinfo = array(
    'grandtotal' => floatval($_POST['grandtotal']),
    'first_name' => getSanitizedValue('first_name'),
    'last_name' => getSanitizedValue('last_name'),
    'email' => getSanitizedValue('email'),
    'phone_number' => getSanitizedValue('phone_number'),
    'country' => getSanitizedValue('country'),
    'billing_province' => getSanitizedValue('billing_province'),
    'billing_city' => getSanitizedValue('billing_city'),
    'billing_street' => getSanitizedValue('billing_street'),
    'billing_apartment_number' => getSanitizedValue('billing_apartment_number'),
    'billing_postal_code' => getSanitizedValue('billing_postal_code'),
    'shipping_province' => getSanitizedValue('shipping_province'),
    'shipping_city' => getSanitizedValue('shipping_city'),
    'shipping_street' => getSanitizedValue('shipping_street'),
    'shipping_apartment_number' => getSanitizedValue('shipping_apartment_number'),
    'shipping_postal_code' => getSanitizedValue('shipping_postal_code'),
    'card_number' => getSanitizedValue('card_number'),
    'expirydate' => getSanitizedValue('expirydate'),
    'cvv' => getSanitizedValue('cvv'),
    'cardtype' => getSanitizedValue('cardtype')
    );
  
  $checkoutcontent = '';
  
  $userid = $_SESSION['customer_id'];
  
  $tid = initTransaction($userid, $paymentinfo);
  
  if ($tid < 0) {
    return 'Could not create a transaction.';
  }
    
  $transaction = new pagerange5bx(LOGIN_ID, API_KEY);
  $transaction->amount($paymentinfo['grandtotal']);
  $transaction->card_num($paymentinfo['card_number']); // credit card number
  $transaction->exp_date ($paymentinfo['expirydate']); // expiry date month and year
  $transaction->cvv($paymentinfo['cvv']); // card cvv number
  $transaction->ref_num($tid); // the reference or invoice number
  $transaction->card_type($paymentinfo['cardtype']); // card type
  $response = $transaction->authorize_and_capture(); // returns JSON object
  
  if ($response->transaction_response->response_code == '1') {
      // Your transaction was authorized
      // Continue our order
  } elseif (count((array)$response->transaction_response->errors)) {
      $errors = '';
      foreach($response->transaction_response->errors as $error) {
          $errors .= $error . '<br />';
      }
      // Card is not accepted by
      return $errors;
  }
  
  // Assume that there is no returned Invoice
  $returnedInvoiceId = 0;
  
  $productstakenout = orderProduct($userid, $_SESSION["cartitem"], $paymentinfo, $tid, $response);
  
  
  // Is there any error?
  if (count($productstakenout)>0) {
    // Is this an error message?
    if (isset($productstakenout['error'])) {
      // Print it out
      $checkoutcontent .=  $productstakenout['error'];
      return $checkoutcontent;
      
    } else {
      // Populate the booked product ids
      $checkoutcontent = '';
      $amounttotal = 0;
      
      // Convert booked ids into a key array
      foreach ($productstakenout as $row) {
        $bookedproductids[] = $row['product_id']; 
      }
      
      foreach ($_SESSION["cartitem"] as $key => $row) {
        $productid = $key;
        $title = $row['title'];
        $price = $row['price'];
        $total = $price * $row['quantity'];
        $amounttotal += $total;
        
        // Is this a booked product?
        if (in_array($key, $bookedproductids)) {
          $checkoutcontent .= <<<EOT
            <div class='row'>
              <div class='col-md-2'><img src='images/product_thumbnail_images/{$row['image']}' alt='$title' width='60' height='40'></div>
              <div class='col-md-2 text-danger'><strike>$title</strike></div>
              <div class='col-md-2'><strike>$price</strike></div>
              <div class='col-md-2'><strike>{$row['quantity']}</strike></div>
              <div class='col-md-2'><strike>$total</strike></div>
              <div class='col-md-2'><a href='#' data-id='$productid' class='btn btn-danger remove'><span class='glyphicon glyphicon-trash'></span></a></div>
            </div>
EOT;
        } else {
          $checkoutcontent .= <<<EOT
            <div class='row'>
              <div class='col-md-2'><img src='images/product_thumbnail_images/{$row['image']}' alt='$title' width='60' height='40'></div>
              <div class='col-md-2 text-info'>$title</div>
              <div class='col-md-2'><input class='form-control price' type='text' size='10' data-id='$productid' id='price-$productid' value='$price' disabled></div>
              <div class='col-md-2'><input class='form-control qty' type='text' size='10' dataid='$productid' id='qty-$productid' value='{$row['quantity']}' disabled></div>
              <div class='col-md-2'><input class='total form-control price' type='text' size='10' dataid='$productid' id='amt-$productid' value='$total' disabled></div>
              <div class='col-md-2'><a href='#' data-id='$productid' class='btn btn-danger remove'><span class='glyphicon glyphicon-trash'></span></a></div>
            </div>
EOT;
        }
      }

      $checkoutcontent .= <<<EOT
          <div class='row'>
            <div class='col-md-8'>
            </div>
            <div class='col-md-4'>
                <b>Total: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$$amounttotal</b>
            </div>
          </div>
EOT;

      return $checkoutcontent;
    }
  } else {
    // Everything is OK
    unset($_SESSION["cartitem"]);
    
    // Return with auth code
    return 'OK'.$response->transaction_response->auth_code;
  }  
  
  return $checkoutcontent;
}


// paymentcheckout function
if (isset($_POST['paymentcheckout']) && isset($_POST['csrf'])) {
  // CSRF Check
  if ($csrfkey == $_POST['csrf']) {
    $validator = new checker();
    
    $validator->validation_bind('first_name', 'First Name', 'compulsory/minlength[2]/maxlength[255]');
    $validator->validation_bind('last_name', 'Last Name', 'compulsory/minlength[2]/maxlength[255]');
    $validator->validation_bind('phone_number', 'Phone', 'compulsory/phone');
    $validator->validation_bind('email', 'Email', 'compulsory/validemail');
    $validator->validation_bind('shipping_province', 'Shipping Province', 'compulsory/minlength[2]/maxlength[255]');
    $validator->validation_bind('shipping_city', 'Shipping City', 'compulsory/minlength[2]/maxlength[255]');
    $validator->validation_bind('shipping_street', 'Shipping Street', 'compulsory/minlength[2]/maxlength[255]');
    $validator->validation_bind('shipping_apartment_number', 'Shipping Apartment Number', 'minlength[1]/maxlength[255]');
    $validator->validation_bind('shipping_postal_code', 'Shipping Postal Code', 'compulsory/canadianpostal');
    $validator->validation_bind('billing_province', 'Billing Province', 'compulsory/minlength[2]/maxlength[255]');
    $validator->validation_bind('billing_city', 'Billing City', 'compulsory/minlength[2]/maxlength[255]');
    $validator->validation_bind('billing_street', 'Billing Street', 'compulsory/minlength[2]/maxlength[255]');
    $validator->validation_bind('billing_apartment_number', 'Billing Apartment Number', 'minlength[1]/maxlength[255]');
    $validator->validation_bind('billing_postal_code', 'Billing Postal Code', 'compulsory/canadianpostal');
    $validator->validation_bind('card_number', 'Card Number', 'compulsory/numericdash/minlength[16]/maxlength[20]');
    $validator->validation_bind('expirydate', 'Expiry Date', 'compulsory/numericdash/minlength[4]/maxlength[4]');
    $validator->validation_bind('cvv', 'CVV', 'compulsory/numericdash/minlength[3]/maxlength[3]');
    $validator->validation_bind('cardtype', 'Card Type', 'compulsory/minlength[2]/maxlength[255]');

    if ($ok = $validator->isValidated()) {
       $checkoutcontent = doPaymentCheckOut();
    } else {
       $checkoutcontent = $validator->buildErrorMessages();
    }

    echo $checkoutcontent;
  }

}


// ------------------------------------------------------------------------


?>