<?php

require '../config.php';
require '../functions.php';

$title = 'Privacy Policy';
$active_page = '';
?>

<?php include '../includes/header.inc.php'; ?>


    <!-- container starts -->
    <div id="container">
      
    
      <!-- content starts -->
      <div id="content">
        
        <!-- about_us_paragraph starts here -->
        <div id="about_us_paragraph">
        <h1> Sports Buy Privacy Statement </h1>
          <h2>Introduction and Scope of this Privacy Statement</h2>
          <p>We, Sports Buy Canada want to give you the best experience possible. We use the personal information you share with us to make our products and services and your experience even better. This privacy statement aims to give you a clear view of how we use personal information that you provide, our dedication to protecting it, your rights and the options you have to control your personal information and protect your privacy. It also outlines what personal information we collect about you when you visit our websites, stores or when you use our mobile apps, how we use your personal information and the third parties we will share it with.</p>

          <h2> USE OF OUR PRODUCTS AND SERVICES</h2>
          <p>When you use our products and services we will ask for personal information (at the moment of collection we will explain what information is required and what information you may choose to provide), such as:</p>
          
          <ul>
            <li>Your postal address and payment details when ordering from our web shop;</li>
            <li>Your date of birth and/or confirmation that you are above a certain age;</li>
            <li>Your email-address when signing up for one of our contests, free prize giveaways ("Prize Giveaways") or newsletters;</li>
            <li>A record of our correspondence with you (including any feedback you have provided on any of our products or services), if you have been in contact with our Customer Service.</li>
          </ul>
          
          <h2> HOW WE USE YOUR PERSONAL INFORMATION</h2>
          <p>The personal information we collect when you create first sign in will be used for identification and authentication purposes across sportsbuy services, so you can use the same account details to log in from any location and on any device. The global single sign on will allow sportsbuy systems to identify you wherever you are in the world, meaning you will not need to register with us again if you are signing in from a different country.</p>

          <ul>
            <li><strong>Improving our products and services:</strong>we will use your personal information to build a profile on you in order to understand how you use our products and services, to develop more interesting and relevant products and services, as well as to personalise the products and services we offer you.</li>

            <li><strong>Processing your order:</strong>we use relevant personal information described above (including your name, address and payment details) to process and deliver your order, and to notify you of the status of your order. In addition, we may use your age, year of birth or confirmation that you are above a certain age to ascertain whether you meet the minimum age in your country for online purchases and whether you are considered to be a minor for data protection and/or marketing law purposes.</li>

            <li><strong>Customer service:</strong>if you contact our Customer Service (or vice versa), we will use personal information such as your order information and contact history to process your request and provide you with the best service possible.</li>
          </ul>


          <h2>NEXT… WHY DO WE USE COOKIES?</h2>
          <p>Sports Buy uses cookies primarily to ensure your visit to our website is as pleasant as possible, as well as for advertising-related purposes during your future visits to other websites. Below you see a more elaborate overview of the types of cookies we are using and why:</p>

          <ul>
            <li><strong>REQUIRED COOKIES:</strong>are essential and help you navigate, move around on the website and see certain features (e.g. these cookies make sure your basket is saved during all the steps of your checkout process). These cookies are required to enable core site functionality. These cookies are stored for the duration of your browsing session.</li>

            <li><strong>FUNCTIONALITY AND ANALYTICS COOKIES</strong>are there so we can give you an even smoother customer experience (e.g. these cookies help you to save and remind you about your shopping bag and enable you to create a wish list). These cookies allow us to analyse site usage so we can measure and improve performance. These cookies may be placed by us or a third party on our behalf (see our cookie opt-out tool) and are stored for the duration of your browsing session.</li>

            <li><strong>ADVERTISING AND SOCIAL MEDIA COOKIES:</strong>will remember your product and buying preferences or assist marketing efforts in other ways. These cookies enable us to share data, such as what you like, with our advertisers, so the advertisement you see can be more relevant to your preferences (sometimes referred to as "targeting cookies").</li>
          </ul>
            
            <h2>FINALLY…WHERE CAN I GET FURTHER INFORMATION?</h2>
            <p>If you have any questions about our use of cookies or other technologies, <strong>please email us at sports_buy@gmail.ca.</strong></p>
            
        </div><!-- terms_cond_paragraph ends here -->

      </div><!-- content ends -->

    </div><!-- Container ends -->


<?php include '../includes/footer.inc.php'; ?>


