<?php

require '../config.php';

//unsetting customer data in SESSION // analog $_SESSION = array();
unset($_SESSION['logged_in']); // logs customer out by destroying $_SESSION['logged_in']
unset($_SESSION['customer_id']);
session_regenerate_id(); // regenerate session id
$_SESSION['logged_out'] = true;//set a new key, $_SESSION['logged_out'] to display success logout msg after redirection
header('Location: login.php'); //redirect to the login page



