<?php

require '../config.php';
require '../functions.php';

$title = 'Payment Methods';
$active_page = '';

?>

<?php include '../includes/header.inc.php'; ?>
    
    <!-- container starts -->
    <div id="container">
      
    
      <!-- content starts -->
      <div id="content">
        <!-- about_us_paragraph starts here -->
        <div id="about_us_paragraph">
        <h1> Payment Options </h1>
          <h2>What Payment Methods can i use on Sports Buy?</h2>
          <p>We want to make buying your favourite items online fast and easy, and we accept the following payment options:</p>

          <ul>
            <li>Visa</li>
            <li>Master Card</li>
            <li>Debit Card</li>
            <li>PayPal</li>
          </ul>

          <p>Sports Buy Members can store multiple debit or credit cards in their profile for faster checkout.</p>
          <p><strong>If you're not already a member, join SportsBuy today.</strong></p>

        </div><!-- terms_cond_paragraph ends here -->

      </div><!-- content ends -->

    </div><!-- Container ends -->


<?php include '../includes/footer.inc.php'; ?>