<?php

require '../config.php';
require '../functions.php';

$title = 'Products';
$active_page = 'products';

//get all categories for sidebar
$categories = getCategories($dbh);


//get products to display, depending on customers choice
// if get query to display products by category name
if(!empty($_GET['category'])) {
  if($_GET['category'] == 'all') { //if category name is All Products
    $products = getAllProducts($dbh);//get all products
    $container_title = 'All Products';
  }//END if($_GET['category'] == 'all')
  else {//if category name is not all
    $products = getProductsByCategory($dbh, $_GET['category']);
    $category_name = getCategoryName($dbh, $_GET['category']);//if no products under the category, we get category name to display separately, not from $products as it will be empty
    $container_title = $category_name['category_name'];
  }
}//END if(!empty($_GET['category']))

// if get query to display products by price
elseif(!empty($_GET['price'])) {
  list($price_min, $price_max) = explode('_', $_GET['price']);//split $_GET['price'] value by delimiter _ , assign first value to $price_min
  $products = getProductsByPriceRange($dbh, $price_min, $price_max);
  $container_title = "Products By Price: C$" . $price_min . " - C$" . $price_max;
}//END elseif(!empty($_GET['price']))

// if get query to display products by brand name
elseif(!empty($_GET['brand'])) {
  $products = getProductsByBrand($dbh, $_GET['brand']);
  $brand_name = getBrandName($dbh, $_GET['brand']);
  $container_title = $brand_name['brand_name'];
}//END elseif(!empty($_GET['brand']))

// if get keyword from search field
elseif(!empty($_GET['keyword'])) {
  $keyword = strip_tags(htmlspecialchars($_GET['keyword'])); //assign $_GET['keyword'] value to a variable, after escaping special chars and removing tags for security reasons
  $keyword_extended ="%" . $keyword . "%"; //search including any chars before typed keyword (%$keyword) any chars after typed ($keyword%)
  $products = search($dbh, $keyword_extended); //send to the search function: database and value of $keyword
  $container_title = 'Results for: ' . $keyword;
  $search_result_count = count($products);//number of items(subarrays)
}

//if none of above $_GET queries, display random products as default
else {
  $products = getRandomProducts($dbh, 20); //get random 20 products
  $container_title = "Random Products";
}


?>
<?php include '../includes/header.inc.php'; ?>
    
    
    
    <div id="container">

      <!--########## START sidebar ###########-->
      <div id="sidebar">
        <h1>Filters</h1>
        
          <h2>Categories</h2>
          <ul>
            <?php foreach($categories as $key => $value) : ?>
              <li>
                <a href="products.php?category=<?php echo $value['category_id']?>">
                  <?php echo $value['category_name']?>
                </a>
              </li>
            <?php endforeach; ?>
            <li><a href="products.php?category=all">All Products</a></li>
          </ul>

          <h2>Price</h2>
          <ul>
            <li><a href="products.php?price=0_50">Under C$50</a></li>
            <li><a href="products.php?price=50_100">C$50-C$100</a></li>
            <li><a href="products.php?price=100_200">C$100-C$200</a></li>
            <li><a href="products.php?price=200_300">C$200-C$300</a></li>
            <li><a href="products.php?price=300_1000000">Above C$300</a></li>
          </ul>
      </div> <!-- #sidebar end -->
      <!--########## END sidebar ###########-->



      <!-- ############### START product list ################################ -->
      <div id="product_list">
        <h1><?php echo $container_title?></h1>

        <div id="center_products">

          <?php foreach($products as $key => $value) : ?>
            <div class="single_product">
              <a href="index.php?p=detail.php&pid=<?php echo $value['product_id']; ?>">
                <div class="single_product_image"><!-- product image -->
                  <img src="images/product_thumbnail_images/<?php echo $value['thumbnail_image'] ?>" alt="<?php echo $value['product_name'] ?>" />
                </div> <!-- single_product_image ends here -->
                
                <div class="single_product_info"><!-- product info -->
                  <ul>
                    <li><p><strong><?php echo $value['product_name'] ?></strong></p></li>
                    <li>C$ <?php echo $value['price'] ?></li>
                    <li>Availability: <?php echo $value['availability'] == 1 ? 'Yes' : 'No' ?></li>
                  </ul>
                </div><!-- single_product_info ends here -->
              </a>
            </div><!-- END single_product -->
          <?php endforeach; ?>

        </div> <!-- END #center_products -->

      </div> <!-- END product_list ends here -->
      <!-- ############### END product list ################################ -->

    </div><!-- Container ends -->
    <div class="clear_both"></div>



<?php include '../includes/footer.inc.php'; ?>







