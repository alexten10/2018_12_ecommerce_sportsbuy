<?php

require '../config.php';
require '../functions.php';

$title = 'Home';
$active_page = 'home';
$categories = getCategories($dbh);
$new_arrivals = getNewArrivals($dbh);
$trending = getTrendingBrands($dbh);

?>
<?php include '../includes/header.inc.php'; ?>





    <div id="container">

      <!-- Slider starts here -->
      <div class="slider">
        <div><img src="images/slider/slide1_bike.jpg" alt="Bike slide" /></div>
        <div><img src="images/slider/slide2_shoe.jpg" alt="Shoe slide" /></div>
        <div><img src="images/slider/slide3_bag.jpg" alt="Bag slide" /></div>
      </div><!-- Slider ends -->
       


      <!-- shop_by_category starts here -->
      <div id="shop_by_category">
        <h1>Shop by Category</h1>
        <ul>
          <?php foreach($categories as $key => $value) : ?>
            
            <li>
              <a href="products.php?category=<?php echo $value['category_id']?>">
                <img src="images/category_images/<?php echo $value['category_image']?>" alt="<?php echo $value['category_name']?>" />
              </a>
              <p><?php echo $value['category_name']?></p>
            </li>
          <?php endforeach; ?>
        </ul>
      </div><!-- shop_by_category ends -->


      <!-- new_arrivals starts here -->
      <div id="new_arrivals">
        <h1>New Arrivals</h1>
        <ul>
          <?php foreach($new_arrivals as $key => $value) : ?>
            <li>
              <a href="index.php?p=detail.php&pid=<?php echo $value['product_id']?>">
                <img src="images/product_thumbnail_images/<?php echo $value['thumbnail_image']?>" alt="<?php echo $value['product_name']?>" />
                <h2><?php echo $value['product_name']?></h2>
                <p>Shop now</p>
              </a>
            </li>
          <?php endforeach; ?>
        </ul>
      </div><!-- new_arrivals ends -->


      <!-- ATTENTION hardcoded trending section, cause we dont have logos for all brands-->
      <!-- Trending starts here -->
      <div id="trending">
        <h1>Trending</h1>
        <ul>
          <?php foreach($trending as $key => $value) : ?>
            <li>
              <a href="products.php?brand=<?php echo $value['brand_id']?>">
                <img src="images/brand_images/<?php echo $value['image']?>" alt="<?php echo $value['brand_name']?>" />
              </a>
            </li>
          <?php endforeach; ?>
        </ul>
      </div><!-- Trending ends -->

    </div><!-- Container ends -->




<?php include '../includes/footer.inc.php'; ?>
