<?php

require '../config.php';
require '../functions.php';

$title = 'Delivery & Return';
$active_page = '';

?>

<?php include '../includes/header.inc.php'; ?>
    
    <!-- container starts -->
    <div id="container">
    
      <!-- content starts -->
      <div id="content">
        
        <!-- about_us_paragraph starts here -->
        <div id="about_us_paragraph">
        <h1> Delivery and Returns </h1>
           <h2>Standard Shipping</h2>
          
          <p><strong>Please Note: Most orders are delivered within 5-7 business days(From Time Order)</strong></p>

          <p>Orders are estimated to arrive approximately 3-4 business days after shipping from a fulfillment centre. Please note that shipping to select rural or remote destinations may result in extended shipping times.</p><br />

          <p>Depending on the time of the year, you will have the option to upgrade the shipping method of your order for faster delivery with our Express Shipping service. If you choose to upgrade to Express Shipping, your order must be received and clear credit authorization by 12:00 PM (noon) EST or your order may not be processed until the following business day. Business days are Monday to Friday, excluding statutory holidays.</p>

          <h2>Shipping Process</h2>
          <p>You will not be charged for any item purchased with a credit card until the item has been shipped from one of our fulfillment centres. You will however see a pre-authorization on the date that the order was created.</p>

          <p>Shipping offers may apply to delivery by Standard Shipping only, may apply to select items only, may be limited to a maximum credit towards shipping costs or may not apply to all shipping destinations. Please review the shipping offer details for specific terms and restrictions.</p>

          <p>We reserve the right to modify shipping offer rules and regulations or to discontinue shipping offers at any time without notice.</p>

          <p>If you have additional questions, please email us at:<strong>sports_buy@gmail.ca</strong> </p>

        </div><!-- terms_cond_paragraph ends here -->

      </div><!-- content ends -->

    </div><!-- Container ends -->


<?php include '../includes/footer.inc.php'; ?>