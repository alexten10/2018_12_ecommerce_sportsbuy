<?php

require '../config.php';
require '../functions.php';
use \classes\utility\validator;
$vldtr = new validator;



$title = 'Login';
$active_page = 'login';

//var_dump($_SESSION);

//check if $_POST comes to the page
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  //validate input fields
  $vldtr->validateEmail('email'); //this is 1
  $vldtr->required('email'); //this is 2
  $vldtr->required('password');
  
  
  //if no validation errors found
  if(empty($vldtr->errors())) {
    
    //sanitize input
    $post = $_POST;
    $sanitized_post_array = sanitizeFormInputs($post);
    //var_dump($sanitized_post_array);

    //check if entered email exists in db
    $customer_info = getCustomerByEmail($dbh, $sanitized_post_array['email']);
    //var_dump($customer_info); //to check if query works properly and array has all info about specific customer
    
    
    //compare typed password with the password stored in database
    /// 1 if passwords match (remember if there is no customer were found before, it will compare typed password against empty array)
    if(password_verify($sanitized_post_array['password'], $customer_info['password'])) {
      $greeting = "Welcome, {$customer_info['first_name']} {$customer_info['last_name']}!";
      $_SESSION['success'] = $greeting;//if email and password match, set $_SESSION['success'] to exist
      $_SESSION['logged_in'] = true;
      $_SESSION['customer_id'] = $customer_info['customer_id'];//non-sensitive info, but it can identify customer by id
      session_regenerate_id();//regenerate session id
      header('Location: profile.php');//redirect to profile.php
      die;//if redirect, then die (stop) this page loading
    }//END if
    /// 2 if passwords dont match OR no info found in database about user by email
    else {
      $_SESSION['no_success'] = 'Sorry, credentials don\'t match!';
      $flash_message_no_success = $_SESSION['no_success'];//set error message
      unset($_SESSION['no_success']);
    }//END else
    
  }//END if(empty($vldtr->errors())) 
  
}//END if($_SERVER['REQUEST_METHOD'] == 'POST')

//if $_SESSION['logged_out'] exists, which comes form logout.php
if(isset($_SESSION['logged_out'])){
  $_SESSION['logged_out'] = 'You have logged out.';
  $flash_message_logout = $_SESSION['logged_out'];
  unset ($_SESSION['logged_out']);
}

$errors = $vldtr->errors();
?>
<?php include '../includes/header.inc.php'; ?>

    <div id="container">
      
      <h1><?php echo $title ?></h1>
      
      <!-- display error msg if wrong login or password-->
      <?php if(!empty($flash_message_no_success)) echo "<h2 id=\"flash_message_no_success\">$flash_message_no_success</h2>"; ?>
      
      <!--if logout, show message for success log out-->
      <?php if(!empty($flash_message_logout)) echo "<h2 id=\"flash_message_success\">$flash_message_logout</h2>"; ?>

      
      <div id="login_form">
        
        <form method="post"
              action="login.php"
              id="login"
              name="login"
              accept-charset="utf-8" 
              novalidate="novalidate">

          <fieldset>
            <legend><b>LogIn</b></legend>

            <p>
              <label for="email">Email Address</label>
              <input type="email"
                     name="email"
                     id="email"
                     value="<?php 
                              if (!empty($_POST['email'])) {
                                echo esc ($_POST['email']);
                              }
                            ?>"
                     placeholder="Enter your email" /><br />
                     
                     <!-- display errors if exist -->
                    <?php if(!empty($errors['email'])) : ?>
                      <span class="error"><?=$errors['email']?></span><br />
                    <?php endif; ?>
            </p>

            <p>
              <label for="pass">Password</label>
              <input type="password"
                     id="pass"
                     name="password"
                     maxlength="255"
                     value=""
                     placeholder="Enter your password" /><br />
                     
              <?php if(!empty($errors['password'])) : ?>
                <span class="error"><?=$errors['password']?></span><br />
              <?php endif; ?>
            </p>

          </fieldset>
        

          <p id="form_submit_buttons">
            <input type="submit" value="Login" class="button" />&nbsp; &nbsp;
            <input type="button" value="Clear Form" class="button" onclick="clearForm(this.form);" />
          </p>

          <script>
            //JS to clear input fields
            function clearForm() {
              var tags = document.getElementsByTagName("input");
              for (i=0; i<tags.length; i++) {
                if(tags[i].type == "button" || tags[i].type == "submit"){
                  continue;
                }//END if
                tags[i].value = '';
              }//END for
            }//END clearForm()
          </script>

        </form>

      </div><!-- END #login_form -->
    </div><!-- Container ends -->

<?php include '../includes/footer.inc.php'; ?>