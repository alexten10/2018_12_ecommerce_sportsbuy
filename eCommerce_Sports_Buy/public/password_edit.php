<?php

require '../config.php';
require '../functions.php';
use \classes\utility\validator;
$vldtr = new validator;


$title = 'Edit Password';
$active_page = '';

if(empty($_SESSION['logged_in'])) {
  header ('Location: index.php');
  die;
}


if(!empty($_GET['customer_id'])) {
  $customer = getCustomerById($dbh, $_GET['customer_id']);
}


if($_SERVER['REQUEST_METHOD'] == 'POST') {
  
  $customer = getCustomerById($dbh, $_POST['customer_id']);

  //if typed old password matches to the stored in DB
  if ( password_verify($_POST['old_password'], $customer['password']) ) {
    $current_date = date('Y-m-d h:m:s');
    
    // !!! order of calling functions is important !!!
    $vldtr->validatePasswordStrength('password');
    $vldtr->minLength('password', 6);//minimum required length of password is 6 characters
    $vldtr->maxLength('password', 12);//maximum required password length is 12 characters
    $vldtr->passwordsMatch('password', 'password_confirm');
    $vldtr->required('password');
    
    if(empty($vldtr->errors())) {
      
      $new_password = htmlspecialchars($_POST['password']);
      $updatePassword = updatePassword($dbh, $new_password, $current_date, $customer['customer_id']);
      if($updatePassword == true) {
        $_SESSION['password_updated'] = "Password updated";
        $flash_message = $_SESSION['password_updated'];
        unset($_SESSION['password_updated']);
        $email_sent = sendEmail($customer['first_name'], $customer['email'], $_POST['password']);
      }//END if($updatePassword == true)
      
    }//END if(empty($vldtr->errors()))
    
  }//END if (  (password_hash($_POST['old_password']))
  else {
    $wrong_password_msg = 'Typed Old password is wrong!';
  }//END else
    
}//END if($_SERVER['REQUEST_METHOD'] == 'POST')
$errors = $vldtr->errors();

?>
<?php include '../includes/header.inc.php'; ?>



    <div id="container">
        
        
        <h1><?=$title?></h1>
        <br/>
        
        <!-- if editing profile was unsuccessful, show a message-->
        <?php if(isset($flash_message)) :?>
          <h2><?php echo $flash_message ?></h2>
        <?php endif; ?>
        
        <?php if(isset($wrong_password_msg)) :?>
          <h2><?php echo $wrong_password_msg ?></h2>
        <?php endif; ?>
        
        <?php if(isset($email_sent)) :?>
          <h2>Confirmation is sent to your email</h2>
        <?php endif; ?>
        
        <div id="customer_password_form">
          <form method="post"
                  action="password_edit.php"
                  id="password_edit_form"
                  name="password_edit_form"
                  accept-charset="utf-8" 
                  novalidate="novalidate">
              
            <fieldset>
              <legend><b>Edit Customer Password</b></legend>
              
              <p><span class="required_sign">*&nbsp;</span>- required field</p>

              <p>
                <label for="password"><span class="required_sign">*&nbsp;</span>Old Password</label>
                <input type="password"
                       id="old_password"
                       name="old_password"
                       maxlength="255"
                       value=""
                       placeholder="Type your old password" /><br />
                       
                <?php if(!empty($errors['old_password'])) : ?>
                  <span class="error"><?=$errors['old_password']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="password"><span class="required_sign">*&nbsp;</span>New Password</label>
                <input type="password"
                       id="password"
                       name="password"
                       maxlength="255"
                       value=""
                       placeholder="New password (min length 6)" /><br />
                       
                <?php if(!empty($errors['password'])) : ?>
                  <span class="error"><?=$errors['password']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="pass_confirm"><span class="required_sign">*&nbsp;</span>Confirm Password</label>
                <input type="password"
                       id="pass_confirm"
                       name="password_confirm"
                       maxlength="255"
                       value=""
                       placeholder="Type again your new password" />
              </p>


              <input type="hidden" name="customer_id" value="<?=$customer['customer_id']?>" />

            </fieldset>


            <!-- *******  BUTTONS ************-->
            <p id="form_submit_buttons">
              <input type="submit"
                     value="Submit"
                     class="button" />&nbsp; &nbsp;
              <input type="button"
                     value="Clear Form"
                     onclick="clearForm(this.form);"
                     class="button" />
            </p>
            
            
            <!--****** SCRIPT FOR BUTTON "CLEAR FORM" *****  -->
            <script>
              //JS to clear 'input' fields
              function clearForm() {
                //clear input fields
                var tags = document.getElementsByTagName("input");
                for (i=0; i<tags.length; i++) {
                  if(tags[i].type == "button" || tags[i].type == "submit"){
                    continue;
                  }//END if
                  tags[i].value = '';
                }//END for
                
              }//END clearForm()
            </script>
          
          </form>
        </div><!-- END #customer_password_form -->
        
    </div><!-- Container ends -->


<?php include '../includes/footer.inc.php'; ?>