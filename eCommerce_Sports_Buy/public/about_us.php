<?php


require '../config.php';
require '../functions.php';

$title = "About Us";
$active_page = 'about_us';
?>

<?php include '../includes/header.inc.php'; ?>



    <!-- container starts here -->
    <div id="container">
      
      <!-- aboutus_hero_image starts here -->
      <div id="aboutus_hero_image">
        <img src="images/about_us/about_us_banner.jpg" alt="about_us_image" />
      </div> <!-- aboutus_hero_image ends here -->


      <!-- div id content starts for the whole page -->
      <div id="content">

        <!-- about_us_paragraph starts here -->
        <div id="about_us_paragraph">
          <h1> About Us </h1>
          <p><strong>Sports Buy was established in 2013 which is Canada's health and wellness destination for looking well, feeling well, and performing well. We are Canada's largest provider of team sporting equipment, apparel, accessories and gym supplements.</strong></p>
          <p>We are a sincere company with a straightforward vision and we want to inspire our customers to live an active lifestyle by making sports and activity more fun, approachable, and inclusive.</p>

          <h2>Our Mission</h2>
          <p>Our mission is to “to help motivate Canadians to get moving and help them achieve their wellness goals and want to inspire our customers to live an active lifestyle by playing sports and activities more fun, approachable, and inclusive”.</p>
        </div><!-- about_us_paragraph ends here -->

        
        <!-- team_members starts here -->
        <div id="team_members">
          <h1> Our Team Members</h1>
          <ul>
            <li><img src="images/about_us/about_us_thai.png" alt="thai_image" />
            <p><strong>Thai Tran</strong> is an Owner and the Creative Director for Sports Buy. Possessing the ability to drive unique insights into any design or marketing challenge. Thai assists our clients in maximizing the value of their online presence.</p><div class="clear_both"></div></li>
            

            

            <li><img src="images/about_us/about_us_alex.jpg" alt="alex_image">
            <p><strong>Alex Ten</strong> is a Project Manager for the Sports Buy. He is responsible for direction, executive, control and completion of the projects. From leading business development meetings to participating in "project evangelism" to rolling up his sleeves for the hands-on work that goes along with day-to-day operations.</p><div class="clear_both"></div></li>


            
          
            <li><img src="images/about_us/about_us_arushi.jpg" alt="arushi_image">
            <p><strong>Arushi Jain</strong> is a Chief Executive Officer of Sports Buy. She is charged with keeping the company pushing forward. She entails being ultimately responsible for all day-to-day management decisions and implementing the Company's long and short term plans.</p><div class="clear_both"></div></li>


            

            <li><img src="images/about_us/about_us_amar.jpg" alt="amar_image" />
            <p><strong>Amarjeet Sharma</strong> is a eCommerce Strategist at Sports Buy. He helps companies grow through increasing awareness of our brand, strategizing revenue growth, and expanding the online presence.</p><div class="clear_both"></div></li>


            

            <li><img src="images/about_us/about_us_avinesh.jpg" alt="avinesh_image">
            <p><strong>Avinesh Sijapati</strong> is a Visual Designer and leads the team at Sports Buy. He enjoys working with clients from all the industries and turning their online presence into one that performs well and is aesthically pleasing.</p><div class="clear_both"></div></li>
 
          </ul>
          <div class="clear_both"></div>

        </div><!-- team_members ends here -->

      </div><!-- content ends here -->

    </div><!-- Container ends -->
    <div class="clear_both">
    </div>

<?php include '../includes/footer.inc.php'; ?>  