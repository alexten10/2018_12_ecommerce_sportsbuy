<?php

require '../config.php';
require '../functions.php';

$title = 'Track your Order';
$active_page = '';


?>

<?php include '../includes/header.inc.php'; ?>

    <div id="container">
      
      <!-- content div starts -->
      <div id="content">
        
        <!-- faq_paragraph starts -->
        <div id="about_us_paragraph">
          <h1> Track Your Order </h1>
          <p>Returns within 60 days of the order date will be accepted at Sports Buy retail stores for items that are unused/unworn, in original packaging, and with all paperwork, parts & accessories for credit of the purchase price minus shipping, handling, gift wrap, and other charges. Items marked as clearance may also be returned if the above criteria is met. </p><br />

          <p>Refunds will be in the same form of payment originally used for the purchase.  If PayPal is used for payment, the item may be returned to our Sports Buy retail store locations to receive a refund. PayPal returns at our Sports Buy retail store locations will credit the Debit or Credit Card attached to your PayPal account. </p>

          <p>The order Packing Slip (included with shipment) and e-Receipt are required for proof of purchase/price.</p>

        </div><!-- return_paragraph ends -->

        
        <!-- return_policy starts -->
        <div id="return_policy">
        <h2> The following items are required for returning items to a store for a full refund*</h2>

        <ul>
          <li> Photo ID matching Ship to/Bill to address</li>
          <li> The unused/unworn item in original packaging and with all paperwork, parts and accessories</li>
          <li> The order Packing Slip</li>
          <li> e-Receipt (printed copy or mobile device display)</li>
        </ul>
        <br />

        <p><strong>*If these are not provided the credit amount will be issued for a value equal to the item's lowest promotional price within the last 60 days</strong></p>
        <br />

        <p>Please refer to our Return Instructions when returning your item. 
           For returns to our warehouse, the postmark date of the return package 
           will be considered the return date. If you need to return an item after 
           60 days, <strong>please contact our Online Customer Service Department at 1-877-100-2675</strong></p>
        <br />

        <p><strong>Sports Buy is not responsible for shipping, handling, gift wrap, and other additional charges for returned items.</strong></p>

        </div> <!-- return_policy ends -->
   

      </div> <!-- content div ends -->


    </div><!-- Container ends -->


<?php include '../includes/footer.inc.php'; ?>