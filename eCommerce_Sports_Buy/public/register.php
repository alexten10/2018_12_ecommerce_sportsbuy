<?php

require '../config.php';
require '../functions.php';

use \classes\utility\validator;
$vldtr = new validator;

$title = 'New User Registration';
$active_page = 'register';

//check data coming to this webpage
//var_dump($_POST);


//testing if have POST method
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  
  // !!! order of calling functions is important !!!
  
  $vldtr->validateForSymbols('first_name');
  $vldtr->required('first_name');
  
  $vldtr->validateForSymbols('last_name');
  $vldtr->required('last_name');
  
  $vldtr->validateEmail('email'); //this is 1
  $vldtr->required('email'); //this is 2
  
  $vldtr->validatePhone('phone_number');
  $vldtr->required('phone_number');
  
  $vldtr->required('billing_province');
  
  $vldtr->validateForSymbols('billing_city');
  $vldtr->required('billing_city');
  
  $vldtr->validateStreetSymbols('billing_street');
  $vldtr->required('billing_street');
  
  $vldtr->validatePostalCode('billing_postal_code');
  $vldtr->required('billing_postal_code');
  
  $vldtr->required('shipping_province');
  
  $vldtr->validateForSymbols('shipping_city');
  $vldtr->required('shipping_city');
  
  $vldtr->validateStreetSymbols('shipping_street');
  $vldtr->required('shipping_street');
  
  $vldtr->validatePostalCode('shipping_postal_code');
  $vldtr->required('shipping_postal_code');
  
  // !!! order of calling functions is important !!!
  $vldtr->validatePasswordStrength('password');
  $vldtr->minLength('password', 6);//minimum required length of password is 6 characters
  $vldtr->maxLength('password', 12);//maximum required password length is 12 characters
  $vldtr->required('password');
  $vldtr->passwordsMatch('password', 'password_confirm');
  
  
  //if no errors found
  if(empty($vldtr->errors())) {
    
    
    //START check if email is unique (does not already exist in the db)
    //get all emails that exist in DB
    $emails = getCustomersEmails($dbh);
    
    //match DB emails with the typed in registration form
    foreach ($emails as $key=>$value) {
      //if found matching email in db, set variable $match_found 
      if(($_POST['email'] == $value['email'])) {
        $match_found = true; 
        break;
      } //END if
    }
    //END check if email is unique (is not already exists in the db)

    //if $match_found doesnt exists (the typed email is unique and doesnt exist in db) 
    if(!isset($match_found)) {
      $post = $_POST;
      $sanitized_post_array = sanitizeFormInputs($post); //do strip_tags() and htmlspecialchars()
      $insertNewCustomerIntoDB = insertCustomer($dbh, $sanitized_post_array); //insert sanitized data into DB, get true if inserted
      
      // if INSERT works
      if($insertNewCustomerIntoDB = true) {
        $id = getLastInsertedId($dbh);//get ID of the last inserted row to display info for the new registered user
        $customer = getCustomerById($dbh, $id);//get info about customer by id
      }// END if INSERT works. "if($stmt->execute())" starting line
      else{
        die ("Can't insert a new record");
      }//END else
    }//END if(!isset($match_found)) 
    
    
    //if $match_found exists (that shows entered email is already exists in db)
    else {
      $email_exist_msg = "Sorry, email has already been registered before.";
    }//END else
    
  }//END if no errors. "if(count($errors) == 0)" starting line
    
}//END if test for $_POST. "if($_SERVER['REQUEST_METHOD'] == 'POST')" starting line
$errors = $vldtr->errors();

?>
<?php include '../includes/header.inc.php'; ?>


    <div id="container">
    
      <?php if (empty($customer)) : ?>
      
        <h1><?=$title?></h1>
    
    
        <form method="post"
              action="register.php"
              id="customer_registration_info"
              name="customer_registration_info"
              accept-charset="utf-8" 
              novalidate="novalidate">
                  
          <fieldset>
            <legend><b>New Customer Info</b></legend>
            
            <p><span class="required_sign">*&nbsp;</span>- required field</p>
            
            <p>
              <label for="first_name"><span class="required_sign">*&nbsp;</span>First Name</label><!-- name of the form -->
              <!-- input field is "sticky"(if !empty $-POST...), which means if validation error appears, the typed in the field data will not be earased-->
              <input type="text"
                     id="first_name"
                     name="first_name"
                     maxlength="255"
                     value="<?php 
                              if(!empty($_POST['first_name'])){
                                echo esc ($_POST['first_name']);}
                            ?>"
                     placeholder="Type your first name" /><br />
              
              <!-- display error message near the field, if exists -->
              <?php if(!empty($errors['first_name'])) : ?>
                <span class="error"><?=$errors['first_name']?></span><br />
              <?php endif; ?>
            </p>


            <p>
              <label for="last_name"><span class="required_sign">*&nbsp;</span>Last Name</label>
              <input type="text"
                     id="last_name"
                     name="last_name"
                     maxlength="255"
                     value="<?php 
                              if(!empty($_POST['last_name'])){
                                echo esc ($_POST['last_name']);}
                            ?>"
                     placeholder="Type your last name" /><br />
                     
              <?php if(!empty($errors['last_name'])) : ?>
                <span class="error"><?php echo $errors['last_name']?></span><br />
              <?php endif; ?>
            </p>


            <p>
              <label for="email"><span class="required_sign">*&nbsp;</span>Email Address</label>
              <input type="email"
                     name="email"
                     id="email"
                     value="<?php 
                              if(!empty($_POST['email'])){
                                echo esc ($_POST['email']);}
                            ?>"
                     placeholder="your_email@example.com" /><br />
                     
              <?php if(!empty($errors['email'])) : ?>
                <span class="error"><?=$errors['email']?></span><br />
              <?php endif; ?>
              
              <!-- if email already is in db, show message -->
              <?php if(isset($email_exist_msg)) : ?>
                <span class="error"><?php echo $email_exist_msg ?></span><br />
              <?php endif; ?>
            </p>


            <p>
              <label for="phone_number"><span class="required_sign">*&nbsp;</span>Phone</label>
              <input type="tel"
                     id="phone_number"
                     name="phone_number"
                     maxlength="30"
                     value="<?php 
                              if(!empty($_POST['phone_number'])){
                                echo esc ($_POST['phone_number']);}
                            ?>"
                     placeholder="(000)222-3344"/><br />
                     
              <?php if(!empty($errors['phone_number'])) : ?>
                <span class="error"><?=$errors['phone_number']?></span><br />
              <?php endif; ?>
            </p>


            <!-- ****** START BILLING INFO ******-->
            <div class="address_info_form billing_info">
              <p class="text_align_center">Billing Information</p>
              
              <p class="select_form_tag">
                <label for="billing_province"><span class="required_sign">*&nbsp;</span>Province</label>
                <select name="billing_province">
                  <option value="">Select Billing Province</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Ontario') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Ontario">Ontario</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Quebec') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Quebec">Quebec</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'British Columbia') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="British Columbia">British Columbia</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Alberta') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Alberta">Alberta</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Nova Scotia') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Nova Scotia">Nova Scotia</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Saskatchewan') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Saskatchewan">Saskatchewan</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Manitoba') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Manitoba">Manitoba</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'New Bruncwick') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="New Bruncwick">New Bruncwick</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Newfoundland and Labrador') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Newfoundland and Labrador">Newfoundland and Labrador</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Prince Edward Iceland') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Prince Edward Iceland">Prince Edward Iceland</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Yukon') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Yukon">Yukon</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Northwest Territories') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Northwest Territories">Northwest Territories</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Nunavut') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Nunavut">Nunavut</option>
                  
                </select> 
              </p>
              <?php if(!empty($errors['billing_province'])) : ?>
                <span class="error"><?=$errors['billing_province']?></span>
              <?php endif; ?>


              <p>
                <label for="billing_city"><span class="required_sign">*&nbsp;</span>City</label>
                <input type="text"
                       id="billing_city"
                       name="billing_city"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['billing_city'])){
                                  echo esc ($_POST['billing_city']);}
                              ?>"
                       placeholder="Type your billing city" /><br />
                       
                <?php if(!empty($errors['billing_city'])) : ?>
                  <span class="error"><?=$errors['billing_city']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="billing_street"><span class="required_sign">*&nbsp;</span>Street</label>
                <input type="text"
                       id="billing_street"
                       name="billing_street"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['billing_street'])){
                                  echo esc ($_POST['billing_street']);}
                              ?>"
                       placeholder="Type your billing street" /><br />
                       
                <?php if(!empty($errors['billing_street'])) : ?>
                  <span class="error"><?=$errors['billing_street']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="billing_apartment_number">Apartment</label>
                <input type="text"
                       id="billing_apartment_number"
                       name="billing_apartment_number"
                       maxlength="10"
                       value="<?php 
                                if(!empty($_POST['billing_apartment_number'])){
                                  echo esc ($_POST['billing_apartment_number']);}
                              ?>"
                       placeholder="Type your billing apartment #" /><br />
                       
                <?php if(!empty($errors['billing_apartment_number'])) : ?>
                  <span class="error"><?=$errors['billing_apartment_number']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="billing_postal_code"><span class="required_sign">*&nbsp;</span>Postal Code</label>
                <input type="text"
                       id="billing_postal_code"
                       name="billing_postal_code"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['billing_postal_code'])){
                                  echo esc ($_POST['billing_postal_code']);}
                              ?>"
                       placeholder="Type your billing postal code" /><br />
                       
                <?php if(!empty($errors['billing_postal_code'])) : ?>
                  <span class="error"><?=$errors['billing_postal_code']?></span><br />
                <?php endif; ?>
              </p>
            </div><!-- END #billing_info -->
            <!-- ****** END BILLING INFO ******-->



            <!-- ****** START SHIPPING INFO ******-->
            <div class="address_info_form">
              <p class="text_align_center">Shipping Information</p>
              
              <p class="select_form_tag">
                <label for="shipping_province"><span class="required_sign">*&nbsp;</span>Province</label>
                <select name="shipping_province">
                  <option value="">Select Shipping Province</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Ontario') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Ontario">Ontario</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Quebec') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Quebec">Quebec</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'British Columbia') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="British Columbia">British Columbia</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Alberta') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Alberta">Alberta</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Nova Scotia') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Nova Scotia">Nova Scotia</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Saskatchewan') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Saskatchewan">Saskatchewan</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Manitoba') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Manitoba">Manitoba</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'New Bruncwick') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="New Bruncwick">New Bruncwick</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Newfoundland and Labrador') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Newfoundland and Labrador">Newfoundland and Labrador</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Prince Edward Iceland') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Prince Edward Iceland">Prince Edward Iceland</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Yukon') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Yukon">Yukon</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Northwest Territories') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Northwest Territories">Northwest Territories</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Nunavut') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Nunavut">Nunavut</option>
                  
                </select> 
              </p>
              <?php if(!empty($errors['shipping_province'])) : ?>
                <span class="error"><?=$errors['shipping_province']?></span>
              <?php endif; ?>


              <p>
                <label for="shipping_city"><span class="required_sign">*&nbsp;</span>City</label>
                <input type="text"
                       id="shipping_city"
                       name="shipping_city"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['shipping_city'])){
                                  echo esc ($_POST['shipping_city']);}
                              ?>"
                       placeholder="Type your shipping city" /><br />
                       
                <?php if(!empty($errors['shipping_city'])) : ?>
                  <span class="error"><?=$errors['shipping_city']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="shipping_street"><span class="required_sign">*&nbsp;</span>Street</label>
                <input type="text"
                       id="shipping_street"
                       name="shipping_street"
                       maxlength="255"
                       value="<?php 
                                if(!empty($_POST['shipping_street'])){
                                  echo esc ($_POST['shipping_street']);}
                              ?>"
                       placeholder="Type your shipping street" /><br />
                       
                <?php if(!empty($errors['shipping_street'])) : ?>
                  <span class="error"><?=$errors['shipping_street']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="shipping_apartment_number">Apartment</label>
                <input type="text"
                       id="shipping_apartment_number"
                       name="shipping_apartment_number"
                       maxlength="10"
                       value="<?php 
                                if(!empty($_POST['shipping_apartment_number'])){
                                  echo esc ($_POST['shipping_apartment_number']);}
                              ?>"
                       placeholder="Type your shipping apartment #" /><br />
                       
                <?php if(!empty($errors['shipping_apartment_number'])) : ?>
                  <span class="error"><?=$errors['shipping_apartment_number']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="shipping_postal_code"><span class="required_sign">*&nbsp;</span>Postal Code</label>
                <input type="text"
                       id="shipping_postal_code"
                       name="shipping_postal_code"
                       maxlength="10"
                       value="<?php 
                                if(!empty($_POST['shipping_postal_code'])){
                                  echo esc ($_POST['shipping_postal_code']);}
                              ?>"
                       placeholder="Type your shipping postal code" /><br />
                       
                <?php if(!empty($errors['shipping_postal_code'])) : ?>
                  <span class="error"><?=$errors['shipping_postal_code']?></span><br />
                <?php endif; ?>
              </p>
            </div><!-- END #shipping_info -->
            <!-- ****** END SHIPPING INFO ******-->


            <p>
              <label for="password"><span class="required_sign">*&nbsp;</span>Password</label>
              <input type="password"
                     id="password"
                     name="password"
                     maxlength="255"
                     value="<?php 
                              if(!empty($_POST['password'])){
                                echo esc ($_POST['password']);}
                            ?>"
                     placeholder="Set your password (min 6 characters)" /><br />
                     
              <?php if(!empty($errors['password'])) : ?>
                <span class="error"><?=$errors['password']?></span><br />
              <?php endif; ?>
            </p>


            <p>
              <label for="pass_confirm"><span class="required_sign">*&nbsp;</span>Confirm Password</label>
              <input type="password"
                     id="pass_confirm"
                     name="password_confirm"
                     maxlength="255"
                     value="<?php 
                              if(!empty($_POST['password_confirm'])){
                                echo esc ($_POST['password_confirm']);}
                            ?>"
                     placeholder="Type again your password" />
            </p>

          </fieldset>


          <!-- *******  BUTTONS ************-->
          <p id="form_submit_buttons">
            <input type="submit"
                   value="Register"
                   class="button" />&nbsp; &nbsp;
            <input type="button"
                   value="Clear Form"
                   onclick="clearForm(this.form);"
                   class="button" />
          </p>
          
          
          <!--****** SCRIPT FOR BUTTON "CLEAR FORM" *****  -->
          <script>
            //JS to clear 'input' fields
            function clearForm() {
              //clear input fields
              var tags = document.getElementsByTagName("input");
              for (i=0; i<tags.length; i++) {
                if(tags[i].type == "button" || tags[i].type == "submit"){
                  continue;
                }//END if
                tags[i].value = '';
              }//END for
              
              //clear 'select' fields
              var tagss = document.getElementsByTagName("select");
              for (i=0; i<tagss.length; i++) {
                if(tagss[i].type == "button" || tagss[i].type == "submit"){
                  continue;
                }//END if
                tagss[i].value = '';
              }//END for
              
            }//END clearForm()
          </script>
        
        </form>





      <?php else : ?>
        <div id="new_customer_summary">
          <h1>Thank you for registration</h1>
          
          <p>You submitted the following information:</p>
          
          <!-- output only specific results in array manually -->
          
          <ul>
            <li><strong>First Name:</strong> <?=$customer['first_name']?></li>
            <li><strong>Last Name:</strong> <?=$customer['last_name']?></li>
            <li><strong>Email:</strong> <?=$customer['email']?></li>
            <li><strong>Phone:</strong> <?=$customer['phone_number']?></li>
            <li><strong>Country:</strong> <?=$customer['country']?></li>
            
            <li><strong>Billing Province:</strong> <?=$customer['billing_province']?></li>
            <li><strong>Billing City:</strong> <?=$customer['billing_city']?></li>
            <li><strong>Billing Street:</strong> <?=$customer['billing_street']?></li>
            <?php if ($customer['billing_apartment_number'] !== '') : ?><li><strong>Billing Apartment:</strong> <?=$customer['billing_apartment_number']?></li><?php endif; ?>
            <li><strong>Billing Postal Code:</strong> <?=$customer['billing_postal_code']?></li>
            
            <li><strong>Shipping Province:</strong> <?=$customer['shipping_province']?></li>
            <li><strong>Shipping City:</strong> <?=$customer['shipping_city']?></li>
            <li><strong>Shipping Street:</strong> <?=$customer['shipping_street']?></li>
            <?php if(!empty($customer['shipping_apartment_number'])) : ?><li><strong>Shipping Apartment:</strong> <?=$customer['shipping_apartment_number']?></li><?php endif; ?>
            <li><strong>Shipping Postal Code:</strong> <?=$customer['shipping_postal_code']?></li>
          </ul>
          
          <p>Now you can <a href="login.php">login</a> using your email and password.</p>
        </div><!-- END #new_customer_summary -->
      <?php endif; ?>


    </div><!-- Container ends -->


<?php include '../includes/footer.inc.php'; ?>

















