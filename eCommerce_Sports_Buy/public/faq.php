<?php

require '../config.php';
require '../functions.php';

$title = 'FAQ';
$active_page = '';

?>

<?php include '../includes/header.inc.php'; ?>

    <div id="container">

      
      <!-- content div starts -->
      <div id="content">
        
        <!-- faq_paragraph starts -->
        <div id="about_us_paragraph">
          <h1> Help and FAQs </h1>
          <p><strong>Wondering when your order will arrive? You can review your order by signing in to My Account and viewing your Order History.</strong></p><br />

          <p>If you were unable to find your answer in the Help area of our website, select a topic below for more information. You will have the option to contact our Customer Service Department if the information does not answer your question. </p>

        </div><!-- faq_paragraph ends -->

        <div id="faq_questions">
        <h2>I have a question about an Item </h2>
        <h3>How do I find an item I'm looking for?</h3>

        <p><strong>There are multiple ways to find an item on SportsBuy website.</strong></p>

        <ul>
          <li>Search by entering a Keyword or Item Number in the Search box</li>
          <li>Browse our product catalogue by clicking the links found on each page.</li>
          <li>Ultilize the filter bar at the bottom of the screen, to narrow down your criteria</li>
        </ul>
        <br />

        <h3>How do I add items to my Shopping Cart?</h3>

        <p><strong>To add items to your Shopping Cart, follow these simple steps:</strong></p>

        <ul>
          <li>It may be necessary to choose a category and/or a price for the item you want to purchase. Click on the drop-down menu(s) to select a category and/or a price.</li>
          <li>Click on the menu(s) to select the quantity you would like to order.</li>
          <li>If a product is not listed in the drop-down menu(s), it is out of stock and is not available for purchase.</li>
          <li>Click "Add to Cart" to add the item to your Shopping Cart.</li>
          <li>Make note of the Availability of the item, by clicking on "Additional Information" for the item(s) located in your shopping cart.</li>
        </ul>

        <br />

        <h3>How can I make changes to my Shopping Cart?</h3>

        <p><strong>The following options are available in your Shopping Cart:</strong></p>

        <ul>
          <li><strong>Review your order:</strong> Review the details of your order before you proceed to Checkout.</li>
          <li><strong>Update order quantity:</strong>If you wish to update the order quantity of any item in your Shopping Cart, simply type the desired quantity next to the item and click "Update."</li>
          <li><strong>Remove an item from your Shopping Cart:</strong>
          Select the item and click "Remove Item" or change the order quantity to zero and click "Update."</li>
          <li><strong>Return to shopping:</strong>If you wish to continue shopping while viewing your Shopping Cart, click "Back to..." on the top left corner of the page. Items in your Shopping Cart will remain until they are purchased, removed or moved to your Wish List. At any time, you can return to you cart by clicking the Shopping Cart icon at the top right corner of the page.</li>
          <li><strong>Proceed to Checkout:</strong>When you are ready to purchase your order, click "Checkout" or "Checkout with PayPal."</li>
          <li><strong>Move an item to your Wish List:</strong>If there are items in your Shopping Cart that you wish to save but not purchase immediately, you can add them too.</li>
        </ul>

        <br />

        <h3>How do I know when an item will arrive?</h3>

        <p><strong>Delivery Time =  Handling Time + Shipping Time</strong></p>

        <p><strong>Handling Time:</strong> refers to the amount of time it will take for your order to leave a fulfillment centre.</p>
        <p><strong>Shipping Time:</strong> is the amount of time it will take your order to ship once it has left a fulfillment centre.</p>

        <p>The shipping method selected during Checkout will determine the time it will take the order to arrive after it has left one of our fulfillment centres.</p>

        <p><strong>Example:</strong> The item/s you ordered may takes 1 to 4 full business days to leave one of our fulfillment centres. It will take approximately 7-10 full business days for the item to arrive from the time it was ordered.</p> <br />
        <p><strong>Please note that business days are Monday to Friday, excluding statutory holidays.</strong></p>

        </div>
   

      </div> <!-- content div ends -->


    </div><!-- Container ends -->

    
<?php include '../includes/footer.inc.php'; ?>