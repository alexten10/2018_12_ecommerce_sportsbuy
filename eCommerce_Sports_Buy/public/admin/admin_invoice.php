<?php
/**
 * Invoice Admin
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------


$title = 'Admin Transactions, Shipping and Orders - List View';

// Include external config, model and header files
require  '../../config.php';
require '../../models/products.php';
include ('../../includes/admin_header.inc.php');

    /**
     * Get all of orders from a customer
     *
     * @param  shipping_status: shipping status if needed
     * @param  transaction_status: transaction status if needed
     * @return  array: an array of payment information
     */
    function retrieveOrders($pdo, $invoice_id, $shipping_status, $transaction_status) {
        $query = '';
      
        if (strlen($shipping_status)>0) {
          $shipping_status = intval($shipping_status);
          $query .= " AND (shipping_status = $shipping_status)";
        }

        if (strlen($transaction_status)>0) {
          $transaction_status = intval($transaction_status);
          $query .= " AND (transaction_status = $transaction_status)";
        }
      
        // The where condition for sql statement
        $condition = " WHERE (product_invoice.invoice_id = $invoice_id)";
      
        // Remove the first AND to prevent syntax error :)
        if (strlen($query)>0) {
          $condition .= substr($query, 5);
        }
      

        $stmt = $pdo->prepare("SELECT 
          process_id,
          product_invoice.process_id,
          product_invoice.product_id,
          product_invoice.invoice_id,
          product_invoice.quantity,
          product_invoice.weight,
          product_invoice.product_name,
          product_invoice.price,
          product_invoice.gst,
          product_invoice.pst,
          product_invoice.shipping_cost,
          product_invoice.subtotal,
          product_invoice.total,
          categories.category_name,
          suppliers.supplier_name,
          brands.brand_name,
          invoices.shipping_status
          FROM product_invoice 
          INNER JOIN invoices 
          ON product_invoice.invoice_id = invoices.invoice_id 
          INNER JOIN products ON products.product_id = product_invoice.product_id 
          INNER JOIN categories ON products.category_id = categories.category_id 
          INNER JOIN suppliers ON products.supplier_id = suppliers.supplier_id 
          INNER JOIN brands ON products.brand_id = brands.brand_id 
          $condition");
                             
        // Do it with customer id
        $stmt->execute();
        // Get the result
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Free all resource allocated
        $stmt = null;


        return $result;
                              
    }

if (!isset($_GET['invoice_id'])) {
  // Invalid request
  exit(0); 
}

// Retrieve all requests

$invoice_id = intval($_GET['invoice_id']);

$shipping_status = isset($_GET['shipping_status']) ? $_GET['shipping_status'] : '';
$transaction_status = isset($_GET['transaction_status']) ? $_GET['transaction_status'] : '';


// Fetch and save product details in variable
$order = retrieveOrders($dbh, $invoice_id, $shipping_status, $transaction_status);

?>   
<div id="wrapper">      
  <main id="content">
    <div style="min-height: 60px;">

      <h2 style="color: #fff; text-align: center; background-color: #222; height: 60px; padding-top: 30px; margin-top: 0;">Admin Panel</h2>

      <!-- Navigation for admin -->
      <div id="tables">
      <div id="tables_list">  
        <ul id="admin_tables">
            <li><a href="admin_dashboard.php"> &lt; Home </a></li>
            <li><a href="admin_invoices.php">Orders</a></li>
            <li><a href="../index.php">Live Site &gt; </a></li>
        </ul>
      </div><!-- /#tables_list -->
      </div><!-- /#tables -->
      <!-- Navigation for admin ends-->
      
    </div><!--/#admin_info -->
    
    <div id="product_table">   

      <table class="list_view">
        <tr><th>Product Id</th>
        <th>Name</th>
        <th>Category</th>
        <th>Brand</th>
        <th>Quantity</th>
        <th>Price</th>
        <th>Supplier</th>
        <th>GST</th>
        <th>PST</th>
        <th>Shipping Cost</th>
        <th>Subtotal</th>
        <th>Total</th>
        <th>Back</th></tr>    
      <tr>
        <!-- Fetch & display product details from array -->
        <?php foreach($order as $row) : ?>
   
            <td><?=$row['product_id']?></td>
            <td><?=$row['product_name']?></td>
            <td><?=$row['category_name']?></td>
            <td><?=$row['brand_name']?></td>
            <td><?=$row['quantity']?></td>
            <td>$<?=$row['price']?></td>
            <td><?=$row['supplier_name']?></td>
            <td><?=$row['gst']?></td>
            <td><?=$row['pst']?></td>
            <td><?=$row['shipping_cost']?></td>
            <td><?=$row['subtotal'];?></td>
            <td><?=$row['total'];?></td>
            <td><a style="text-decoration: none; color: #fff; font-weight: bold; padding: 10px; border-radius: 5px; width: 80px; background: #3498DB;" href="admin_invoices.php">Back</a></td>
            </tr>
        <?php endforeach; ?>      
      </table>
    </div>  
  </main>
  
<!-- Include external footer file -->
<?php
include('../../includes/admin_footer.inc.php');
?>
</div><!-- /#wrapper -->

