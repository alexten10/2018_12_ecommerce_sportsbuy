<?php

/**
 * Add Product
 * @add_product.php
 * @course Ecommerce Project, WDD 2018 Jan
 * @author Amarjeet Sharma <Sharmajeetamar31@gmail.com>
 * @created_at 2018-12-05
 **/

$title = "Add Product";


$success = false;
// Include external header & config file
require  '../../config.php';
include ('../../includes/admin_header.inc.php');

// Include config and functions files
require 'functions/functions.php';
require '../../models/products.php';


if(empty($_SESSION['admin_logged_in'])) {
  header ('Location: admin_login.php');
  die;
}

// Load validator classes
use classes\utility\validator;

$v = new validator();

//var_dump($product_details);

// Fetch categories from DB and save in a variable
$categories = adminCategories($dbh);

// Fetch suppliers from DB and save in a variable
$suppliers = adminSuppliers($dbh);

// Fetch brands from DB and save in a variable
$brands = adminBrands($dbh);

// Test for POST request
if($_SERVER['REQUEST_METHOD'] == 'POST') {

	$errors = [];
  
  // String validation functions  
  $v->validateForSymbols('product_name');
  
  // Check only numbers
  $v-> validateSku('SKU');
  $v-> validatePrice('price');
  $v-> validateNumber('units_sold');
  $v-> validateNumber('units_in_stock'); 
  $v->validateWeight('weight'); 
  
  // Validation for required fields
  $v->required('product_name');  
  $v->required('category_id');
  $v->required('weight');
  $v->required('brand_id');
  $v->required('SKU');
  $v->required('price');
  $v->required('supplier_id');
  $v->required('units_sold');
  $v->required('description');

	//var_dump($_POST);
    
	//if no errors in validation
	if(count($v->errors()) == 0){
    $deleted =0;
		//connect to mysql
		$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    //var_dump($_POST);
    
		// create query
		$query = "INSERT INTO 
		products 
    ( category_id, supplier_id, brand_id, product_name, weight, SKU, description, price, availability, units_sold, units_in_stock, full_image1, full_image2, full_image3, thumbnail_image)
		VALUES
		(:category_id, :supplier_id, :brand_id, :product_name, :weight, :SKU, :description, :price, :availability, :units_sold, :units_in_stock, :full_image1, :full_image2, :full_image3, :thumbnail_image )";
    
    //prepare query
    $stmt = $dbh->prepare($query); 
    
    // bind values
    $params = array(

      ':category_id' => $_POST['category_id'],
      ':supplier_id' => $_POST['supplier_id'],
      ':brand_id' => $_POST['brand_id'],
      ':product_name' => $_POST['product_name'],      
      ':weight' => $_POST['weight'],      
      ':SKU' => $_POST['SKU'],
      ':description' => $_POST['description'],
      ':price' => $_POST['price'],
      ':availability' => $_POST['availability'],
      ':units_sold' => $_POST['units_sold'],
      ':units_in_stock' => $_POST['units_in_stock'],
      ':full_image1' => $_POST['full_image1'],
      ':full_image2' => $_POST['full_image2'],
      ':full_image3' => $_POST['full_image3'],
      ':thumbnail_image' => $_POST['thumbnail_image'],
    );   
		
		//if insert successful,
		if($stmt->execute($params)){
      // set session to display "successful" message on edit product page
      $_SESSION['product_added'] = true;

      // Redirect user to the product list view
      header('Location: admin_products.php');
 
	} else {
    
    // set session to display "successful" message on edit product page
    $_SESSION['updated'] = true;
    $_SESSION['update_message'] = "There was a problem adding the record!";
      
    // Redirect user to the product list view
    header('Location: admin_products.php');
	}        
	
}//Endif no errors

$errors = $v->errors();
  
}// End test for POST
      
?> 

<div id="wrapper">
 <?php include('../../includes/admin_header.inc.php'); ?>
 <img src="images/website_logo.png" alt="logo" id="logo">
 
  <div id="tables">

      <main id="content">        
        <div id="contentform"> <!-- div for form start -->
          
          <?php if(empty($user)): ?>
          <h2>Add New Product</h2> <!-- Form heading -->          
          <form method="post" 
                action="add_product.php" 
                id="form" 
                name="form" 
                autocomplete="on"
                novalidate
                > <!-- Form start -->

            <fieldset> <!-- Fieldset for Product Details start -->
              <legend>Product Details</legend>   
              <img src="images/add_product1.png" id="add_new_product" alt="add_product" />
              <!-- source = https://www.iconspng.com/uploads/add-document/add-document.png -->
              <p>              
                <label class="label"
                       for="product_name">Name</label>
                <input type="text" 
                       id="product_name" 
                       name="product_name"
                       size="40"                       
                       value="<?php
                    if(!empty($_POST['product_name'])) {
                        echo esc($_POST['product_name']);
                    }
                ?>"/>
                <?php if(!empty($errors['product_name'])) :?>
                <span style="color: #f00"><small><?=esc($errors['product_name']);?></small></span>
                <?php endif; ?>
              </p>  

              <p>  
                <label class="label"
                       for="category_id">Category</label>    
                <select name="category_id" id="category_id"> <!-- Select list -->
                  <?php foreach($categories as $category) : ?>
                  <option value="<?=$category['category_id']?>"><?=$category['category_name']?></option>
                  <?php endforeach; ?>
                </select>               
              </p>


              <p>  
                <label class="label"
                       for="weight">Weight</label>
                <input type="text" 
                       id="weight" 
                       name="weight"
                       size="40"
                       value="<?php
                    if(!empty($_POST['weight'])) {
                        echo esc($_POST['weight']);
                    }
                ?>"/>
                <?php if(!empty($errors['weight'])) :?>
                <span style="color: #f00"><small><?=esc($errors['weight']);?></small></span>
                <?php endif; ?>                
              </p>

              <p>  
                <label class="label"
                       for="brand_id">Brand</label>    
                <select name="brand_id" id="brand_id"> <!-- Select list -->
                  <?php foreach($brands as $brand) : ?>
                  <option value="<?=$brand['brand_id']?>"><?=$brand['brand_name']?></option>
                  <?php endforeach; ?>
                </select>               
              </p>

              <p>  
                <label class="label"
                       for="SKU">SKU</label>
                <input type="text" 
                       id="SKU" 
                       name="SKU"
                       size="40"
                       value="<?php
                    if(!empty($_POST['SKU'])) {
                        echo esc($_POST['SKU']);
                    }
                ?>"/>
                <?php if(!empty($errors['SKU'])) :?>
                <span style="color: #f00"><small><?=esc($errors['SKU']);?></small></span>
                <?php endif; ?>                
              </p>

              <p>  
                <label class="label"
                       for="description">Description</label>
                <input type="text" 
                       id="description" 
                       name="description"
                       size="40"
                       value="<?php
                    if(!empty($_POST['description'])) {
                        echo esc($_POST['description']);
                    }
                ?>"/>
                <?php if(!empty($errors['description'])) :?>
                <span style="color: #f00"><small><?=esc($errors['description']);?></small></span>
                <?php endif; ?>                
              </p>

              <p>  
                <label class="label"
                       for="price">Price</label>
                <input type="text" 
                       id="price" 
                       name="price"
                       size="40"
                       value="<?php
                    if(!empty($_POST['price'])) {
                        echo esc($_POST['price']);
                    }
                ?>"/>
                <?php if(!empty($errors['price'])) :?>
                <span style="color: #f00"><small><?=esc($errors['price']);?></small></span>
                <?php endif; ?>                
              </p>
              
              <p>  
                <label class="label"
                       for="supplier_id">Supplier</label>    
                <select name="supplier_id" id="supplier_id"> <!-- Select list -->
                  <?php foreach($suppliers as $supplier) : ?>
                  <option value="<?=$supplier['supplier_id']?>"><?=$supplier['supplier_name']?></option>
                  <?php endforeach; ?>
                </select>               
              </p>

              <p>  
                <label class="label"
                       for="availability">Availability</label>    
                <select name="availability" id="availability"> <!-- Select list -->
                  <option value="0">0</option>
                  <option value="1">1</option>                
                </select>               
              </p>
              <p>  
                <label class="label"
                       for="units_sold">Units Sold</label>
                <input type="text" 
                       id="units_sold" 
                       name="units_sold"
                       value="<?php
                    if(!empty($_POST['units_sold'])) {
                        echo esc($_POST['units_sold']);
                    }
                ?>"/>
                <?php if(!empty($errors['units_sold'])) :?>
                <span style="color: #f00"><small><?=esc($errors['units_sold']);?></small></span>
                <?php endif; ?>                
              </p>
              <p>  
                <label class="label"
                       for="units_in_stock">Units Available</label>
                <input type="text" 
                       id="units_in_stock" 
                       name="units_in_stock"
                       value="<?php
                    if(!empty($_POST['units_in_stock'])) {
                        echo esc($_POST['units_in_stock']);
                    }
                ?>"/>
                <?php if(!empty($errors['units_in_stock'])) :?>
                <span style="color: #f00"><small><?=esc($errors['units_in_stock']);?></small></span>
                <?php endif; ?>                
              </p>
              <p>  
                <label class="label"
                       for="full_image1">Image Name</label>
                <input type="text" 
                       id="full_image1" 
                       name="full_image1"
                       value="<?php
                    if(!empty($_POST['full_image1'])) {
                        echo esc($_POST['full_image1']);
                    }
                ?>"/>
                <span style="color: #1A8850; font-weight: bold;"><small>(with extension)</small></span>
                <?php if(!empty($errors['full_image1'])) :?>
                <span style="color: #f00"><small><?=esc($errors['full_image1']);?></small></span>
                <?php endif; ?>                
              </p>

              <p>  
                <label class="label"
                       for="full_image2">Image Name</label>
                <input type="text" 
                       id="full_image2" 
                       name="full_image2"
                       value="<?php
                    if(!empty($_POST['full_image2'])) {
                        echo esc($_POST['full_image2']);
                    }
                ?>"/>
                <span style="color: #1A8850; font-weight: bold;"><small>(with extension)</small></span>
                <?php if(!empty($errors['full_image2'])) :?>
                <span style="color: #f00"><small><?=esc($errors['full_image2']);?></small></span>
                <?php endif; ?>                
              </p>

              <p>  
                <label class="label"
                       for="full_image3">Image Name</label>
                <input type="text" 
                       id="full_image3" 
                       name="full_image3"
                       value="<?php
                    if(!empty($_POST['full_image3'])) {
                        echo esc($_POST['full_image3']);
                    }
                ?>"/>
                <span style="color: #1A8850; font-weight: bold;"><small>(with extension)</small></span>
                <?php if(!empty($errors['full_image3'])) :?>
                <span style="color: #f00"><small><?=esc($errors['full_image3']);?></small></span>
                <?php endif; ?>                
              </p>

              <p>  
                <label class="label"
                       for="thumbnail_image">Image Name</label>
                <input type="text" 
                       id="thumbnail_image" 
                       name="thumbnail_image"
                       value="<?php
                    if(!empty($_POST['thumbnail_image'])) {
                        echo esc($_POST['thumbnail_image']);
                    }
                ?>"/>
                <span style="color: #1A8850; font-weight: bold;"><small>(with extension)</small></span>
                <?php if(!empty($errors['thumbnail_image'])) :?>
                <span style="color: #f00"><small><?=esc($errors['thumbnail_image']);?></small></span>
                <?php endif; ?>                
              </p>
     
            </fieldset><!-- Fieldset for Product Details end -->

            <p><!-- Buttons start -->
              <input type="submit" 
                     value="Add" 
                     id="submit"
                     class="button"/>&nbsp; &nbsp;
              <input type="reset" 
                     value="Clear Values" 
                     id="reset"
                     class="button"/>
            </p><!-- Buttons end -->
          </form> <!-- Form end -->
        <?php endif; ?>          
        </div> <!-- div for form end -->          
      </main>

<!-- Include external footer file -->
<?php
include('../../includes/admin_footer.inc.php');
?>
      
</div><!-- /#wrapper -->