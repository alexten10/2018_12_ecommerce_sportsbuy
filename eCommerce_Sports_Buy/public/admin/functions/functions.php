<?php

  /**
  *escaping fields for security
  *@param String $string - input field value
  *@return sanitized string
  */
//escape a string (result can be seen in source page)
function esc($string)
{
  return strip_tags(htmlspecialchars($string, ENT_QUOTES, 'UTF-8', false));
}


/**
* sanitize Input fields array
* @param array
* @return sanitized array
*/
function sanitizeFormInputs($array) {
  $result = array(); 

  foreach ($array as $key => $value) {
    //if an array inside another
    if (is_array($value)) { 
      $result[$key] = sanitizeFormInputs($value); 
    } else { 
      $result[$key] = strip_tags(htmlspecialchars($value, ENT_QUOTES, 'UTF-8', false));
    }//END if-else
  } //END foreach
  
   return $result;
}


/**
*get info about customer by typed (on login page) email from database
*@param $dbh - PDO database handle
*@param string $email_address - typed into email field string on login page
*@return array with info for specific email
*/
function getCustomerByEmail($dbh, $email_address)
{
  $query = "SELECT
              *
            FROM
              customers
            WHERE
              email = :email";
  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':email', $email_address, PDO:: PARAM_STR);
  $stmt->execute();
  return $stmt->fetch(PDO::FETCH_ASSOC);
  //if no customer found the return result will be an empty array
}

/**
*get info about customer by typed (on login page) email from database
*@param $dbh - PDO database handle
*@param string $email_address - typed into email field string on login page
*@return array with info for specific email
*/
function getAdminByEmail($dbh, $email_address)
{
  $query = "SELECT
              *
            FROM
              admins
            WHERE
              email = :email";
  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':email', $email_address, PDO:: PARAM_STR);
  $stmt->execute();
  return $stmt->fetch(PDO::FETCH_ASSOC);
  //if no customer found the return result will be an empty array
}


/**
*get all emails that exist in DB (register.php)
*@param $dbh - PDO database handle
*@return array with all emails
*/
function getCustomersEmails ($dbh)
{
  $query = 'SELECT
              email, customer_id
            FROM 
              customers
           ';
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  return $stmt->fetchAll(PDO::PARAM_STR);
}


/**
*insert into DB a new customer data (register.php)
*@param $dbh - PDO database handle
*@param array $post - $_POST sanitized array
*@return true
*/
function insertCustomer($dbh, $post)
{
  $query = "INSERT INTO
              customers
                (
                  first_name,
                  last_name,
                  email,
                  phone_number,
                  billing_province,
                  billing_city,
                  billing_street,
                  billing_apartment_number,
                  billing_postal_code,
                  shipping_province,
                  shipping_city,
                  shipping_street,
                  shipping_apartment_number,
                  shipping_postal_code,
                  password
                )
            VALUES
              (
                :first_name,
                :last_name,
                :email,
                :phone_number,
                :billing_province,
                :billing_city,
                :billing_street,
                :billing_apartment_number,
                :billing_postal_code,
                :shipping_province,
                :shipping_city,
                :shipping_street,
                :shipping_apartment_number,
                :shipping_postal_code,
                :password
              )";
  $stmt = $dbh->prepare($query);// prepare query (prepare SQL statement above to be executed)
  
  // bind (connecting) values of validated and sanitized input fields with database values
  $stmt->bindValue(':first_name', $post['first_name'], PDO:: PARAM_STR);
  $stmt->bindValue(':last_name', $post['last_name'], PDO:: PARAM_STR);
  $stmt->bindValue(':email', $post['email'], PDO:: PARAM_STR);
  $stmt->bindValue(':phone_number', $post['phone_number'], PDO:: PARAM_STR);
  $stmt->bindValue(':billing_province', $post['billing_province'], PDO:: PARAM_STR);
  $stmt->bindValue(':billing_city', $post['billing_city'], PDO:: PARAM_STR);
  $stmt->bindValue(':billing_street', $post['billing_street'], PDO:: PARAM_STR);
  $stmt->bindValue(':billing_apartment_number', $post['billing_apartment_number'], PDO:: PARAM_STR);
  $stmt->bindValue(':billing_postal_code', $post['billing_postal_code'], PDO:: PARAM_STR);
  $stmt->bindValue(':shipping_province', $post['shipping_province'], PDO:: PARAM_STR);
  $stmt->bindValue(':shipping_city', $post['shipping_city'], PDO:: PARAM_STR);
  $stmt->bindValue(':shipping_street', $post['shipping_street'], PDO:: PARAM_STR);
  $stmt->bindValue(':shipping_apartment_number', $post['shipping_apartment_number'], PDO:: PARAM_STR);
  $stmt->bindValue(':shipping_postal_code', $post['shipping_postal_code'], PDO:: PARAM_STR);
  $password_encrypted = password_hash($post['password'], PASSWORD_DEFAULT);//encrypt passwors before DB insert
  $stmt->bindValue(':password', $password_encrypted, PDO:: PARAM_STR);
  $stmt->execute();
  return true;
}


/**
 * Get the last inserted id in DB if possible
 * @param $dbh - PDO database handle
 * @return  string: last inserted id in DB
 */
function getLastInsertedId($dbh) {
    return $dbh->lastInsertId();
}


/**
 * Get all customer info by customer_id
 * @param $dbh - PDO database handle
 * @param string - id number
 * @return customer info
 */
function getCustomerById($dbh, $id)
{
  $query = "SELECT
              *
            FROM
              customers
            WHERE
              customer_id = :id
              AND deleted = 0";
  
  $stmt = $dbh->prepare($query);//prepare query (prepare SQL statement to be executed)
  $stmt->bindValue(':id', $id, PDO::PARAM_INT);//bind ID value to the query
  $stmt->execute();//execute query
  return $stmt->fetch(PDO::PARAM_STR);
}


/**
* Get all info from categories table
* @param $dbh - PDO database handle
* @return Array with all categories info
*/
function getCategories($dbh) {

  $query = "SELECT
              *
            FROM
              categories
           ";

  $stmt = $dbh->prepare($query);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


/**
* Get not deleted and the latest by created_at date 3 products from products table
* @param $dbh - PDO database handle
* @return Array with 3 products info
*/
function getNewArrivals($dbh) {

  $query = "SELECT
              *
            FROM
              products
            WHERE
              deleted = 0
            ORDER BY 
              created_at DESC
            LIMIT
              3
           ";

  $stmt = $dbh->prepare($query);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


/**
* Get max 5 trending brends from brands table
* @param $dbh - PDO database handle
* @return Array with max 5 trending brands info
*/
function getTrendingBrands($dbh) {

  $query = "SELECT
              *
            FROM
              brands
            WHERE
              trending = 1
            LIMIT
              5
           ";

  $stmt = $dbh->prepare($query);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


/**
* Get all not delteted products array
* @param $dbh - PDO database handle
* @return Array all products with info
*/
function getAllProducts ($dbh)
{
  $query = "SELECT
              *
            FROM
              products
            WHERE
              deleted = 0
            ";
  
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


/**
* Get all products by category
* @param $dbh - PDO database handle
* @param  string $category - id of category
* @return Array of all products in category
*/
function getProductsByCategory ($dbh, $category)
{
  $query = "SELECT 
              products.product_id,
              products.category_id,
              products.product_name,
              products.thumbnail_image,
              products.price,
              products.availability,
              categories.category_name
            FROM
              products
            JOIN 
              categories using(category_id)
            WHERE
              products.category_id = :category_id
            AND
              products.deleted = 0";
  
  $stmt = $dbh->prepare($query);
  $stmt->bindValue (':category_id', $category, PDO:: PARAM_STR);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


/**
* Get category name from DB category_id
* @param $dbh - PDO database handle
* @param  string $category - id of category
* @return Array with ['category_name'] index
*/
function getCategoryName ($dbh, $category)
{
  $query = "SELECT 
              category_name
            FROM
              categories
            WHERE
              category_id = :category_id
           ";
  
  $stmt = $dbh->prepare($query);
  $stmt->bindValue (':category_id', $category, PDO:: PARAM_STR);
  $stmt->execute();
  return $stmt->fetch(PDO::FETCH_ASSOC);
}


/**
* Get random products for default products.php page
* @param $dbh PDO database handle
* @param $limit Int - limited number of products
* @return Array result of 20 products($limit = 20)
*/
function getRandomProducts($dbh, $limit)
{
  $query = "SELECT
              *
            FROM
              products
            WHERE
              deleted = 0
            ORDER BY
              RAND()
            LIMIT
              :limit";

  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':limit', $limit, PDO::PARAM_INT);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


/**
* Get products by price range
* @param $dbh PDO database handle
* @param $price_min Int - min price of products
* @param $price_max Int - max price of products
* @return Array result of products in price range
*/
function getProductsByPriceRange($dbh, $price_min, $price_max)
{
    $query = "SELECT
                *
              FROM
                products
              WHERE
                deleted = 0
              AND
                price >= :min_price 
              AND 
                price < :max_price
             ";

  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':min_price', $price_min, PDO::PARAM_INT);
  $stmt->bindValue(':max_price', $price_max, PDO::PARAM_INT);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


/**
* Get brand name from DB brand_id
* @param $dbh - PDO database handle
* @param  string $brand - id of brand
* @return Array with ['brand_name'] index
*/
function getBrandName ($dbh, $brand)
{
  $query = "SELECT 
              brand_name
            FROM
              brands
            WHERE
              brand_id = :brand_id
           ";
  
  $stmt = $dbh->prepare($query);
  $stmt->bindValue (':brand_id', $brand, PDO:: PARAM_STR);
  $stmt->execute();
  return $stmt->fetch(PDO::FETCH_ASSOC);
}

/**
* Get supplier name from DB for add_supplier.php
* @param $dbh - PDO database handle
* @param  string $supplier - id of brand
* @return Array with ['supplier_name'] index
*/
function getSuppliers ($dbh)
{
  $query = "SELECT 
              *
            FROM
            suppliers";
  
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

/**
* Get brand name from DB for add_brand.php
* @param $dbh - PDO database handle
* @param  string $brand - id of brand
* @return Array with ['brand_name'] index
*/
function getBrands ($dbh)
{
  $query = "SELECT 
              *
            FROM
            brands";
  
  $stmt = $dbh->prepare($query);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}





/**
* Get all products by brand
* @param $dbh - PDO database handle
* @param  string $brand - id of brand
* @return Array of all products with specific brand_id
*/
function getProductsByBrand ($dbh, $brand)
{
  $query = "SELECT 
              products.product_id,
              products.brand_id,
              products.product_name,
              products.thumbnail_image,
              products.price,
              products.availability,
              brands.brand_name
            FROM
              products
            JOIN 
              brands using(brand_id)
            WHERE
              products.brand_id = :brand_id
            AND
              products.deleted = 0";
  
  $stmt = $dbh->prepare($query);
  $stmt->bindValue (':brand_id', $brand, PDO:: PARAM_STR);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

/**
*update customer info in DB (profile_edit.php)
*@param $dbh - PDO database handle
*@param array $sanitized_post_array - $_POST sanitized array
*@return true as success update
*/
function updateCustomerInfo($dbh, $sanitized_post_array, $current_date)
{
  $query = "UPDATE
              customers
            SET
              first_name = :first_name,
              last_name = :last_name,
              email = :email,
              phone_number = :phone_number,
              billing_province = :billing_province,
              billing_city = :billing_city,
              billing_street = :billing_street,
              billing_apartment_number = :billing_apartment_number,
              billing_postal_code = :billing_postal_code,
              shipping_province = :shipping_province,
              shipping_city = :shipping_city,
              shipping_street = :shipping_street,
              shipping_apartment_number = :shipping_apartment_number,
              shipping_postal_code = :shipping_postal_code,
              updated_at = :updated_at
            WHERE
              customer_id = :customer_id
           ";
  $stmt = $dbh->prepare($query);// prepare query (prepare SQL statement above to be executed)
  
  // bind (connecting) values of validated input fields with database values
  $stmt->bindValue(':first_name', $sanitized_post_array['first_name'], PDO:: PARAM_STR);
  $stmt->bindValue(':last_name', $sanitized_post_array['last_name'], PDO:: PARAM_STR);
  $stmt->bindValue(':email', $sanitized_post_array['email'], PDO:: PARAM_STR);
  $stmt->bindValue(':phone_number', $sanitized_post_array['phone_number'], PDO:: PARAM_STR);
  
  $stmt->bindValue(':billing_province', $sanitized_post_array['billing_province'], PDO:: PARAM_STR);
  $stmt->bindValue(':billing_city', $sanitized_post_array['billing_city'], PDO:: PARAM_STR);
  $stmt->bindValue(':billing_street', $sanitized_post_array['billing_street'], PDO:: PARAM_STR);
  $stmt->bindValue(':billing_apartment_number', $sanitized_post_array['billing_apartment_number'], PDO:: PARAM_STR);
  $stmt->bindValue(':billing_postal_code', $sanitized_post_array['billing_postal_code'], PDO:: PARAM_STR);
  
  $stmt->bindValue(':shipping_province', $sanitized_post_array['shipping_province'], PDO:: PARAM_STR);
  $stmt->bindValue(':shipping_city', $sanitized_post_array['shipping_city'], PDO:: PARAM_STR);
  $stmt->bindValue(':shipping_street', $sanitized_post_array['shipping_street'], PDO:: PARAM_STR);
  $stmt->bindValue(':shipping_apartment_number', $sanitized_post_array['shipping_apartment_number'], PDO:: PARAM_STR);
  $stmt->bindValue(':shipping_postal_code', $sanitized_post_array['shipping_postal_code'], PDO:: PARAM_STR);
  
  $stmt->bindValue(':updated_at', $current_date, PDO:: PARAM_STR);
  $stmt->bindValue(':customer_id', $sanitized_post_array['customer_id'], PDO:: PARAM_STR);
  
  $stmt->execute();
  return true;
}


//$query for 
/**
* Get search products by title
* @param $dbh PDO database handle
* @param $search String - search keyword
* @return Array search result
*/
function search($dbh, $search)
{
  $query = "SELECT
              *
            FROM
              products 
            WHERE
              product_name
            LIKE
              :search";

  $stmt = $dbh->prepare($query);
  $stmt->bindValue(':search', $search, PDO::PARAM_STR);
  $stmt->execute();
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


/**
*update customer password in DB (password_edit.php)
*@param $dbh - PDO database handle
*@param String $new_password - escaped $_POST['password']
*@param $current_date - current date
*@param $customer_id - int customer_id
*@return true as success update
*/
function updatePassword($dbh, $new_password, $current_date, $customer_id)
{
  $query = "UPDATE
              customers
            SET
              password = :password,
              updated_at = :updated_at
            WHERE
              customer_id = :customer_id
           ";
  $stmt = $dbh->prepare($query);// prepare query (prepare SQL statement above to be executed)
  
  // bind (connecting) values of validated input fields with database values
  $password_encrypted = password_hash($new_password, PASSWORD_DEFAULT);
  $stmt->bindValue(':password', $password_encrypted, PDO:: PARAM_STR);
  $stmt->bindValue(':updated_at', $current_date, PDO:: PARAM_STR);
  $stmt->bindValue(':customer_id', $customer_id, PDO:: PARAM_STR);
  
  $stmt->execute();
  return true;
}


/**
*send email to the customer with new password
*@param String $first_name - customer first name
*@param $email - customer email
*@param $new_password - customer new password
*@return true if success
*/
function sendEmail($first_name, $email, $new_password)
{
  $from='Admin <admin@sportsbuy.ca>';
  $headers = '';
  $headers .= "From: " . $from . "\n";
  
  $subject = "Your password is updated!";
  $message = "Hi " . $first_name . "<br>
              Your new password is: " . $new_password . " <br>"
              ;

  mail($email, $subject, $message, $headers);
  return true;
}







