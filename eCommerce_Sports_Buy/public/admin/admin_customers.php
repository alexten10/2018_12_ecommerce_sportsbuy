<?php

/**
 * Admin Customers
 * @admin_customers.php
 * @course Ecommerce Project, WDD 2018 Jan
 * @author Amarjeet Sharma <Sharmajeetamar31@gmail.com>
 * @created_at 2018-12-05
 **/

$title = 'Admin Customers - List View';


// Include external config, model and header files
require  '../../config.php';
require '../../models/products.php';
include ('../../includes/admin_header.inc.php');

if(empty($_SESSION['admin_logged_in'])) {
  header ('Location: admin_login.php');
  die;
}


$customers = getCustomers($dbh);

//var_dump($customers);
?>   
<div id="wrapper">      
  <main id="content">
    <div id="admin_info">

      <img src="images/website_logo.png" alt="logo" id="logo">
      <h2>Admin Panel</h2>  

      <!-- Navigation for admin -->
      <div id="tables">
      <div id="tables_list">  
        <ul id="admin_tables">
            <li><a href="admin_dashboard.php"> &lt; Home </a></li>
            <li><a href="admin_products.php"> Products </a></li>
            <li><a href="../index.php">Live Site &gt; </a></li>
            <li><a href="admin_logout.php" id="logout">Logout</a></li>
        </ul>
      </div><!-- /#tables_list -->
      </div><!-- /#tables -->
      <!-- Navigation for admin ends-->

      <!-- script to hide the flash messages -->
      <script>
        setTimeout(function() {
        $('#flash').fadeOut('slow');
        }, 2000);
      </script>
      

      <?php
      //if $_SESSION['customer_added'] exists, which comes form edit_product.php
      if(isset($_SESSION['customer_added'])){
        $_SESSION['customer_added'] =  
                              '<div id="flash"
                               style="color: #0b0; 
                               padding: 15px;
                               font-size: 16px;
                               margin: 0 auto;
                               margin-top: 10px;
                               margin-left: 385px;
                               width: 235px;
                               position: absolute;
                               border: 1px solid #0b0" >
                    <strong>Customer added successfully!</strong>
                  </div><br />';
        $flash_message_updated = $_SESSION['customer_added'];
        echo $flash_message_updated;
        unset ($_SESSION['customer_added']);
      }
      //if $_SESSION['profile_edited'] exists, which comes form edit_product.php
      if(isset($_SESSION['profile_edited'])){
        $_SESSION['profile_edited'] =  
                              '<div id="flash"
                               style="color: #0b0; 
                               padding: 15px;
                               font-size: 16px;
                               margin: 0 auto;
                               margin-top: 10px;
                               margin-left: 385px;
                               width: 278px;
                               position: absolute;
                               border: 1px solid #0b0" >
                    <strong>Profile details updated successfully!</strong>
                  </div><br />';
        $flash_message_updated = $_SESSION['profile_edited'];
        echo $flash_message_updated;
        unset ($_SESSION['profile_edited']);
      }
      //if $_SESSION['customer_deleted'] exists, which comes form edit_product.php
      if(isset($_SESSION['customer_deleted'])){
        $_SESSION['customer_deleted'] =  
                              '<div id="flash"
                               style="color: #0b0; 
                               padding: 15px;
                               font-size: 16px;
                               margin: 0 auto;
                               margin-top: 10px;
                               margin-left: 340px;
                               width: 297px;
                               position: absolute;
                               border: 1px solid #0b0" >
                    <strong>Customer details deleted successfully!</strong>
                  </div><br />';
        $flash_message_updated = $_SESSION['customer_deleted'];
        echo $flash_message_updated;
        unset ($_SESSION['customer_deleted']);
      }
      ?>

      <img src="images/customer_info.png" alt="customer info" id="customer_info" />
      <!-- source = http://masterchef.wikia.com/wiki/File:Website_under_construction.png -->
      
    </div><!--/#admin_info -->
    
    <div id="customers_table">    
    <div id="button">
    	<a href="add_customer.php">Add Customer</a>
    </div>
      <table class="list_view">
        <tr>
        <th>Customer ID</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Contact no.</th>
        <th>City</th>
        <th>Deleted</th>
        <th>Controls</th></tr>
        <tr>
          
        <!-- Get customer details from array -->  
        <?php foreach($customers as $row) : ?>

          <td><?=$row['customer_id']?></td>
          <td><?=$row['first_name']?></td>
          <td><?=$row['last_name']?></td>
          <td><?=$row['email']?></td>
          <td><?=$row['phone_number']?></td>
          <td><?=$row['billing_city']?></td>

          <?php if($row['deleted'] == 1) : ?>
              <td style="color: #bf0000;">Yes</td>
            <?php else : ?>
              <td style="color: #458B00;">No</td>
           <?php endif; ?>

          <td>
            <a href="edit_customer.php?customer_id=<?=$row['customer_id']?>">Edit</a>            
            <a href="delete_customer.php?customer_id=<?=$row['customer_id']?>">Delete</a>
          </td>
        </tr>
      <?php endforeach; ?>
        
      </table>
    </div><!-- /#customers_table -->  
  </main>

<!-- Include external footer file -->
<?php
include('../../includes/admin_footer.inc.php');
?>

</div><!-- /#customer_wrapper -->

