<?php

require '../../config.php';
require 'functions/functions.php';


use \classes\utility\validator;
$vldtr = new validator;


$title = '-- Administrator\'s Login Area --';

//check if $_POST comes to the page
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  //validate input fields
  $vldtr->validateEmail('email'); //this is 1
  $vldtr->required('email'); //this is 2
  $vldtr->required('password');
  
  
  //if no validation errors found
  if(empty($vldtr->errors())) {
    
    //sanitize input
    $post = $_POST;


    $sanitized_post_array = sanitizeFormInputs($post);


    //check if entered email exists in db
    $admin_info = getAdminByEmail($dbh, $sanitized_post_array['email']);
    //to check if query works properly and array has all info about specific customer


    
    //compare typed password with the password stored in database
    /// 1 if passwords match (remember if there is no customer were found before, it will compare typed password against empty array)
    if(password_verify($sanitized_post_array['password'], $admin_info['password'])) {
      $greeting = "Welcome, {$admin_info['first_name']} {$admin_info['last_name']}!";
      $_SESSION['success'] = $greeting;//if email and password match, set $_SESSION['success'] to exist
      $_SESSION['admin_logged_in'] = true;

      session_regenerate_id();//regenerate session id
      header('Location: admin_dashboard.php');//redirect to profile.php
      die;//if redirect, then die (stop) this page loading
    }//END if
    /// 2 if passwords dont match OR no info found in database about user by email
    else {
      $_SESSION['no_success'] = 'Sorry, credentials don\'t match!';
      $flash_message_no_success = $_SESSION['no_success'];//set error message
      unset($_SESSION['no_success']);
    }//END else
    
  }//END if(empty($vldtr->errors())) 
  
}//END if($_SERVER['REQUEST_METHOD'] == 'POST')

$errors = $vldtr->errors();
?>

<!-- script to hide the flash messages -->
      <script>
        setTimeout(function() {
        $('#flash').fadeOut('slow');
        }, 2000);
      </script>


<?php
//if $_SESSION['logged_out'] exists, which comes form logout.php
if(isset($_SESSION['admin_logged_out'])){
  $_SESSION['admin_logged_out'] =  
                        '<div id="flash"
                         style="color: #0b0; 
                         padding: 15px;
                         font-size: 16px;
                         margin: 0 auto;
                         margin-top: 20px;
                         margin-left: 350px;
                         width: 265px;
                         position: absolute;
                         border: 1px solid #0b0" >
              <strong>You have logged out successfully!</strong>
            </div><br />';
  $flash_message_logout = $_SESSION['admin_logged_out'];
  unset ($_SESSION['admin_logged_out']);
}
?>

<div id="wrapper">
 <?php include('../../includes/admin_header.inc.php'); ?>
    <div id="container" style="min-height: 600px;">
      
      <div id="tables">
        <h2><?php echo $title ?></h2>
      </div>
      
      <!-- display error msg if wrong login or password-->
      <?php if(!empty($flash_message_no_success)) echo "<h2 id=\"flash_message_no_success\">$flash_message_no_success</h2>"; ?>
      
      <!--if logout, show message for success log out-->
      <?php if(!empty($flash_message_logout)) echo "<h2 id=\"flash_message_success\">$flash_message_logout</h2>"; ?>
      
      <img src="images/login.png" alt="login">
      <div id="login">
        
        <form method="post"
              action="admin_login.php"
              id="login"
              name="login"
              accept-charset="utf-8" 
              novalidate="novalidate">

          <fieldset>
            <legend><b>LogIn</b></legend>

            <p>
              <label for="email">Email Address</label>
              <input type="email"
                     name="email"
                     id="email"
                     value="<?php 
                              if (!empty($_POST['email'])) {
                                echo esc ($_POST['email']);
                              }
                            ?>"
                     placeholder="Enter your email" /><br />
                     
                     <!-- display errors if exist -->
                    <?php if(!empty($errors['email'])) : ?>
                      <span class="error"><?=$errors['email']?></span><br />
                    <?php endif; ?>
            </p>

            <p>
              <label for="pass">Password</label>
              <input type="password"
                     id="pass"
                     name="password"
                     maxlength="255"
                     value=""
                     placeholder="Enter your password" /><br />
                     
              <?php if(!empty($errors['password'])) : ?>
                <span class="error"><?=$errors['password']?></span><br />
              <?php endif; ?>
            </p>

          </fieldset>
        

          <p id="form_submit_buttons">
            <input type="submit" value="Login" class="button" />&nbsp; &nbsp;
            <input type="button" value="Clear Form" class="button" onclick="clearForm(this.form);" />
          </p>

          <script>
            //JS to clear input fields
            function clearForm() {
              var tags = document.getElementsByTagName("input");
              for (i=0; i<tags.length; i++) {
                if(tags[i].type == "button" || tags[i].type == "submit"){
                  continue;
                }//END if
                tags[i].value = '';
              }//END for
            }//END clearForm()
          </script>

        </form>

      </div><!-- END #login_form -->
    </div><!-- Container ends -->
    <!-- Include external footer file -->
<?php
include('../../includes/admin_footer.inc.php');
?>

</div><!-- Wrapper ends -->

