<?php

/**
 * Add Customer
 * @add_customer.php
 * @course Ecommerce Project, WDD 2018 Jan
 * @author Amarjeet Sharma <Sharmajeetamar31@gmail.com>
 * @created_at 2018-12-09
 **/

$title = "Edit Customer";
$success = false;

// Include external header & config file
require  '../../config.php';
include ('../../includes/admin_header.inc.php');

// Include config and functions files
require 'functions/functions.php';
require '../../models/products.php';


if(empty($_SESSION['admin_logged_in'])) {
  header ('Location: admin_login.php');
  die;
}


// Load validator classes
use classes\utility\validator;

$vldtr = new validator;

// Fetch customer details from DB and save in a variable
if(!empty($_GET['customer_id'])) {
  $customer_details = getCustomerById($dbh, $_GET['customer_id']);
}



//testing if have POST method
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  
  // !!! order of calling functions is important !!!
  
  $customer_details = $_POST;

  $current_date = date('Y-m-d h:m:s');
  
  $vldtr->validateForSymbols('first_name');
  $vldtr->required('first_name');
  
  $vldtr->validateForSymbols('last_name');
  $vldtr->required('last_name');
  
  $vldtr->validateEmail('email'); //this is 1
  $vldtr->required('email'); //this is 2
  
  $vldtr->validatePhone('phone_number');
  $vldtr->required('phone_number');
  
  $vldtr->required('billing_province');
  
  $vldtr->validateForSymbols('billing_city');
  $vldtr->required('billing_city');
  
  $vldtr->validateStreetSymbols('billing_street');
  $vldtr->required('billing_street');

  if (!empty ($_POST['billing_apartment_number'])) {
    $vldtr->validateAppartmentNumber('billing_apartment_number');
  }
  
  $vldtr->validatePostalCode('billing_postal_code');
  $vldtr->required('billing_postal_code');
  
  $vldtr->required('shipping_province');
  
  $vldtr->validateForSymbols('shipping_city');
  $vldtr->required('shipping_city');
  
  $vldtr->validateStreetSymbols('shipping_street');
  $vldtr->required('shipping_street');

  if (!empty ($_POST['shipping_apartment_number'])) {
    $vldtr->validateAppartmentNumber('shipping_apartment_number');
  }
  
  $vldtr->validatePostalCode('shipping_postal_code');
  $vldtr->required('shipping_postal_code');
  

  
  //if no errors found
  if(empty($vldtr->errors())) {
    
    
    //START check if email is unique (does not already exist in the db)
    //get all emails that exist in DB
    $emails = getCustomersEmails($dbh);

    //match DB emails with the typed in registration form
    foreach ($emails as $key=>$value) {
      //if found matching email in db, set variable $match_found 
     if($value['customer_id'] !== $_POST['customer_id']) {
        //if found matching email in db, set variable $match_found as flag
        if(($_POST['email'] == $value['email'])) {
          $match_found = true; 
          break;
        }//END if(($_POST['email']
      }//END if($value['customer_id']
    }
    //END check if email is unique (is not already exists in the db)

    //if $match_found doesnt exists (the typed email is unique and doesnt exist in db) 
    if(!isset($match_found)) {
      $post = $_POST;
      $sanitized_post_array = sanitizeFormInputs($post); //do strip_tags() and htmlspecialchars()
      $updatedCustomerInfoInDB = updateCustomerInfo($dbh, $sanitized_post_array, $current_date); //update DB with new data, get true if inserted
      
      /* if UPDATE(updateCustomerInfo function) success, redirect to profile.php page,
       * where the new info is displayed with a flash confirmation msg*/
      if($updatedCustomerInfoInDB = true) {
        $_SESSION['profile_edited'] = true;
        header('Location: admin_customers.php');
        die;
      }// END if UPDATE(updateCustomerInfo function) works
      else{
        $customer_not_edited = 'Can not edit profile';
      }//END else
    }//END if(!isset($match_found)) 
    
    
    //if $match_found exists (that shows entered email is already exists in db)
    else {
      $email_exist_msg = "Sorry, email has already been registered before.";
    }//END else
    
  }//END if no errors. "if(count($errors) == 0)" starting line
    
}//END if test for $_POST. "if($_SERVER['REQUEST_METHOD'] == 'POST')" starting line
$errors = $vldtr->errors();
?>

<div id="container">
      
        <h1><?=$title?></h1>

        <h2><a href="admin_pswd_change_for_cust.php?customer_id=<?php echo $customer_details['customer_id'] ?>">Change Password</a></h2>
        
        <form method="post"
              action="edit_customer.php"
              id="customer_registration_info"
              name="customer_registration_info"
              accept-charset="utf-8" 
              novalidate="novalidate">
                  
          <fieldset>
            <legend><b>Edit Customer Info</b></legend>
            
            <p><span class="required_sign">*&nbsp;</span>- required field</p>
            
            <p>
              <label for="first_name"><span class="required_sign">*&nbsp;</span>First Name</label><!-- name of the form -->
              <!-- input field is "sticky"(if !empty $-POST...), which means if validation error appears, the typed in the field data will not be earased-->
              <input type="text"
                     id="first_name"
                     name="first_name"
                     maxlength="255"
                     value="<?=$customer_details['first_name']?>"/>
                <br />
              
              <!-- display error message near the field, if exists -->
              <?php if(!empty($errors['first_name'])) : ?>
                <span class="error"><?=$errors['first_name']?></span><br />
              <?php endif; ?>
            </p>


            <p>
              <label for="last_name"><span class="required_sign">*&nbsp;</span>Last Name</label>
              <input type="text"
                     id="last_name"
                     name="last_name"
                     maxlength="255"
                     value="<?=$customer_details['last_name']?>" /><br />
                     
              <?php if(!empty($errors['last_name'])) : ?>
                <span class="error"><?php echo $errors['last_name']?></span><br />
              <?php endif; ?>
            </p>


            <p>
              <label for="email"><span class="required_sign">*&nbsp;</span>Email Address</label>
              <input type="email"
                     name="email"
                     id="email"
                     value="<?=$customer_details['email']?>" /><br />
                     
              <?php if(!empty($errors['email'])) : ?>
                <span class="error"><?=$errors['email']?></span><br />
              <?php endif; ?>
              
              <!-- if email already is in db, show message -->
              <?php if(isset($email_exist_msg)) : ?>
                <span class="error"><?php echo $email_exist_msg ?></span><br />
              <?php endif; ?>
            </p>


            <p>
              <label for="phone_number"><span class="required_sign">*&nbsp;</span>Phone</label>
              <input type="tel"
                     id="phone_number"
                     name="phone_number"
                     maxlength="30"
                     value="<?=$customer_details['phone_number']?>"/><br />
                     
              <?php if(!empty($errors['phone_number'])) : ?>
                <span class="error"><?=$errors['phone_number']?></span><br />
              <?php endif; ?>
            </p>


            <!-- ****** START BILLING INFO ******-->
            <div class="address_info_form billing_info">
              <p class="text_align_center">Billing Information</p>
              
              <p class="select_form_tag">
                <label for="billing_province"><span class="required_sign">*&nbsp;</span>Province</label>
                <select name="billing_province">
                  <option value=""><?=$customer_details['billing_province']?></option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Ontario') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Ontario">Ontario</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Quebec') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Quebec">Quebec</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'British Columbia') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="British Columbia">British Columbia</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Alberta') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Alberta">Alberta</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Nova Scotia') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Nova Scotia">Nova Scotia</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Saskatchewan') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Saskatchewan">Saskatchewan</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Manitoba') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Manitoba">Manitoba</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'New Bruncwick') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="New Bruncwick">New Bruncwick</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Newfoundland and Labrador') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Newfoundland and Labrador">Newfoundland and Labrador</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Prince Edward Iceland') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Prince Edward Iceland">Prince Edward Iceland</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Yukon') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Yukon">Yukon</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Northwest Territories') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Northwest Territories">Northwest Territories</option>
                  
                  <option 
                    <?php if(isset($_POST['billing_province'])) : ?>
                      <?php if($_POST['billing_province'] == 'Nunavut') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Nunavut">Nunavut</option>
                  
                </select> 
              </p>
              <?php if(!empty($errors['billing_province'])) : ?>
                <span class="error"><?=$errors['billing_province']?></span>
              <?php endif; ?>


              <p>
                <label for="billing_city"><span class="required_sign">*&nbsp;</span>City</label>
                <input type="text"
                       id="billing_city"
                       name="billing_city"
                       maxlength="255"
                       value="<?=$customer_details['billing_city']?>" /><br />
                       
                <?php if(!empty($errors['billing_city'])) : ?>
                  <span class="error"><?=$errors['billing_city']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="billing_street"><span class="required_sign">*&nbsp;</span>Street</label>
                <input type="text"
                       id="billing_street"
                       name="billing_street"
                       maxlength="255"
                       value="<?=$customer_details['billing_street']?>" /><br />
                       
                <?php if(!empty($errors['billing_street'])) : ?>
                  <span class="error"><?=$errors['billing_street']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="billing_apartment_number">Apartment</label>
                <input type="text"
                       id="billing_apartment_number"
                       name="billing_apartment_number"
                       maxlength="10"
                       value="<?=$customer_details['billing_apartment_number']?>" /><br />
                       
                <?php if(!empty($errors['billing_apartment_number'])) : ?>
                  <span class="error"><?=$errors['billing_apartment_number']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="billing_postal_code"><span class="required_sign">*&nbsp;</span>Postal Code</label>
                <input type="text"
                       id="billing_postal_code"
                       name="billing_postal_code"
                       maxlength="255"
                       value="<?=$customer_details['billing_postal_code']?>" /><br />
                       
                <?php if(!empty($errors['billing_postal_code'])) : ?>
                  <span class="error"><?=$errors['billing_postal_code']?></span><br />
                <?php endif; ?>
              </p>
            </div><!-- END #billing_info -->
            <!-- ****** END BILLING INFO ******-->



            <!-- ****** START SHIPPING INFO ******-->
            <div class="address_info_form">
              <p class="text_align_center">Shipping Information</p>
              
              <p class="select_form_tag">
                <label for="shipping_province"><span class="required_sign">*&nbsp;</span>Province</label>
                <select name="shipping_province">
                  <option value=""><?=$customer_details['shipping_province']?></option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Ontario') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Ontario">Ontario</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Quebec') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Quebec">Quebec</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'British Columbia') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="British Columbia">British Columbia</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Alberta') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Alberta">Alberta</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Nova Scotia') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Nova Scotia">Nova Scotia</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Saskatchewan') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Saskatchewan">Saskatchewan</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Manitoba') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Manitoba">Manitoba</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'New Bruncwick') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="New Bruncwick">New Bruncwick</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Newfoundland and Labrador') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Newfoundland and Labrador">Newfoundland and Labrador</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Prince Edward Iceland') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Prince Edward Iceland">Prince Edward Iceland</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Yukon') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Yukon">Yukon</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Northwest Territories') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Northwest Territories">Northwest Territories</option>
                  
                  <option 
                    <?php if(isset($_POST['shipping_province'])) : ?>
                      <?php if($_POST['shipping_province'] == 'Nunavut') : ?> selected <?php endif; ?> 
                    <?php endif; ?>
                  value="Nunavut">Nunavut</option>
                  
                </select> 
              </p>
              <?php if(!empty($errors['shipping_province'])) : ?>
                <span class="error"><?=$errors['shipping_province']?></span>
              <?php endif; ?>


              <p>
                <label for="shipping_city"><span class="required_sign">*&nbsp;</span>City</label>
                <input type="text"
                       id="shipping_city"
                       name="shipping_city"
                       maxlength="255"
                       value="<?=$customer_details['shipping_city']?>" /><br />
                       
                <?php if(!empty($errors['shipping_city'])) : ?>
                  <span class="error"><?=$errors['shipping_city']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="shipping_street"><span class="required_sign">*&nbsp;</span>Street</label>
                <input type="text"
                       id="shipping_street"
                       name="shipping_street"
                       maxlength="255"
                       value="<?=$customer_details['shipping_street']?>" /><br />
                       
                <?php if(!empty($errors['shipping_street'])) : ?>
                  <span class="error"><?=$errors['shipping_street']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="shipping_apartment_number">Apartment</label>
                <input type="text"
                       id="shipping_apartment_number"
                       name="shipping_apartment_number"
                       maxlength="10"
                       value="<?=$customer_details['shipping_apartment_number']?>" /><br />
                       
                <?php if(!empty($errors['shipping_apartment_number'])) : ?>
                  <span class="error"><?=$errors['shipping_apartment_number']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="shipping_postal_code"><span class="required_sign">*&nbsp;</span>Postal Code</label>
                <input type="text"
                       id="shipping_postal_code"
                       name="shipping_postal_code"
                       maxlength="10"
                       value="<?=$customer_details['shipping_postal_code']?>" /><br />
                       
                <?php if(!empty($errors['shipping_postal_code'])) : ?>
                  <span class="error"><?=$errors['shipping_postal_code']?></span><br />
                <?php endif; ?>
              </p>
            </div><!-- END #shipping_info -->
            <!-- ****** END SHIPPING INFO ******-->

            <input type="hidden" name="customer_id" value="<?=$customer_details['customer_id']?>" />


          </fieldset>


          <!-- *******  BUTTONS ************-->
          <p id="form_submit_buttons">
            <input type="submit"
                   value="Update"
                   class="button" />&nbsp; &nbsp;
            <input type="button"
                   value="Clear Form"
                   onclick="clearForm(this.form);"
                   class="button" />
          </p>
          
          
          <!--****** SCRIPT FOR BUTTON "CLEAR FORM" *****  -->
          <script>
            //JS to clear 'input' fields
            function clearForm() {
              //clear input fields
              var tags = document.getElementsByTagName("input");
              for (i=0; i<tags.length; i++) {
                if(tags[i].type == "button" || tags[i].type == "submit"){
                  continue;
                }//END if
                tags[i].value = '';
              }//END for
              
              //clear 'select' fields
              var tagss = document.getElementsByTagName("select");
              for (i=0; i<tagss.length; i++) {
                if(tagss[i].type == "button" || tagss[i].type == "submit"){
                  continue;
                }//END if
                tagss[i].value = '';
              }//END for
              
            }//END clearForm()
          </script>
        
        </form>

        <!-- Include external footer file -->
<?php
include('../../includes/admin_footer.inc.php');
?>

    </div><!-- /#container -->

