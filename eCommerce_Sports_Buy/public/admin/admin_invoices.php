<?php
/**
 * Invoice Admin
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------


$title = 'Admin Transactions, Shipping and Orders - List View';

// Include external config, model and header files
require  '../../config.php';
require '../../models/products.php';
include ('../../includes/admin_header.inc.php');

    /**
     * toggle_shipping_status, toggle a shipping status
     *
     * @param  process_id: id of a product_invoice
     * @return  nothing
     */
    function toggle_shipping_status($pdo, $invoice_id) {
          try {
            
            // toggle a shipping status 
            $stmt = $pdo->prepare("UPDATE `invoices`
            SET
            shipping_status = ! shipping_status
            WHERE `invoice_id` = :invoice_id
            ");

            $stmt->bindParam(':invoice_id', $invoice_id, PDO::PARAM_INT);

            // Do it 
            $stmt->execute();

            // Free all resource allocated
            $stmt = null;

          } catch (Exception $e) {
            // There is a problem in database
            die($e->getMessage());
          }

    }

    // --------------------------------------------------------------------

    /**
     * toggle_shipping_status, toggle a shipping status
     *
     * @param  process_id: id of a product_invoice
     * @return  nothing
     */
    function toggle_transaction_status($pdo, $invoice_id) {
          try {
            // toggle a transaction status 
            $stmt = $pdo->prepare("UPDATE `invoices`
            SET
            transaction_status = ! transaction_status
            WHERE `invoice_id` = :invoice_id
            ");

            $stmt->bindParam(':invoice_id', $invoice_id, PDO::PARAM_INT);

            // Do it 
            $stmt->execute();

            // Free all resource allocated
            $stmt = null;

          } catch (Exception $e) {
            // There is a problem in database
            die($e->getMessage());
          }

    }

    /**
     * Get all of orders from a customer
     *
     * @param  shipping_status: shipping status if needed
     * @param  transaction_status: transaction status if needed
     * @return  array: an array of payment information
     */
    function retrieveOrders($pdo, $shipping_status, $transaction_status) {
        $query = '';
      
        if (strlen($shipping_status)>0) {
          $shipping_status = intval($shipping_status);
          $query .= " AND (shipping_status = $shipping_status)";
        }

        if (strlen($transaction_status)>0) {
          $transaction_status = intval($transaction_status);
          $query .= " AND (transaction_status = $transaction_status)";          
        }
      
        // Remove the first AND to prevent syntax error :)
        if (strlen($query)>0) {
          $query = ' WHERE ' . substr($query, 5);
        }
      
        $stmt = $pdo->prepare("SELECT 
          `invoices`.`invoice_id`,
          `invoice_date`,
          `invoices`.`customer_id`,
          `first_name`,
          `last_name`,
          `email`,
          `phone_number`,
          `billing_city`,
          `billing_street`,
          `billing_apartment_number`,
          `billing_postal_code`,
          `billing_province`,
          `shipping_province`,
          `shipping_city`,
          `shipping_street`,
          `shipping_apartment_number`,
          `shipping_postal_code`,
          `shipping_status`,
          `transaction_status`,
          `created_at`,
          `updated_at`,
          `deleted`
          FROM invoices 
          $query");
                             
        // Do it with customer id
        $stmt->execute();
        // Get the result
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Free all resource allocated
        $stmt = null;


        return $result;
                              
    }

$shipping_status = isset($_GET['shipping_status']) ? $_GET['shipping_status'] : '';
$transaction_status = isset($_GET['transaction_status']) ? $_GET['transaction_status'] : '';

if (isset($_GET['toggle_shipping_status'])) {
  toggle_shipping_status($dbh, $_GET['toggle_shipping_status']);
}
  
if (isset($_GET['toggle_transaction_status'])) {
  toggle_transaction_status($dbh, $_GET['toggle_transaction_status']);
}
  
// Fetch and save product details in variable
$order = retrieveOrders($dbh, $shipping_status, $transaction_status);


$shippingsortopt = [
  'All' => '',
  'Shipped' => "1", 
  'Pending' => "0"
];
// List out all shippingsortopt
$shippingsortlist = '';

if (count($shippingsortopt)>0) {
  $shippingsortlist .= '<select id="shippingsortlistid" name="shippingsortlist" style="text-decoration: none; color: #fff; font-weight: bold; padding: 10px; border-radius: 5px; height: 38px; background: #999; font-size: 14px; box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);"  >';
  foreach ($shippingsortopt as $key => $value) {
    if ($shipping_status === $value) {
      $shippingsortlist .= "<option value=\"$value\" selected>$key</option>";
    } else {
      $shippingsortlist .= "<option value=\"$value\">$key</option>";
    }
  }
  $shippingsortlist .= '</select>';
}


$transsortopt = [
  'All' => '',
  'Transaction OK' => "1", 
  'Transaction Incomplete' => "0"
];
// List out all transsortopt
$transsortlist = '';

if (count($transsortopt)>0) {
  $transsortlist .= '<select id="transsortlistid" name="transsortlist" style="text-decoration: none; color: #fff; font-weight: bold; padding: 10px; border-radius: 5px; height: 38px; background: #999; font-size: 14px; box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);"  >';
  foreach ($transsortopt as $key => $value) {
    if ($transaction_status === $value) {
      $transsortlist .= "<option value=\"$value\" selected>$key</option>";
    } else {
      $transsortlist .= "<option value=\"$value\">$key</option>";
    }
  }
  $transsortlist .= '</select>';
}


?>   
<script>
  function doTheFilter(shippingsortlistid, transsortlistid) {
    shippingsortlistid = document.getElementById("shippingsortlistid");
    sid = shippingsortlistid.options[shippingsortlistid.selectedIndex].value;
    transsortlistid = document.getElementById("transsortlistid");
    tid = transsortlistid.options[transsortlistid.selectedIndex].value;

    window.location.href = 'admin_invoices.php?shipping_status='+sid+'&transaction_status='+tid;
  }
</script>

<div id="wrapper">      
  <main id="content">
    <div style="min-height: 60px;">

      <h2 style="color: #fff; text-align: center; background-color: #222; height: 60px; padding-top: 30px; margin-top: 0;">Admin Panel</h2>

      <!-- Navigation for admin -->
      <div id="tables">
      <div id="tables_list1" style="    margin-left: 320px;
    margin-bottom: 30px;
    min-height: 80px;">  
        <ul id="admin_tables">
            <li><a href="admin_dashboard.php"> &lt; Home </a></li>
            <li><a href="../index.php">Live Site &gt; </a></li>
        </ul>
      </div><!-- /#tables_list -->
      </div><!-- /#tables -->
      <!-- Navigation for admin ends-->
      
    </div><!--/#admin_info -->
    
    <div id="product_table">   
      
      <div style="margin-bottom: 20px; margin-left: 15px;">
        
        <?=$shippingsortlist?>
        <?=$transsortlist?>
        <a style="text-decoration: none; color: #fff; font-weight: bold; padding: 10px; border-radius: 5px; background: #000;" href="javascript:doTheFilter()">Go</a>
      </div>
      
      <table class="list_view">
        <tr>
        <th>Invoice Id</th>
        <th>Invoice Date</th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Email</th>
        <th>Phone Number</th>
        <th>Billing Province</th>
        <th>Shipping Province</th>
        <th>Shipping</th>
        <th>Transaction</th>
        <th>View</th>
        </tr>    
      <tr>
        <!-- Fetch & display product details from array -->
        <?php foreach($order as $row) : ?>
   
            <td><?=$row['invoice_id']?></td>
            <td><?=$row['invoice_date']?></td>
            <td><?=$row['first_name']?></td>
            <td><?=$row['last_name']?></td>
            <td><?=$row['email']?></td>
            <td><?=$row['phone_number']?></td>
            <td><?=$row['billing_province']?></td>
            <td><?=$row['shipping_province']?></td>
            <?php if($row['shipping_status'] > 0) : ?>
              <td>
        
                <div class="orderstatusbtn shippedbtn" onclick="window.location = 'admin_invoices.php?toggle_shipping_status=<?=$row['invoice_id']?>'">Shipped<span class="tooltiptext">Click to change to Pending status</span></div>
                
              </td>
            <?php else : ?>
              <td>
        
                <div class="orderstatusbtn pendingbtn" onclick="window.location = 'admin_invoices.php?toggle_shipping_status=<?=$row['invoice_id']?>'">Pending<span class="tooltiptext">Click to change to Shipping status</span></div>
                
              </td>
            <?php endif; ?>
            <?php if($row['transaction_status'] > 0) : ?>
              <td>
        
                <div class="orderstatusbtn completebtn" onclick="window.location = 'admin_invoices.php?toggle_transaction_status=<?=$row['invoice_id']?>'">Complete<span class="tooltiptext">Click to change to Incomplete status</span></div>
                
              </td>
            <?php else : ?>
              <td>
        
                <div class="orderstatusbtn incompletebtn" onclick="window.location = 'admin_invoices.php?toggle_transaction_status=<?=$row['invoice_id']?>'">Incomplete<span class="tooltiptext">Click to change to Complete status</span></div>
                
              </td>
            <?php endif; ?>
              <td>
        
                <div class="orderstatusbtn viewbtn" onclick="window.location = 'admin_invoice.php?invoice_id=<?=$row['invoice_id']?>'">View</div>
                
              </td>        
            </tr>
        <?php endforeach; ?>      
      </table>
    </div>  
  </main>
  
<!-- Include external footer file -->
<?php
include('../../includes/admin_footer.inc.php');
?>
</div><!-- /#wrapper -->