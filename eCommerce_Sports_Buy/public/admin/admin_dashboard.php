<?php

/**
 * Admin View
 * @admin_dashboard.php
 * @course Ecommerce Project, WDD 2018 Jan
 * @author Amarjeet Sharma <Sharmajeetamar31@gmail.com>
 * @created_at 2018-12-01
 **/

$title = 'Admin Dashboard';

// Include external config, model and header files
require  '../../config.php';
require '../../models/products.php';
include ('../../includes/admin_header.inc.php');

if(!isset($_SESSION['admin_logged_in'])) {
  header ('Location: admin_login.php');
  die;
}



// Call function to get average price of products 
$aggregate = aggData($dbh);

//var_dump($_SESSION);
//var_dump($aggregate);
?>
<div id="wrapper">
	<div id="tables">

    <img src="images/website_logo.png" alt="logo" id="logo">

		<h2>--&nbsp;&nbsp;Welcome to Admin Panel&nbsp;&nbsp;--</h2>
 		<h3>Database Tables</h3>
    <img id="admin_dashboard" src="images/admin_dashboard1.png" alt="admin_dashboard" />
    
		<div id="tables_list">  
		  <ul id="admin_tables">
		      <li><a href="admin_products.php">Products</a></li>
		      <li><a href="admin_customers.php">Customers</a></li>
          <li><a href="admin_invoices.php">Orders</a></li>
		      <li><a href="admin_logout.php" id="logout">Logout</a></li>
		  </ul>
		</div><!-- /#tables_list -->

		<div id="data">
    		<h2>A Glimpse of Data</h2>
        <img id="stats" src="images/stats.png" alt="stats" />
    		<ul>
    		<!-- Fetch from array and show all aggregate data on page -->
            <?php foreach($aggregate as $row) : ?>  
              <li>Total financial value of items:
                <strong>$<?=round($row['SUM(price)'], 2);?></strong>
              </li>
              <li>The maximum financial value of item:
                <strong>$<?=round($row['MAX(price)'], 2);?></strong>
              </li>    
              <li>The average financial value of items:
                <strong>$<?=round($row['AVG(price)'], 2);?></strong>
              </li>    
              <li>The minimum financial value of item:
                <strong>$<?=round($row['MIN(price)'], 2);?></strong>
              </li>    
              <li>The maximum units sold for any item:
                <strong><?=$row['MAX(units_sold)'];?> Units</strong>
              </li>
              <li>The least units sold for any item:
                <strong><?=$row['MIN(units_sold)'];?> Units</strong>
              </li>
              <li>The maximum units available for any item:
                <strong><?=$row['MAX(units_in_stock)'];?> Units</strong>
              </li>
              <li>The average units available for any item:
                <strong><?=round($row['AVG(units_in_stock)'], 0);?> Units</strong>
              </li>
              <li>The least units available for any item:
                <strong><?=$row['MIN(units_in_stock)'];?> Units</strong>
              </li>
            <?php endforeach; ?>
    		</ul>

    	</div><!-- /#data -->
	</div><!-- /#tables -->

<!-- Include external footer file -->
<?php
include('../../includes/admin_footer.inc.php');
?>