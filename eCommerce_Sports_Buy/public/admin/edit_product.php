<?php

/**
 * Edit Product
 * @edit_product.php
 * @course Ecommerce Project, WDD 2018 Jan
 * @author Amarjeet Sharma <Sharmajeetamar31@gmail.com>
 * @created_at 2018-12-01
 **/

$title = "Edit Product Details";
$success = false;

// Include external header & config file
require  '../../config.php';
include ('../../includes/admin_header.inc.php');

// Include config and functions files
require 'functions/functions.php';
require '../../models/products.php';

if(empty($_SESSION['admin_logged_in'])) {
  header ('Location: admin_login.php');
  die;
}

// Load validator classes
use classes\utility\validator;

// create object
$v = new validator();

// Fetching database values by admin
if(!empty($_GET)){
  $product_details = adminProducts($dbh, $_GET['product_id']);
}

// Fetch categories from DB and save in a variable
$categories = adminCategories($dbh);

// Fetch suppliers from DB and save in a variable
$suppliers = adminSuppliers($dbh);

// Fetch brands from DB and save in a variable
$brands = adminBrands($dbh);

//var_dump($_GET);

// Test for POST request
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  $product_details = $_POST;
	$errors = [];
  
  // String validation functions  
  $v->validateForSymbols('product_name');
  
  // Check only numbers
  $v-> validateSku('SKU');
  $v-> validatePrice('price');
  $v-> validateUnits('units_sold');
  $v-> validateUnits('units_in_stock'); 
  $v->validateWeight('weight'); 
  
  // Validation for required fields
  $v->required('product_name');  
  $v->required('category_id');
  $v->required('weight');
  $v->required('brand_id');
  $v->required('SKU');
  $v->required('price');
  $v->required('supplier_id');
  $v->required('units_sold');

	//var_dump($_POST);    
	
  /* Validate form for all required fields */    
	//if no errors in validation
	if(count($v->errors()) == 0){

		//connect to mysql
		$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    //var_dump($_POST);
    
		// create query
		$query = "UPDATE 
            products SET                        
                        category_id = :category_id, 
                        supplier_id = :supplier_id,
                        brand_id = :brand_id,  
                        product_name = :product_name, 
                        weight = :weight,                         
                        SKU = :SKU, 
                        price = :price,                         
                        availability = :availability, 
                        units_sold = :units_sold, 
                        units_in_stock = :units_in_stock, 
                        updated_at = CURRENT_TIMESTAMP()
            WHERE product_id = :product_id";
    
    //prepare query
    $stmt = $dbh->prepare($query); 
    
    // bind values
    $params = array(
      ':category_id' => $_POST['category_id'],
      ':supplier_id' => $_POST['supplier_id'],
      ':brand_id' => $_POST['brand_id'],
      ':product_name' => $_POST['product_name'],      
      ':weight' => $_POST['weight'],      
      ':SKU' => $_POST['SKU'],
      ':price' => $_POST['price'],      
      ':availability' => $_POST['availability'],
      ':units_sold' => $_POST['units_sold'],
      ':units_in_stock' => $_POST['units_in_stock'],
      ':product_id' => $_POST['product_id']
    );
		
		//if insert successful,
		if($stmt->execute($params)){
      // set session to display "successful" message on edit product page
      $_SESSION['updated'] = true;

      // Redirect admin to the product list view
      header('Location: admin_products.php');
      die; 
	} else {
    
    // set session to display "successful" message on edit product page
    $_SESSION['updated'] = false;
    $_SESSION['update_message'] = "There was a problem updating the record!";
      
    // Redirect admin to the product list view
    header('Location: admin_products.php');
	}
	
}//Endif no errors

$errors = $v->errors();
  
}// End test for POST
      
?>            
      <main id="content">
        <div id="contentform"> <!-- div for form start -->
          
          <?php if(empty($user)): ?>
          <h2>Edit Product Details</h2> <!-- Form heading -->          
          <form method="post" 
                action="edit_product.php" 
                id="form" 
                name="form" 
                autocomplete="on"
                novalidate
                > <!-- Form start -->

            <fieldset> <!-- Fieldset for Product Details start -->
              <legend>Product Details</legend>
              <img src="images/edit_product.png" id="edit_product" alt="edit_product" />
        <!-- source = http://icons.iconarchive.com/icons/oxygen-icons.org/oxygen/256/Actions-document-edit-icon.png-->
              <p><input type="hidden" 
                       id="product_id" 
                       name="product_id"
                       size="40"                       
                       value="<?=$product_details['product_id']?>"/></p>
              <p>              
                <label class="label"
                       for="product_name">Name</label>
                <input type="text" 
                       id="product_name" 
                       name="product_name"
                       size="40"                       
                       value="<?=$product_details['product_name']?>"/>
                <?php if(!empty($errors['name'])) :?>
                <span style="color: #f00"><small><?=esc($errors['product_name']);?></small></span>
                <?php endif; ?>
              </p>  

              <p>  
                <label class="label"
                       for="category_id">Category</label>    
                <select name="category_id" id="category_id"> <!-- Select list -->
                  <?php foreach($categories as $category) : ?>
                  <option value="<?=$category['category_id']?>"><?=$category['category_name']?></option>
                  <?php endforeach; ?>
                </select>               
              </p>

              <p>  
                <label class="label"
                       for="weight">Weight</label>
                <input type="text" 
                       id="weight" 
                       name="weight"
                       size="40"
                       value="<?=$product_details['weight']?>"/>
                <?php if(!empty($errors['weight'])) :?>
                <span style="color: #f00"><small><?=esc($errors['weight']);?></small></span>
                <?php endif; ?>                
              </p>

              <p>  
                <label class="label"
                       for="brand_id">Brand</label>    
                <select name="brand_id" id="brand_id"> <!-- Select list -->
                  <?php foreach($brands as $brand) : ?>
                  <option value="<?=$brand['brand_id']?>"><?=$brand['brand_name']?></option>
                  <?php endforeach; ?>
                </select>               
              </p>

              <p>  
                <label class="label"
                       for="SKU">SKU</label>
                <input type="text" 
                       id="SKU" 
                       name="SKU"
                       size="40"
                       value="<?=$product_details['SKU']?>"/>
                <?php if(!empty($errors['SKU'])) :?>
                <span style="color: #f00"><small><?=esc($errors['SKU']);?></small></span>
                <?php endif; ?>                
              </p>
              <p>  
                <label class="label"
                       for="price">Price</label>
                <input type="text" 
                       id="price" 
                       name="price"
                       size="40"
                       value="<?=$product_details['price']?>"/>
                <?php if(!empty($errors['price'])) :?>
                <span style="color: #f00"><small><?=esc($errors['price']);?></small></span>
                <?php endif; ?>                
              </p>

              <p>  
                <label class="label"
                       for="supplier_id">Supplier</label>    
                <select name="supplier_id" id="supplier_id"> <!-- Select list -->
                  <?php foreach($suppliers as $supplier) : ?>
                  <option value="<?=$supplier['supplier_id']?>"><?=$supplier['supplier_name']?></option>
                  <?php endforeach; ?>
                </select>               
              </p>


              <p>  
                <label class="label"
                       for="availability">Availability</label>    
                <select name="availability" id="availability"> <!-- Select list -->
                  <option value="0">0</option>
                  <option value="1">1</option>                
                </select>               
              </p>
              <p>  
                <label class="label"
                       for="units_sold">Units Sold</label>
                <input type="text" 
                       id="units_sold" 
                       name="units_sold"
                       value="<?=$product_details['units_sold']?>"/>
                <?php if(!empty($errors['units_sold'])) :?>
                <span style="color: #f00"><small><?=esc($errors['units_sold']);?></small></span>
                <?php endif; ?>                
              </p>
              
              <p>  
                <label class="label"
                       for="units_in_stock">Units Available</label>
                <input type="text" 
                       id="units_in_stock" 
                       name="units_in_stock"
                       value="<?=$product_details['units_in_stock']?>"/>
                <?php if(!empty($errors['units_in_stock'])) :?>
                <span style="color: #f00"><small><?=esc($errors['units_in_stock']);?></small></span>
                <?php endif; ?>                
              </p>
     
            </fieldset><!-- Fieldset for Product Details end -->

            <p style="text-align: center;"><!-- Buttons start -->
              <input type="submit" 
                     value="Update" 
                     id="submit"
                     class="button"/>&nbsp; &nbsp;

            </p><!-- Buttons end -->
            
            
          </form> <!-- Form end -->
        <?php endif; ?>
        </div> <!-- div for form end -->          
      </main>

<!-- Include external footer file -->
<?php
include('../../includes/admin_footer.inc.php');
?>
      
