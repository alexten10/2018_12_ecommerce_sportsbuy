<?php

/**
 * Admin Products
 * @admin_products.php
 * @course Ecommerce Project, WDD 2018 Jan
 * @author Amarjeet Sharma <Sharmajeetamar31@gmail.com>
 * @created_at 2018-12-01
 **/

$title = 'Admin Products - List View';


// Include external config, model and header files
require  '../../config.php';
require '../../models/products.php';
include ('../../includes/admin_header.inc.php');
require 'functions/functions.php';


if(empty($_SESSION['admin_logged_in'])) {
  header ('Location: admin_login.php');
  die;
}

// Fetch and save product details in variable
$product = getProducts($dbh);


/* Search functionality */
// if get keyword from search field
if(!empty($_GET['keyword'])) {
  $keyword = strip_tags(htmlspecialchars($_GET['keyword'])); //assign $_GET['keyword'] value to a variable, after escaping special chars and removing tags for security reasons
  $keyword_extended ="%" . $keyword . "%"; //search including any chars before typed keyword (%$keyword) any chars after typed ($keyword%)
  $products = search($dbh, $keyword_extended); //send to the search function: database and value of $keyword
  $container_title = 'Results for: ' . $keyword;
  $search_result_count = count($products);//number of items(subarrays)
}



?>   
<div id="wrapper">      
  <main id="content">
    <div id="admin_info">

      <img src="images/website_logo.png" alt="logo" id="logo">
      
      <h2>Admin Panel</h2>

      <!-- Navigation for admin -->
      <div id="tables">
      <div id="tables_list">  
        <ul id="admin_tables">
            <li><a href="admin_dashboard.php"> &lt; Home </a></li>
            <li><a href="admin_customers.php"> Customers </a></li>
            <li><a href="../index.php">Live Site &gt; </a></li>
            <li><a href="admin_logout.php" id="logout">Logout</a></li>
        </ul>
      </div><!-- /#tables_list -->
      </div><!-- /#tables -->
      <!-- Navigation for admin ends-->

      <!-- script to hide the flash messages -->
      <script>
        setTimeout(function() {
        $('#flash').fadeOut('slow');
        }, 2000);
      </script>
      

      <?php
      //if $_SESSION['update'] exists, which comes form edit_product.php
      if(isset($_SESSION['updated'])){
        $_SESSION['updated'] =  
                              '<div id="flash"
                               style="color: #0b0; 
                               padding: 15px;
                               font-size: 16px;
                               margin: 0 auto;
                               margin-top: 10px;
                               margin-left: 300px;
                               width: 295px;
                               position: absolute;
                               border: 1px solid #0b0" >
                    <strong>Product details updated successfully!</strong>
                  </div><br />';
        $flash_message_updated = $_SESSION['updated'];
        echo $flash_message_updated;
        unset ($_SESSION['updated']);
      }

      //if $_SESSION['added'] exists, which comes form edit_product.php
      if(isset($_SESSION['added'])){
        $_SESSION['added'] =  
                              '<div id="flash"
                               style="color: #0b0; 
                               padding: 15px;
                               font-size: 16px;
                               margin: 0 auto;
                               margin-top: 10px;
                               margin-left: 345px;
                               width: 260px;
                               position: absolute;
                               border: 1px solid #0b0" >
                    <strong>Brand details added successfully!</strong>
                  </div><br />';
        $flash_message_updated = $_SESSION['added'];
        echo $flash_message_updated;
        unset ($_SESSION['added']);
      }
      //if $_SESSION['product_added'] exists, which comes form edit_product.php
      if(isset($_SESSION['product_added'])){
        $_SESSION['product_added'] =  
                              '<div id="flash"
                               style="color: #0b0; 
                               padding: 15px;
                               font-size: 16px;
                               margin: 0 auto;
                               margin-top: 10px;
                               margin-left: 345px;
                               width: 255px;
                               position: absolute;
                               border: 1px solid #0b0" >
                    <strong>Product was added successfully!</strong>
                  </div><br />';
        $flash_message_updated = $_SESSION['product_added'];
        echo $flash_message_updated;
        unset ($_SESSION['product_added']);
      }
      //if $_SESSION['supplier_added'] exists, which comes form edit_product.php
      if(isset($_SESSION['supplier_added'])){
        $_SESSION['supplier_added'] =  
                              '<div id="flash"
                               style="color: #0b0; 
                               padding: 15px;
                               font-size: 16px;
                               margin: 0 auto;
                               margin-top: 10px;
                               margin-left: 345px;
                               width: 260px;
                               position: absolute;
                               border: 1px solid #0b0" >
                    <strong>Supplier was added successfully!</strong>
                  </div><br />';
        $flash_message_updated = $_SESSION['supplier_added'];
        echo $flash_message_updated;
        unset ($_SESSION['supplier_added']);
      }
      //if $_SESSION['category_added'] exists, which comes form edit_product.php
      if(isset($_SESSION['category_added'])){
        $_SESSION['category_added'] =  
                              '<div id="flash"
                               style="color: #0b0; 
                               padding: 15px;
                               font-size: 16px;
                               margin: 0 auto;
                               margin-top: 10px;
                               margin-left: 345px;
                               width: 265px;
                               position: absolute;
                               border: 1px solid #0b0" >
                    <strong>Category was added successfully!</strong>
                  </div><br />';
        $flash_message_updated = $_SESSION['category_added'];
        echo $flash_message_updated;
        unset ($_SESSION['category_added']);
      }
      //if $_SESSION['product_deleted'] exists, which comes form edit_product.php
      if(isset($_SESSION['product_deleted'])){
        $_SESSION['product_deleted'] =  
                              '<div id="flash"
                               style="color: #0b0; 
                               padding: 15px;
                               font-size: 16px;
                               margin: 0 auto;
                               margin-top: 10px;
                               margin-left: 345px;
                               width: 265px;
                               position: absolute;
                               border: 1px solid #0b0" >
                    <strong>Product was deleted successfully!</strong>
                  </div><br />';
        $flash_message_updated = $_SESSION['product_deleted'];
        echo $flash_message_updated;
        unset ($_SESSION['product_deleted']);
      }

      ?>
      
      <img src="images/product.png" alt="product" id="customer_info" />

      <div id="search">
        <form action="admin_products.php" 
        id="search_form"
        method="get" 
        novalidate="novalidate" 
        autocomplete="off">
        
        <input type="text"
           id="searchbox" 
           name="keyword" 
           maxlength="255" 
           placeholder="Search product by name" />&nbsp; <!-- name="keyword" is used for $_GET['keyword'] -->
           
        <input id="search_button" type="submit" value="search" />     
      </form>

      </div><!--/#search -->
      
    </div><!--/#admin_info -->
    
    <div id="product_table">   

      <div id="search_result">
        <?php if (!empty($container_title)) : ?>
        <h3><?php echo $container_title ?></h3>
        <h3>Found (<?php echo $search_result_count ?>) results:</h3>        

        <ul>
        <?php foreach ($products as $key => $value) : ?> 

          <li><a href="edit_product.php?product_id=<?php echo $value['product_id']?>" 
            ><?=$value['product_name'] ?></a></li>
        
        <?php endforeach; ?>
        </ul>
        <?php endif; ?>
      </div>


      <div id="heading">
        <h3>Additional Actions</h3>
      </div>


      <div id="list_container">
        <ul id="add_list">
          <li id="brand"><a href="add_brand.php">Add Brand</a></li>
          <li><a href="add_product.php">Add Product</a></li>
          <li><a href="add_supplier.php">Add Supplier</a></li>          
          <li id="category"><a href="add_category.php">Add Category</a></li>
        </ul>
      </div>

      <table class="list_view">
        <tr><th>Product Id</th>
        <th>Name</th>
        <th>Category</th>
        <th>Brand</th>
        <th>Price</th>
        <th>Supplier</th>
        <th>Availability</th>
        <th>Units Sold</th>
        <th>Units Available</th>
        <th>Deleted</th>    
        <th>Controls</th></tr>    
      <tr>
        <!-- Fetch & display product details from array -->
        <?php foreach($product as $row) : ?>
   
            <td><?=$row['product_id']?></td>
            <td><?=$row['product_name']?></td>
            <td><?=$row['category_name']?></td>
            <td><?=$row['brand_name']?></td>
            <td>$<?=$row['price']?></td>
            <td><?=$row['supplier_name']?></td>
            <td><?=$row['availability']?></td>
            <td><?=$row['units_sold']?></td>
            <td><?=$row['units_in_stock'];?></td>

            <?php if($row['deleted'] == 1) : ?>
              <td style="color: #bf0000;">Yes</td>
            <?php else : ?>
              <td style="color: #458B00;">No</td>
           <?php endif; ?>

            <td>
              <!-- Pass product id to edit/delete page -->
              <a href="edit_product.php?product_id=<?=$row['product_id']?>">Edit</a>            
              <a 
              href="delete_product.php?product_id=<?=$row['product_id']?>">Delete</a>
            </td>
            </tr>
        <?php endforeach; ?>      
      </table>
    </div>  
  </main>

  <!-- Include external footer file -->
<?php
include('../../includes/admin_footer.inc.php');
?>

</div><!-- /#wrapper -->

