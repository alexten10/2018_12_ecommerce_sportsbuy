<?php

/**
 * Add Supplier
 * @add_supplier.php
 * @course Ecommerce Project, WDD 2018 Jan
 * @author Amarjeet Sharma <Sharmajeetamar31@gmail.com>
 * @created_at 2018-12-11
 **/

$title = "Add Supplier";


$success = false;
// Include external header & config file
require  '../../config.php';

// Include config and functions files
require 'functions/functions.php';


if(empty($_SESSION['admin_logged_in'])) {
  header ('Location: admin_login.php');
  die;
}

// Load validator classes
use classes\utility\validator;

$v = new validator();

$existing_suppliers = getSuppliers($dbh);

//var_dump($existing_suppliers);


// Test for POST request
if($_SERVER['REQUEST_METHOD'] == 'POST') {

  $errors = [];
  
  // String validation functions  
  $v->validateForSymbols('supplier_name');
  $v->required('supplier_name');

	//var_dump($_POST);
    
	//if no errors in validation
	if(count($v->errors()) == 0){
    $deleted =0;
		//connect to mysql
		$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    //var_dump($_POST);
    
		// create query
		$query = "INSERT INTO 
		suppliers
    	(supplier_name)
		VALUES
		(:supplier_name)";
    
    //prepare query
    $stmt = $dbh->prepare($query); 
    
    // bind values
    $params = array(

      ':supplier_name' => $_POST['supplier_name'],
    );   
		
	  //if insert successful,
	  if($stmt->execute($params)){
      // set session to display "successful" message on edit product page
      $_SESSION['supplier_added'] = true;

      // Redirect user to the product list view
      header('Location: admin_products.php');
 
	} else {
    
    // set session to display "successful" message on edit product page
    $_SESSION['updated'] = true;
    $_SESSION['update_message'] = "There was a problem adding the record!";
      
    // Redirect user to the product list view
    header('Location: admin_products.php');
	}        
	
}//Endif no errors

$errors = $v->errors();
  
}// End test for POST
      
?>     

<div id="wrapper">
 <?php include('../../includes/admin_header.inc.php'); ?>
 <img src="images/website_logo.png" alt="logo" id="logo">
 
  <div id="tables">
    <h2><?php echo $title ?></h2>
  </div>       

  <div id="container" style="min-height: 600px;">

	<main id="content">        

	<!-- Navigation for admin -->
      <div id="tables">
      <div id="tables_list">  
        <ul id="admin_tables">
            <li><a href="admin_dashboard.php"> < Home </a></li>
            <li><a href="admin_products.php"> Products </a></li>
            <li><a href="../index.php">Live Site > </a></li>
            <li><a href="logout.php" id="logout">Logout</a></li>
        </ul>
      </div><!-- /#tables_list -->
      </div><!-- /#tables -->
      <!-- Navigation for admin ends-->

        <div id="contentform"> <!-- div for form start -->

          <h2>Add New Supplier</h2> <!-- Form heading -->  


          <form method="post" 
                action="add_supplier.php" 
                id="form" 
                name="form" 
                autocomplete="on"
                novalidate
                > <!-- Form start -->

            <fieldset> <!-- Fieldset for Supplier Details start -->
              <legend>Supplier Details</legend>   
              <img src="images/add_supplier.png" id="add_product" alt="add supplier" />
              <!-- source = https://www.iconspng.com/uploads/add-document/add-document.png -->
              <p>              
                <label class="label"
                       for="supplier_name">Name</label>
                <input type="text" 
                       id="supplier_name" 
                       name="supplier_name"
                       size="40"                       
                       value="<?php
                    if(!empty($_POST['supplier_name'])) {
                        echo esc($_POST['supplier_name']);
                    }
                ?>"/>
                <?php if(!empty($errors['supplier_name'])) :?>
                <span style="color: #f00"><small><?=esc($errors['supplier_name']);?></small></span>
                <?php endif; ?>
              </p> 

              </fieldset><!-- Fieldset for Product Details end -->

            <p><!-- Buttons start -->
              <input type="submit" 
                     value="Add" 
                     id="submit"
                     class="button"/>&nbsp; &nbsp;
              <input type="reset" 
                     value="Clear Values" 
                     id="reset"
                     class="button"/>
            </p><!-- Buttons end -->
          </form> <!-- Form end -->
     
       </div> <!-- div for form end -->  

       <div id="data">
    		<h2>Our Existing Suppliers</h2>
    		<ul>
    		<!-- Fetch from array and show all suppliers data on page -->
            <?php foreach($existing_suppliers as $row) : ?>  
              <li><?=($row['supplier_name']);?></li>
            <?php endforeach; ?>
    		</ul>

    	</div><!-- /#data -->

      </main>
  	</div><!-- div for wrapper end --> 
<!-- Include external footer file -->
<?php
include('../../includes/admin_footer.inc.php');
?>
</div>