<?php

/**
 * Products
 * @products.php
 * @course Ecommerce Project, WDD 2018 Jan
 * @author Amarjeet Sharma <Sharmajeetamar31@gmail.com>
 * @created_at 2018-12-04
 **/

// Include external header & config file
require  '../../config.php';

// Include config and functions files
require '../../models/products.php';

// Create query
	$query = 'UPDATE 
	products SET 
				deleted = 1,
				availability = 0 
			WHERE product_id = :product_id';
  
  // prepare query
	$stmt = $dbh->prepare($query);

  // Bind values
	$stmt->bindValue(':product_id', $_GET['product_id'], PDO::PARAM_INT);
  
  // Execute
	$stmt->execute();
  
  // Redirect user to the products list view page for admin
  header('Location: admin_products.php');

// set session to display "successful" message on edit product page
$_SESSION['product_deleted'] = true;

die;

?>



