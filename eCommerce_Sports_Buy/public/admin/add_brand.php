<?php

/**
 * Add Brand
 * @add_brand.php
 * @course Ecommerce Project, WDD 2018 Jan
 * @author Amarjeet Sharma <Sharmajeetamar31@gmail.com>
 * @created_at 2018-12-11
 **/

$title = "Add Brand";


// Include external header & config file
require  '../../config.php';
require '../../models/products.php';

// Include config and functions files
require 'functions/functions.php';

if(empty($_SESSION['admin_logged_in'])) {
  header ('Location: admin_login.php');
  die;
}

// Load validator classes
use classes\utility\validator;

$v = new validator();

$existing_brands = getBrands($dbh);

// Test for POST request
if($_SERVER['REQUEST_METHOD'] == 'POST') {

  $errors = [];
  
  // String validation functions  
  $v->validateForSymbols('brand_name');
  $v->required('brand_name'); 

	//var_dump($_POST);
    
	//if no errors in validation
	if(count($v->errors()) == 0){
    $deleted =0;
		//connect to mysql
		$dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    //var_dump($_POST);
    
		// create query
		$query = "INSERT INTO 
		brands
    	(brand_name, image)
		VALUES
		(:brand_name, :image)";
    
    //prepare query
    $stmt = $dbh->prepare($query); 
    
    // bind values
    $params = array(

      ':brand_name' => $_POST['brand_name'],
      ':image' => $_POST['image'],
    );   
		
	  //if insert successful,
	  if($stmt->execute($params)){
      // set session to display "successful" message on edit product page
      $_SESSION['added'] = true;

      // Redirect user to the product list view
      header('Location: admin_products.php');
 
	} else {
    
    // set session to display "successful" message on edit product page
    $_SESSION['updated'] = true;
    $_SESSION['update_message'] = "There was a problem adding the record!";
      
    // Redirect user to the product list view
    header('Location: admin_products.php');
    die;
	}        
	
}//Endif no errors

$errors = $v->errors();
  
}// End test for POST
      
?>     

<div id="wrapper">
 <?php include('../../includes/admin_header.inc.php'); ?>
 <img src="images/website_logo.png" alt="logo" id="logo">

  <div id="tables">
    <h2><?php echo $title ?></h2>
  </div>       

  <div id="container" style="min-height: 600px;">

	<main id="content">        

	<!-- Navigation for admin -->
      <div id="tables">
      <div id="tables_list">  
        <ul id="admin_tables">
            <li><a href="admin_dashboard.php"> < Home </a></li>
            <li><a href="admin_products.php"> Products </a></li>
            <li><a href="../index.php">Live Site > </a></li>
            <li><a href="logout.php" id="logout">Logout</a></li>
        </ul>
      </div><!-- /#tables_list -->
      </div><!-- /#tables -->
      <!-- Navigation for admin ends-->

        <div id="contentform"> <!-- div for form start -->
          <?php if(empty($user)): ?>
          <h2>Add New Brand</h2> <!-- Form heading -->  


          <form method="post" 
                action="add_brand.php" 
                id="form" 
                name="form" 
                autocomplete="on"
                novalidate
                > <!-- Form start -->

            <fieldset> <!-- Fieldset for Supplier Details start -->
              <legend>Brand Details</legend>   
              <img src="images/add_brand.png" id="add_product" alt="add brand" />
              <!-- source = https://www.iconspng.com/uploads/add-document/add-document.png -->
              <p>              
                <label class="label"
                       for="brand_name">Name</label>
                <input type="text" 
                       id="brand_name" 
                       name="brand_name"
                       size="40"                       
                       value="<?php
                    if(!empty($_POST['brand_name'])) {
                        echo esc($_POST['brand_name']);
                    }
                ?>"/>
                <?php if(!empty($errors['brand_name'])) :?>
                <span style="color: #f00"><small><?=esc($errors['brand_name']);?></small></span>
                <?php endif; ?>
              </p> 

              <p>  
                <label class="label"
                       for="image">Image Name</label>
                <input type="text" 
                       id="image" 
                       name="image"
                       value="<?php
                    if(!empty($_POST['image'])) {
                        echo esc($_POST['image']);
                    }
                ?>"/>
                <span style="color: #1A8850; font-weight: bold;"><small>(with extension)</small></span>
                <?php if(!empty($errors['image'])) :?>
                <span style="color: #f00"><small><?=esc($errors['image']);?></small></span>
                <?php endif; ?>                
              </p>


              </fieldset><!-- Fieldset for Product Details end -->

            <p><!-- Buttons start -->
              <input type="submit" 
                     value="Add" 
                     id="submit"
                     class="button"/>&nbsp; &nbsp;
              <input type="reset" 
                     value="Clear Values" 
                     id="reset"
                     class="button"/>
            </p><!-- Buttons end -->
          </form> <!-- Form end -->
        <?php endif; ?>          
       </div> <!-- div for form end -->  

       <div id="data">
    		<h2>Our Existing Brands</h2>
    		<ul>
    		<!-- Fetch from array and show all suppliers data on page -->
            <?php foreach($existing_brands as $row) : ?>  
              <li><?=($row['brand_name']);?></li>
            <?php endforeach; ?>
    		</ul>

    	</div><!-- /#data -->

      </main>
  	</div><!-- div for wrapper end --> 
<!-- Include external footer file -->
<?php
include('../../includes/admin_footer.inc.php');
?>
</div>