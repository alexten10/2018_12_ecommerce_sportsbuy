<?php

/**
 * Products
 * @delete_customer.php
 * @course Ecommerce Project, WDD 2018 Jan
 * @author Amarjeet Sharma <Sharmajeetamar31@gmail.com>
 * @created_at 2018-12-08
 **/

// Include external header & config file
require  '../../config.php';

// Create query
	$query = 'UPDATE 
	customers SET 
				deleted = 1
			WHERE customer_id = :customer_id';
  
  // prepare query
	$stmt = $dbh->prepare($query);

  // Bind values
	$stmt->bindValue(':customer_id', $_GET['customer_id'], PDO::PARAM_INT);
  
  // Execute
	$stmt->execute();
  
  // Redirect user to the products list view page for admin
  header('Location: admin_customers.php');

// set session to display "successful" message on edit product page
$_SESSION['customer_deleted'] = true;
die;

?>