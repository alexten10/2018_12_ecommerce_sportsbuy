<?php

require  '../../config.php';
require 'functions/functions.php';
use \classes\utility\validator;
$vldtr = new validator;


$title = 'Edit Password';

var_dump($_GET);
if(empty($_SESSION['admin_logged_in'])) {
  header ('Location: admin_login.php');
  die;
}


if(!empty($_GET['customer_id'])) {
  $customer = getCustomerById($dbh, $_GET['customer_id']);
}


if($_SERVER['REQUEST_METHOD'] == 'POST') {
  
  $customer = getCustomerById($dbh, $_POST['customer_id']);

  //if typed old password matches to the stored in DB

    $current_date = date('Y-m-d h:m:s');
    
    // !!! order of calling functions is important !!!
    $vldtr->validatePasswordStrength('password');
    $vldtr->minLength('password', 6);//minimum required length of password is 6 characters
    $vldtr->maxLength('password', 12);//maximum required password length is 12 characters
    $vldtr->passwordsMatch('password', 'password_confirm');
    $vldtr->required('password');
    
    if(empty($vldtr->errors())) {
      
      $new_password = htmlspecialchars($_POST['password']);
      $updatePassword = updatePassword($dbh, $new_password, $current_date, $customer['customer_id']);
      if($updatePassword == true) {
        $_SESSION['password_updated'] = "Password updated";
        $flash_message = $_SESSION['password_updated'];
        unset($_SESSION['password_updated']);
        $email_sent = sendEmail($customer['first_name'], $customer['email'], $_POST['password']);
      }//END if($updatePassword == true)
      
    }//END if(empty($vldtr->errors()))

    
}//END if($_SERVER['REQUEST_METHOD'] == 'POST')
$errors = $vldtr->errors();

?><div id="container">
      
        <h1><?=$title?></h1>
        
        <!-- if editing profile was unsuccessful, show a message-->
        <?php if(isset($flash_message)) :?>
          <h2><?php echo $flash_message ?></h2>
        <?php endif; ?>
        
        <?php if(isset($wrong_password_msg)) :?>
          <h2><?php echo $wrong_password_msg ?></h2>
        <?php endif; ?>
        
        <?php if(isset($email_sent)) :?>
          <h2>Confirmation is sent to your email</h2>
        <?php endif; ?>
        
        <div id="customer_password_form">
          <form method="post"
                  action="admin_pswd_change_for_cust.php"
                  id="admin_pswd_change_for_cust"
                  name="admin_pswd_change_for_cust"
                  accept-charset="utf-8" 
                  novalidate="novalidate">
              
            <fieldset>
              <legend><b>Edit Customer Password</b></legend>
              
              <p><span class="required_sign">*&nbsp;</span>- required field</p>

              <p>
                <label for="password"><span class="required_sign">*&nbsp;</span>New Password</label>
                <input type="password"
                       id="password"
                       name="password"
                       maxlength="255"
                       value=""
                       placeholder="New password (min length 6)" /><br />
                       
                <?php if(!empty($errors['password'])) : ?>
                  <span class="error"><?=$errors['password']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="pass_confirm"><span class="required_sign">*&nbsp;</span>Confirm Password</label>
                <input type="password"
                       id="pass_confirm"
                       name="password_confirm"
                       maxlength="255"
                       value=""
                       placeholder="Type again your new password" />
              </p>


              <input type="hidden" name="customer_id" value="<?=$customer['customer_id']?>" />

            </fieldset>


            <!-- *******  BUTTONS ************-->
            <p id="form_submit_buttons">
              <input type="submit"
                     value="Submit"
                     class="button" />&nbsp; &nbsp;
              <input type="button"
                     value="Clear Form"
                     onclick="clearForm(this.form);"
                     class="button" />
          </p>
        </form> 

        <!-- Include external footer file -->
<?php
include('../../includes/admin_footer.inc.php');
?>
</div><!-- /#container -->