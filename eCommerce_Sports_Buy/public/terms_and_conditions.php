<?php

require '../config.php';
require '../functions.php';

$title = 'Terms and  Conditions';
$active_page = '';

?>

<?php include '../includes/header.inc.php'; ?>
    
    <!-- container starts -->
    <div id="container">
      
    
      <!-- content starts -->
      <div id="content">
        
        <!-- about_us_paragraph starts here -->
        <div id="about_us_paragraph">
          <h1> TERMS and CONDITIONS </h1>
          <p>Please read these terms and conditions (“Terms and Conditions”) carefully before using this website and any adidas content on social media sites (including without limit Facebook and Twitter) (together, the “Site”). These Terms and Conditions apply to all visits and use of the Site, as well as to the Content (as defined below), information, recommendations, products and/or services provided to you on or through the Site. By accessing and using the Site, you signify your consent to these Terms and Conditions in their entirety in addition to any other law or regulation that applies to the Site and the Internet. If you do not agree to these Terms and Conditions in their entirety, please leave the Site.</p>

          <h2>CONTENT ON THE SITE</h2>
          <p>All of the content featured or displayed on or through the Site, including, but not limited to, logos, icons, trade marks, text, graphics, photographs, images, moving images, sound, illustrations, software, and other information (“Content”) is owned by Sports Buy, its affiliated companies, its licensors or its content providers, as applicable. </p>

          <p>All elements of the Site including, but not limited to, the general design and the Content, may be protected by copyright, moral rights, database rights, trade mark and other laws relating to intellectual property or other rights. Except as explicitly permitted under these Terms and Conditions or another agreement with Sports Buy, no portion or element of the Site or its Content may be copied or retransmitted via any means. The Site, its Content and all related rights shall remain the exclusive property of Sports Buy, its affiliated companies, its licensors, or its content providers, as applicable, unless otherwise expressly agreed. All such rights are reserved.</p>

          <p>Please be aware that we are not familiar with your individual physical characteristics and health. Before you begin an exercise program, you should get a medical checkup. It is important that you warm up and stretch before engaging in physical activity and that you use common sense while engaging in physical activity. If you experience any pain, feel weak, dizzy or exhausted or become short of breath, immediately stop your workout. When you engage in physical activity, you assume all inherent risks.</p>
          
          <h2>Copyrights and Trademarks</h2>
          <p>All rights in the Content, including copyright, are and remain owned by Sports Buy, its affiliated companies, its licensors, or content providers, as applicable. Except as may be otherwise indicated within the Site, you are authorised to view, play, print and download Content found on the Site for personal, informational, and non-commercial purposes only. You may not modify any of the Content and you may not copy, distribute, transmit, display, perform, reproduce, publish, license, create derivative works from, transfer or sell any Content. You may not re-use any Content without first obtaining the written consent of sports buy. The use of any Content on any other website or networked computer environment is prohibited. You may not remove any copyright, trademark or other proprietary notices from Content found on the Site.</p>

        </div><!-- terms_cond_paragraph ends here -->

      </div><!-- content ends -->

    </div><!-- Container ends -->


<?php include '../includes/footer.inc.php'; ?>