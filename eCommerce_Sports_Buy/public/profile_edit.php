<?php

require '../config.php';
require '../functions.php';
use \classes\utility\validator;
$vldtr = new validator;


$title = 'Profile Edit';
$active_page = '';

//var_dump($_GET['customer_id']);

if(empty($_SESSION['logged_in'])) {
  header ('Location: index.php');
  die;
}


if(!empty($_GET['customer_id'])) {
  $customer = getCustomerById($dbh, $_GET['customer_id']);
}
//var_dump($customer);

if($_SERVER['REQUEST_METHOD'] == 'POST') {
  $customer = $_POST;
  
  $current_date = date('Y-m-d h:m:s');
  
  $vldtr->validateForSymbols('first_name');
  $vldtr->required('first_name');
  
  $vldtr->validateForSymbols('last_name');
  $vldtr->required('last_name');
  
  $vldtr->validateEmail('email'); //this is 1
  $vldtr->required('email'); //this is 2
  
  $vldtr->validatePhone('phone_number');
  $vldtr->required('phone_number');
  
  $vldtr->required('billing_province');
  
  $vldtr->validateForSymbols('billing_city');
  $vldtr->required('billing_city');
  
  $vldtr->validateStreetSymbols('billing_street');
  $vldtr->required('billing_street');
  
  if (!empty ($_POST['billing_apartment_number'])) {
    $vldtr->validateAppartmentNumber('billing_apartment_number');
  }
  
  $vldtr->validatePostalCode('billing_postal_code');
  $vldtr->required('billing_postal_code');
  
  $vldtr->required('shipping_province');
  
  $vldtr->validateForSymbols('shipping_city');
  $vldtr->required('shipping_city');
  
  $vldtr->validateStreetSymbols('shipping_street');
  $vldtr->required('shipping_street');
  
  if (!empty ($_POST['shipping_apartment_number'])) {
    $vldtr->validateAppartmentNumber('shipping_apartment_number');
  }
  
  $vldtr->validatePostalCode('shipping_postal_code');
  $vldtr->required('shipping_postal_code');
  
  
  if(empty($vldtr->errors())) {
    
    
    //*******  START check if email is unique (does not already exist in the db) except that belongs to the logged in customer
    //get all emails that exist in DB
    $emails = getCustomersEmails($dbh);
    
    //match DB emails with the typed in registration form
    foreach ($emails as $key=>$value) {
      /*do not match against the logged in customer's email in DB,
       *otherwise if customer doesnt change email, matching email will be found in DB*/
      if($value['customer_id'] !== $_POST['customer_id']) {
        //if found matching email in db, set variable $match_found as flag
        if(($_POST['email'] == $value['email'])) {
          $match_found = true; 
          break;
        }//END if(($_POST['email']
      }//END if($value['customer_id']
    }// END foreach
    //*******  END check if email is unique (is not already exists in the db)
    
    
    
    //if $match_found doesnt exists (the typed email is unique and doesnt exist in db) 
    if(!isset($match_found)) {
      $post = $_POST;
      $sanitized_post_array = sanitizeFormInputs($post); //do strip_tags() and htmlspecialchars()
      $updatedCustomerInfoInDB = updateCustomerInfo($dbh, $sanitized_post_array, $current_date); //update DB with new data, get true if inserted
      
      /* if UPDATE(updateCustomerInfo function) success, redirect to profile.php page,
       * where the new info is displayed with a flash confirmation msg*/
      if($updatedCustomerInfoInDB = true) {
        $_SESSION['profile_edited'] = true;
        header('Location: profile.php');
        die;
      }// END if UPDATE(updateCustomerInfo function) works
      else{
        $customer_not_edited = 'Can not edit profile';
      }//END else
    }//END if(!isset($match_found)) 
    
    //if $match_found exists (that shows entered email is already exists in db)
    else {
      $email_exist_msg = "Sorry, email has already been registered before.";
    }//END else
    
  }//END if(empty($vldtr->errors()))
    
}//END if($_SERVER['REQUEST_METHOD'] == 'POST')
$errors = $vldtr->errors();

?>
<?php include '../includes/header.inc.php'; ?>












    <div id="container">
        
        <!--
        <?php //if(isset($_SESSION['cart'])) { //if anything is added in cart
          //include '../includes/cart.inc.php';
        //} ?>  -->
        
        <h1><?=$title?></h1>
        <br/>
        
        <!-- if editing profile was unsuccessful, show a message-->
        <?php if(isset($customer_not_edited)) :?>
          <h2><?php echo $customer_not_edited ?></h2>
        <?php endif; ?>
        
        <div id="customer_edit_form">
          <form method="post"
                  action="profile_edit.php"
                  id="profile_edit_form"
                  name="profile_edit_form"
                  accept-charset="utf-8" 
                  novalidate="novalidate">
              
            <fieldset>
              <legend><b>Edit Customer Info</b></legend>
              
              <p><span class="required_sign">*&nbsp;</span>- required field</p>
              
              <p>
                <label for="first_name"><span class="required_sign">*&nbsp;</span>First Name</label><!-- name of the form -->
                <!-- input field is "sticky"(if !empty $-POST...), which means if validation error appears, the typed in the field data will not be earased-->
                <input type="text"
                       id="first_name"
                       name="first_name"
                       maxlength="255"
                       value="<?php 
                                if(!empty($customer['first_name'])){
                                  echo esc ($customer['first_name']);}
                              ?>"
                       placeholder="Type your first name" /><br />
                
                <!-- display error message near the field, if exists -->
                <?php if(!empty($errors['first_name'])) : ?>
                  <span class="error"><?=$errors['first_name']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="last_name"><span class="required_sign">*&nbsp;</span>Last Name</label>
                <input type="text"
                       id="last_name"
                       name="last_name"
                       maxlength="255"
                       value="<?php 
                                if(!empty($customer['last_name'])){
                                  echo esc ($customer['last_name']);}
                              ?>"
                       placeholder="Type your last name" /><br />
                       
                <?php if(!empty($errors['last_name'])) : ?>
                  <span class="error"><?php echo $errors['last_name']?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="email"><span class="required_sign">*&nbsp;</span>Email Address</label>
                <input type="email"
                       name="email"
                       id="email"
                       value="<?php 
                                if(!empty($customer['email'])){
                                  echo esc ($customer['email']);}
                              ?>"
                       placeholder="your_email@example.com" /><br />
                       
                <?php if(!empty($errors['email'])) : ?>
                  <span class="error"><?=$errors['email']?></span><br />
                <?php endif; ?>
                
                <!-- if email already is in db, show message -->
                <?php if(isset($email_exist_msg)) : ?>
                  <span class="error"><?php echo $email_exist_msg ?></span><br />
                <?php endif; ?>
              </p>


              <p>
                <label for="phone_number"><span class="required_sign">*&nbsp;</span>Phone</label>
                <input type="tel"
                       id="phone_number"
                       name="phone_number"
                       maxlength="30"
                       value="<?php 
                                if(!empty($customer['phone_number'])){
                                  echo esc ($customer['phone_number']);}
                              ?>"
                       placeholder="(000)222-3344"/><br />
                       
                <?php if(!empty($errors['phone_number'])) : ?>
                  <span class="error"><?=$errors['phone_number']?></span><br />
                <?php endif; ?>
              </p>


              <!-- ****** START BILLING INFO ******-->
              <div class="address_info_form billing_info">
                <p class="text_align_center">Billing Information</p>
                
                <p class="select_form_tag">
                  <label for="billing_province"><span class="required_sign">*&nbsp;</span>Province</label>
                  <select name="billing_province">
                    <option value="">Select Billing Province</option>
                    
                    <option 
                      <?php if(isset($customer['billing_province'])) : ?>
                        <?php if($customer['billing_province'] == 'Ontario') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Ontario">Ontario</option>
                    
                    <option 
                      <?php if(isset($customer['billing_province'])) : ?>
                        <?php if($customer['billing_province'] == 'Quebec') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Quebec">Quebec</option>
                    
                    <option 
                      <?php if(isset($customer['billing_province'])) : ?>
                        <?php if($customer['billing_province'] == 'British Columbia') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="British Columbia">British Columbia</option>
                    
                    <option 
                      <?php if(isset($customer['billing_province'])) : ?>
                        <?php if($customer['billing_province'] == 'Alberta') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Alberta">Alberta</option>
                    
                    <option 
                      <?php if(isset($customer['billing_province'])) : ?>
                        <?php if($customer['billing_province'] == 'Nova Scotia') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Nova Scotia">Nova Scotia</option>
                    
                    <option 
                      <?php if(isset($customer['billing_province'])) : ?>
                        <?php if($customer['billing_province'] == 'Saskatchewan') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Saskatchewan">Saskatchewan</option>
                    
                    <option 
                      <?php if(isset($customer['billing_province'])) : ?>
                        <?php if($customer['billing_province'] == 'Manitoba') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Manitoba">Manitoba</option>
                    
                    <option 
                      <?php if(isset($customer['billing_province'])) : ?>
                        <?php if($customer['billing_province'] == 'New Bruncwick') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="New Bruncwick">New Bruncwick</option>
                    
                    <option 
                      <?php if(isset($customer['billing_province'])) : ?>
                        <?php if($customer['billing_province'] == 'Newfoundland and Labrador') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Newfoundland and Labrador">Newfoundland and Labrador</option>
                    
                    <option 
                      <?php if(isset($customer['billing_province'])) : ?>
                        <?php if($customer['billing_province'] == 'Prince Edward Iceland') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Prince Edward Iceland">Prince Edward Iceland</option>
                    
                    <option 
                      <?php if(isset($customer['billing_province'])) : ?>
                        <?php if($customer['billing_province'] == 'Yukon') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Yukon">Yukon</option>
                    
                    <option 
                      <?php if(isset($customer['billing_province'])) : ?>
                        <?php if($customer['billing_province'] == 'Northwest Territories') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Northwest Territories">Northwest Territories</option>
                    
                    <option 
                      <?php if(isset($customer['billing_province'])) : ?>
                        <?php if($customer['billing_province'] == 'Nunavut') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Nunavut">Nunavut</option>
                    
                  </select> 
                </p>
                <?php if(!empty($errors['billing_province'])) : ?>
                  <span class="error"><?=$errors['billing_province']?></span>
                <?php endif; ?>


                <p>
                  <label for="billing_city"><span class="required_sign">*&nbsp;</span>City</label>
                  <input type="text"
                         id="billing_city"
                         name="billing_city"
                         maxlength="255"
                         value="<?php 
                                  if(!empty($customer['billing_city'])){
                                    echo esc ($customer['billing_city']);}
                                ?>"
                         placeholder="Type your billing city" /><br />
                         
                  <?php if(!empty($errors['billing_city'])) : ?>
                    <span class="error"><?=$errors['billing_city']?></span><br />
                  <?php endif; ?>
                </p>


                <p>
                  <label for="billing_street"><span class="required_sign">*&nbsp;</span>Street</label>
                  <input type="text"
                         id="billing_street"
                         name="billing_street"
                         maxlength="255"
                         value="<?php 
                                  if(!empty($customer['billing_street'])){
                                    echo esc ($customer['billing_street']);}
                                ?>"
                         placeholder="Type your billing street" /><br />
                         
                  <?php if(!empty($errors['billing_street'])) : ?>
                    <span class="error"><?=$errors['billing_street']?></span><br />
                  <?php endif; ?>
                </p>


                <p>
                  <label for="billing_apartment_number">Apartment</label>
                  <input type="text"
                         id="billing_apartment_number"
                         name="billing_apartment_number"
                         maxlength="10"
                         value="<?php 
                                  if(!empty($customer['billing_apartment_number'])){
                                    echo esc ($customer['billing_apartment_number']);}
                                ?>"
                         placeholder="Type your billing apartment #" /><br />
                         
                  <?php if(!empty($errors['billing_apartment_number'])) : ?>
                    <span class="error"><?=$errors['billing_apartment_number']?></span><br />
                  <?php endif; ?>
                </p>


                <p>
                  <label for="billing_postal_code"><span class="required_sign">*&nbsp;</span>Postal Code</label>
                  <input type="text"
                         id="billing_postal_code"
                         name="billing_postal_code"
                         maxlength="255"
                         value="<?php 
                                  if(!empty($customer['billing_postal_code'])){
                                    echo esc ($customer['billing_postal_code']);}
                                ?>"
                         placeholder="Type your billing postal code" /><br />
                         
                  <?php if(!empty($errors['billing_postal_code'])) : ?>
                    <span class="error"><?=$errors['billing_postal_code']?></span><br />
                  <?php endif; ?>
                </p>
              </div><!-- END #billing_info -->
              <!-- ****** END BILLING INFO ******-->



              <!-- ****** START SHIPPING INFO ******-->
              <div class="address_info_form">
                <p class="text_align_center">Shipping Information</p>
                
                <p class="select_form_tag">
                  <label for="shipping_province"><span class="required_sign">*&nbsp;</span>Province</label>
                  <select name="shipping_province">
                    <option value="">Select Shipping Province</option>
                    
                    <option 
                      <?php if(isset($customer['shipping_province'])) : ?>
                        <?php if($customer['shipping_province'] == 'Ontario') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Ontario">Ontario</option>
                    
                    <option 
                      <?php if(isset($customer['shipping_province'])) : ?>
                        <?php if($customer['shipping_province'] == 'Quebec') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Quebec">Quebec</option>
                    
                    <option 
                      <?php if(isset($customer['shipping_province'])) : ?>
                        <?php if($customer['shipping_province'] == 'British Columbia') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="British Columbia">British Columbia</option>
                    
                    <option 
                      <?php if(isset($customer['shipping_province'])) : ?>
                        <?php if($customer['shipping_province'] == 'Alberta') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Alberta">Alberta</option>
                    
                    <option 
                      <?php if(isset($customer['shipping_province'])) : ?>
                        <?php if($customer['shipping_province'] == 'Nova Scotia') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Nova Scotia">Nova Scotia</option>
                    
                    <option 
                      <?php if(isset($customer['shipping_province'])) : ?>
                        <?php if($customer['shipping_province'] == 'Saskatchewan') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Saskatchewan">Saskatchewan</option>
                    
                    <option 
                      <?php if(isset($customer['shipping_province'])) : ?>
                        <?php if($customer['shipping_province'] == 'Manitoba') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Manitoba">Manitoba</option>
                    
                    <option 
                      <?php if(isset($customer['shipping_province'])) : ?>
                        <?php if($customer['shipping_province'] == 'New Bruncwick') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="New Bruncwick">New Bruncwick</option>
                    
                    <option 
                      <?php if(isset($customer['shipping_province'])) : ?>
                        <?php if($customer['shipping_province'] == 'Newfoundland and Labrador') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Newfoundland and Labrador">Newfoundland and Labrador</option>
                    
                    <option 
                      <?php if(isset($customer['shipping_province'])) : ?>
                        <?php if($customer['shipping_province'] == 'Prince Edward Iceland') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Prince Edward Iceland">Prince Edward Iceland</option>
                    
                    <option 
                      <?php if(isset($customer['shipping_province'])) : ?>
                        <?php if($customer['shipping_province'] == 'Yukon') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Yukon">Yukon</option>
                    
                    <option 
                      <?php if(isset($customer['shipping_province'])) : ?>
                        <?php if($customer['shipping_province'] == 'Northwest Territories') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Northwest Territories">Northwest Territories</option>
                    
                    <option 
                      <?php if(isset($customer['shipping_province'])) : ?>
                        <?php if($customer['shipping_province'] == 'Nunavut') : ?> selected <?php endif; ?> 
                      <?php endif; ?>
                    value="Nunavut">Nunavut</option>
                    
                  </select> 
                </p>
                <?php if(!empty($errors['shipping_province'])) : ?>
                  <span class="error"><?=$errors['shipping_province']?></span>
                <?php endif; ?>


                <p>
                  <label for="shipping_city"><span class="required_sign">*&nbsp;</span>City</label>
                  <input type="text"
                         id="shipping_city"
                         name="shipping_city"
                         maxlength="255"
                         value="<?php 
                                  if(!empty($customer['shipping_city'])){
                                    echo esc ($customer['shipping_city']);}
                                ?>"
                         placeholder="Type your shipping city" /><br />
                         
                  <?php if(!empty($errors['shipping_city'])) : ?>
                    <span class="error"><?=$errors['shipping_city']?></span><br />
                  <?php endif; ?>
                </p>


                <p>
                  <label for="shipping_street"><span class="required_sign">*&nbsp;</span>Street</label>
                  <input type="text"
                         id="shipping_street"
                         name="shipping_street"
                         maxlength="255"
                         value="<?php 
                                  if(!empty($customer['shipping_street'])){
                                    echo esc ($customer['shipping_street']);}
                                ?>"
                         placeholder="Type your shipping street" /><br />
                         
                  <?php if(!empty($errors['shipping_street'])) : ?>
                    <span class="error"><?=$errors['shipping_street']?></span><br />
                  <?php endif; ?>
                </p>


                <p>
                  <label for="shipping_apartment_number">Apartment</label>
                  <input type="text"
                         id="shipping_apartment_number"
                         name="shipping_apartment_number"
                         maxlength="10"
                         value="<?php 
                                  if(!empty($customer['shipping_apartment_number'])){
                                    echo esc ($customer['shipping_apartment_number']);}
                                ?>"
                         placeholder="Type your shipping apartment #" /><br />
                         
                  <?php if(!empty($errors['shipping_apartment_number'])) : ?>
                    <span class="error"><?=$errors['shipping_apartment_number']?></span><br />
                  <?php endif; ?>
                </p>


                <p>
                  <label for="shipping_postal_code"><span class="required_sign">*&nbsp;</span>Postal Code</label>
                  <input type="text"
                         id="shipping_postal_code"
                         name="shipping_postal_code"
                         maxlength="10"
                         value="<?php 
                                  if(!empty($customer['shipping_postal_code'])){
                                    echo esc ($customer['shipping_postal_code']);}
                                ?>"
                         placeholder="Type your shipping postal code" /><br />
                         
                  <?php if(!empty($errors['shipping_postal_code'])) : ?>
                    <span class="error"><?=$errors['shipping_postal_code']?></span><br />
                  <?php endif; ?>
                </p>
              </div><!-- END #shipping_info -->
              <!-- ****** END SHIPPING INFO ******-->


              <input type="hidden" name="customer_id" value="<?=$customer['customer_id']?>" />

            </fieldset>


            <!-- *******  BUTTONS ************-->
            <p id="form_submit_buttons">
              <input type="submit"
                     value="Submit"
                     class="button" />&nbsp; &nbsp;
              <input type="button"
                     value="Clear Form"
                     onclick="clearForm(this.form);"
                     class="button" />
            </p>
            
            
            <!--****** SCRIPT FOR BUTTON "CLEAR FORM" *****  -->
            <script>
              //JS to clear 'input' fields
              function clearForm() {
                //clear input fields
                var tags = document.getElementsByTagName("input");
                for (i=0; i<tags.length; i++) {
                  if(tags[i].type == "button" || tags[i].type == "submit"){
                    continue;
                  }//END if
                  tags[i].value = '';
                }//END for
                
                //clear 'select' fields
                var tagss = document.getElementsByTagName("select");
                for (i=0; i<tagss.length; i++) {
                  if(tagss[i].type == "button" || tagss[i].type == "submit"){
                    continue;
                  }//END if
                  tagss[i].value = '';
                }//END for
                
              }//END clearForm()
            </script>
          
          </form>
        </div><!-- END #customer_edit_form -->
        
    </div><!-- Container ends -->



<?php include '../includes/footer.inc.php'; ?>