<?php
/**
 * Database module for Product Information
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

    /**
     * Get all of product information if possible by its id
     *
     * @return  array: an array of product information
     */
    function getCategories() {
      global $pdo;
      
        $query = "SELECT
                    *
                  FROM
                    categories
                  ORDER BY
                    RAND()
                  LIMIT
                    5
                 ";

        $stmt = $pdo->prepare($query);
        $stmt->execute();
        // Get the result
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Free all resource allocated
        $stmt = null;

        // Get back the result
        return $result;
    }



    /**
     * Get all of product information if possible by its id
     *
     * @param  pid: id of a product
     * @return  array: an array of product information
     */
    function retrieveProductInfo($pid, $activeproductcheck = true) {
        global $pdo;

        // Search only for active tours, not deleted tours
        $activeproduct = '';
        if ($activeproductcheck) {
          //$activeproduct = ' deleted = 0 AND ';
        }
        
        // Prepare the query
        $stmt = $pdo->prepare("SELECT `product_id`,
          `products`.`category_id`,
          `supplier_id`,
          `brand_id`,
          `product_name`,
          `weight`,
          `SKU`,
          `description`,
          `price`,
          `availability`,
          `units_sold`,
          `units_in_stock`,
          `full_image1`,
          `full_image2`,
          `full_image3`,
          `thumbnail_image`,
          `products`.`created_at`,
          `products`.`updated_at`, 
          `products`.`deleted`,
          `categories`.`category_name`
          FROM `products` 
          INNER JOIN categories ON products.category_id = categories.category_id 
          WHERE 
          $activeproduct
          product_id = ?");
        // Do it with id
        $stmt->execute([$pid]);
        // Get the result
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Free all resource allocated
        $stmt = null;


        // To prevent XSS attacks
        if (count($result) > 0) {
          // There is only one customer found, pick one row only
          $result = getArrayForForm($result[0]);
          
          return $result;
        }

        // Get back the result
        return array();
    }
    // --------------------------------------------------------------------


    /**
     * Get all of payment information from a customer
     *
     * @param  customerid: id of a customer
     * @return  array: an array of payment information
     */
    function retrievePaymentInfo($customerid) {
        global $pdo;

        // First, we are looking into the lastest invoice of the user to get the lastest update of payment
        $stmt = $pdo->prepare("SELECT 
          `first_name`,
          `last_name`,
          `email`,
          `phone_number`,
          `country`,
          `billing_province`,
          `billing_city`,
          `billing_street`,
          `billing_apartment_number`,
          `billing_postal_code`,
          `shipping_province`,
          `shipping_city`,
          `shipping_street`,
          `shipping_apartment_number`,
          `shipping_postal_code`,
          `created_at`,
          `updated_at`,
          `deleted`
          FROM `invoices` 
          WHERE 
          invoice_id = (
            SELECT MAX(invoice_id)
            FROM `invoices` 
            WHERE customer_id = :cid
          )");
        // Do it with customer id
        $stmt->execute([$customerid]);
        // Get the result
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Free all resource allocated
        $stmt = null;


        // To prevent XSS attacks
        if (count($result) > 0) {
          // There is only one invoice found, pick one row only
          $result = getArrayForForm($result[0]);
          
          return $result;
        }

        // Look at the customer info
        $stmt = $pdo->prepare("SELECT 
          `first_name`,
          `last_name`,
          `email`,
          `phone_number`,
          `country`,
          `billing_province`,
          `billing_city`,
          `billing_street`,
          `billing_apartment_number`,
          `billing_postal_code`,
          `shipping_province`,
          `shipping_city`,
          `shipping_street`,
          `shipping_apartment_number`,
          `shipping_postal_code`,
          `created_at`,
          `updated_at`,
          `deleted`
          FROM `customers` 
          WHERE customer_id = :cid ");
        // Do it with customer id
        $stmt->execute([$customerid]);
        // Get the result
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Free all resource allocated
        $stmt = null;


        // To prevent XSS attacks
        if (count($result) > 0) {
          // There is only one invoice found, pick one row only
          $result = getArrayForForm($result[0]);
          
          return $result;
        }
      
      // I have no idea when coming to this place
      return array();

    }
    // --------------------------------------------------------------------

    /**
     * orderProduct, put information from a cart into orders
     *
     * @param  customerid: id of a customer
     * @param  cartArr: cart info
     * @param  paymentinfo: payment info
     * @param  tid: transaction id
     * @param  response: Responsed object from card transaction
     * @return  array: an empty array if successful or an array of product information that were booked by other customers
     */
    function orderProduct($customerid, $cartArr, $paymentinfo, $tid, $response) {
        global $pdo;

        // If Cart is not empty
        if (count($cartArr)>0) {
        
          // Find tours which are booked by someone else by looking at a decreased units_in_stock
          $query = '';
          foreach ($cartArr as $key => $row) {
            $quantity = intval($row['quantity']);
            $query .= " OR (product_id = $key AND units_in_stock < $quantity)";
          }

          // Remove the first OR to prevent syntax error :)
          if (strlen($query)>0) {
            $query = "SELECT `product_id`
              FROM `products` 
              WHERE " . substr($query, 4);
          }
          
          // Prepare the query
          $stmt = $pdo->prepare($query);
          // Do it with id
          $stmt->execute();
          // Get the result
          $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
          // Free all resource allocated
          $stmt = null;


          // To prevent XSS attacks
          if (count($result) > 0) {
            // Prevent XSS attacks
            $result = getArrayForForm($result);

            // Yes, there are some products which are booked by others
            return $result;
          } else {
            // OK, No product in the cart is booked, go on with our orders
            try {
              // Create a new invoice for a product 
              $stmt = $pdo->prepare("INSERT INTO `invoices`
              (`invoice_date`,
              `customer_id`,
              `first_name`,
              `last_name`,
              `email`,
              `phone_number`,
              `country`,
              `billing_province`,
              `billing_city`,
              `billing_street`,
              `billing_apartment_number`,
              `billing_postal_code`,
              `shipping_province`,
              `shipping_city`,
              `shipping_street`,
              `shipping_apartment_number`,
              `shipping_postal_code`,
              `shipping_status`,
              `transaction_status`,
              `updated_at`,
              `deleted`)
              VALUES
              (now(),
              :customer_id,
              :first_name,
              :last_name,
              :email,
              :phone_number,
              :country,
              :billing_province,
              :billing_city,
              :billing_street,
              :billing_apartment_number,
              :billing_postal_code,
              :shipping_province,
              :shipping_city,
              :shipping_street,
              :shipping_apartment_number,
              :shipping_postal_code,
              0,
              1,
              now(),
              0)"
              );

              $stmt->bindParam(':customer_id', $customerid, PDO::PARAM_INT);
              $stmt->bindParam(':first_name', $paymentinfo['first_name'], PDO::PARAM_STR);
              $stmt->bindParam(':last_name', $paymentinfo['last_name'], PDO::PARAM_STR);
              $stmt->bindParam(':email', $paymentinfo['email'], PDO::PARAM_STR);
              $stmt->bindParam(':phone_number', $paymentinfo['phone_number'], PDO::PARAM_STR);
              $stmt->bindParam(':country', $paymentinfo['country'], PDO::PARAM_STR);
              $stmt->bindParam(':billing_province', $paymentinfo['billing_province'], PDO::PARAM_STR);
              $stmt->bindParam(':billing_city', $paymentinfo['billing_city'], PDO::PARAM_STR);
              $stmt->bindParam(':billing_street', $paymentinfo['billing_street'], PDO::PARAM_STR);
              $stmt->bindParam(':billing_apartment_number', $paymentinfo['billing_apartment_number'], PDO::PARAM_STR);
              $stmt->bindParam(':billing_postal_code', $paymentinfo['billing_postal_code'], PDO::PARAM_STR);
              $stmt->bindParam(':shipping_province', $paymentinfo['shipping_province'], PDO::PARAM_STR);
              $stmt->bindParam(':shipping_city', $paymentinfo['shipping_city'], PDO::PARAM_STR);
              $stmt->bindParam(':shipping_street', $paymentinfo['shipping_street'], PDO::PARAM_STR);
              $stmt->bindParam(':shipping_apartment_number', $paymentinfo['shipping_apartment_number'], PDO::PARAM_STR);
              $stmt->bindParam(':shipping_postal_code', $paymentinfo['shipping_postal_code'], PDO::PARAM_STR);

              // Do it 
              $stmt->execute();

              // Free all resource allocated
              $stmt = null;

              $invoiceid = $pdo->lastInsertId();
              
              foreach ($cartArr as $key => $row) {
                // Update available PAX in tour
                $stmt = $pdo->prepare("UPDATE `products`
                  SET
                  units_sold = units_sold + :quantity1,
                  units_in_stock = units_in_stock - :quantity2,
                  availability = units_in_stock > :quantity3,
                  updated_at = NOW()
                  WHERE product_id = :product_id"
                  );

                $stmt->bindParam(':product_id', $key, PDO::PARAM_INT);
                
                $quantity1 = $row['quantity'];
                $stmt->bindParam(':quantity1', $quantity1, PDO::PARAM_INT);
                $quantity2 = $row['quantity'];
                $stmt->bindParam(':quantity2', $quantity2, PDO::PARAM_INT);
                $quantity3 = $row['quantity'];
                $stmt->bindParam(':quantity3', $quantity3, PDO::PARAM_INT);

                                
                // Do it 
                $stmt->execute();
                // Free all resource allocated
                $stmt = null;
                
                // Create a new order for a tour in the Cart
                $stmt = $pdo->prepare("INSERT INTO `product_invoice`
                  (`product_id`,
                  `invoice_id`,
                  `quantity`,
                  `weight`,
                  `product_name`,
                  `price`,
                  `gst`,
                  `pst`,
                  `shipping_cost`,
                  `subtotal`,
                  `total`)
                  VALUES
                  (:product_id,
                  :invoice_id,
                  :quantity,
                  :weight,
                  :product_name,
                  :price,
                  :gst,
                  :pst,
                  :shipping_cost,
                  :subtotal,
                  :total)"
                  );
                
                // Customer id
                $stmt->bindParam(':invoice_id', $invoiceid, PDO::PARAM_INT);
                // Product id
                $stmt->bindParam(':product_id', $key, PDO::PARAM_INT);
                // Quantity
                $stmt->bindParam(':quantity', $row['quantity'], PDO::PARAM_INT);
                // Product name
                $stmt->bindParam(':product_name', $row['title'], PDO::PARAM_STR);
                // Weight
                $stmt->bindParam(':weight', $row['weight'], PDO::PARAM_STR);
                // Price
                $stmt->bindParam(':price', $row['price'], PDO::PARAM_STR);
                $subtotal = $row['price'] * $row['quantity'];
                $gst = calculateGST($subtotal);
                $pst = calculatePST($subtotal, $paymentinfo['billing_province']);
                $shippingcost = calculateShippingCost($subtotal);
                $grandtotal = $subtotal + $pst + $gst + $shippingcost;
                // GST
                $stmt->bindParam(':gst', $gst, PDO::PARAM_STR);
                // PST
                $stmt->bindParam(':pst', $pst, PDO::PARAM_STR);
                // shipping_cost
                $stmt->bindParam(':shipping_cost', $shippingcost, PDO::PARAM_STR);
                // Sub Total
                $stmt->bindParam(':subtotal', $subtotal, PDO::PARAM_STR);
                // Grand Total
                $stmt->bindParam(':total', $grandtotal, PDO::PARAM_STR);

                // Do it 
                $stmt->execute();
                // Free all resource allocated
                $stmt = null;

              }
              
              
              // Update the transaction with successful status
              updateTransaction($tid, $invoiceid, $response->transaction_response->auth_code, json_encode($response));
              

            } catch (Exception $e) {
              // There is a problem in database, we let users know
              return array('error' => $e->getMessage());
            }
            
          }

        }
      
      // Everything is OK
      return array();
    }

    // --------------------------------------------------------------------


    /**
     * Get the last inserted id if possible
     *
     * @return  string: the desired id
     */
    function getLastInsertedId() {
        global $pdo;
        return $pdo->lastInsertId();
    }

    // --------------------------------------------------------------------

    /**
     * initTransaction, create a transaction
     *
     * @param  customerid: id of a customer
     * @param  paymentinfo: payment info
     * @return  id of created transaction
     */
    function initTransaction($customerid, $paymentinfo) {
        global $pdo;

          try {
            // Create a new transaction for a product 
            $stmt = $pdo->prepare("INSERT INTO `transactions`
            (`customer_id`,
            `invoice_id`,
            `card_number`,
            `transaction_code`,
            `response`)
            VALUES
            (:customer_id,
            0,
            :card_number,
            0,
            null)"
            );

            $stmt->bindParam(':customer_id', $customerid, PDO::PARAM_INT);
            // Only 4 last digits is needed
            $last4digits = substr(trim($paymentinfo['card_number']), -4);
            $stmt->bindParam(':card_number', $last4digits, PDO::PARAM_STR);

            // Do it 
            $stmt->execute();

            // Free all resource allocated
            $stmt = null;

            $tid = $pdo->lastInsertId();
            
            return $tid;
            
          } catch (Exception $e) {
            // There is a problem in database, we let users know
            die($e->getMessage());
          }

    }

    // --------------------------------------------------------------------

    /**
     * updateTransaction, update a transaction
     *
     * @param  customerid: id of a customer
     * @param  paymentinfo: payment info
     * @return  id of created transaction
     */
    function updateTransaction($tid, $invoiceid, $status, $response) {
        global $pdo;

          try {
            // Create a new transaction for a product 
            $stmt = $pdo->prepare("UPDATE `transactions`
            SET
            `invoice_id` = :invoice_id,
            `transaction_code` = :transaction_code,
            `response` = :response
            WHERE `transaction_id` = :transaction_id");

            $stmt->bindParam(':invoice_id', $invoiceid, PDO::PARAM_INT);
            $stmt->bindParam(':transaction_code', $status, PDO::PARAM_STR);
            $stmt->bindParam(':response', $response, PDO::PARAM_STR);
            $stmt->bindParam(':transaction_id', $tid, PDO::PARAM_INT);

            // Do it 
            $stmt->execute();

            // Free all resource allocated
            $stmt = null;

          } catch (Exception $e) {
            // There is a problem in database
            die($e->getMessage());
          }

    }

    // --------------------------------------------------------------------



?>