<?php
/**
 * Database Module
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------


$dsn = "{$db['default']['dbdriver']}:host={$db['default']['hostname']};dbname={$db['default']['database']};charset={$db['default']['charset']}";

$options = [
  PDO::ATTR_EMULATE_PREPARES   => false, // turn off emulation mode for "real" prepared statements
  PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION, //turn on errors in the form of exceptions
  PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, //make the default fetch be an associative array
];


// Setting up the database connection
try {
  $pdo = new PDO($dsn, $db['default']['username'], $db['default']['password'], $options);
} catch (Exception $e) {
  error_log($e->getMessage());
  die($e->getMessage());
}


?>