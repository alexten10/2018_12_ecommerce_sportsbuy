<?php

/**
 * Products
 * @products.php
 * @course Ecommerce Project, WDD 2018 Jan
 * @author Amarjeet Sharma <Sharmajeetamar31@gmail.com>
 * @created_at 2018-12-01
 **/

// Function to fetch all product details from the database
/**
 * @param  [object] $dbh is database handler
 * @return [array] array with details of multiple products
 */
function getProducts($dbh)
{  
  // Create query
  $query = "SELECT  products.product_id,
                    products.category_id,
                    products.supplier_id,
                    products.brand_id,
                    products.product_name,
                    products.weight,
                    products.SKU,
                    products.price,
                    products.availability,
                    products.units_sold,
                    products.units_in_stock,
                    products.deleted,
                    categories.category_name,
                    suppliers.supplier_name,
                    brands.brand_name
            FROM products
            INNER JOIN categories ON products.category_id = categories.category_id
            INNER JOIN suppliers ON products.supplier_id = suppliers.supplier_id
            INNER JOIN brands ON products.brand_id = brands.brand_id
            ORDER BY product_id";
  
  // Prepare query
  $stmt = $dbh->prepare($query);
  
  // Execute query
  $stmt->execute();

  // fetch multiple products
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


// Function to fetch all customer details from the database
/**
 * @param  [object] $dbh is database handler
 * @return [array] array with details of multiple customers
 */
function getCustomers($dbh)
{  
  // Create query
  $query = "SELECT * FROM customers";
  
  // Prepare query
  $stmt = $dbh->prepare($query);
  
  // Execute query
  $stmt->execute();

  // fetch multiple products
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

// Function to fetch aggregate data of products from the DB
/**
 * [aggData description]
 * @param  [object] $dbh [database handler]
 * @return [array]      [array of aggregate values]
 */
function aggData($dbh)
{
  // Create query
  $query = "SELECT SUM(price),
                   AVG(price), 
                   MAX(price), 
                   MIN(price),
                   MAX(units_sold),
                   MIN(units_sold),
                   MAX(units_in_stock),
                   AVG(units_in_stock),
                   MIN(units_in_stock)
            FROM products";
  
  // Prepare query
  $stmt = $dbh->prepare($query);  

  // Execute query
	$stmt->execute();
  
	// fetch multiple products
	return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


// Function to get product details from DB to show on admin page
/**
 * @param  [object] $dbh is database handler
 * @param  [integer] $product_id is used to shortlist & fetch products based on product id
 * @return [array] returns array of product details 
 */
function adminProducts($dbh, $product_id)
{
   // Create query
  $query = "SELECT  products.product_id,
                    products.category_id,
                    products.supplier_id,
                    products.brand_id,
                    products.product_name,
                    products.weight,
                    products.SKU,
                    products.price,
                    products.availability,
                    products.units_sold,
                    products.units_in_stock,
                    products.deleted,
                    categories.category_name,
                    suppliers.supplier_name,
                    brands.brand_name
            FROM products
            INNER JOIN categories ON products.category_id = categories.category_id
            INNER JOIN suppliers ON products.supplier_id = suppliers.supplier_id
            INNER JOIN brands ON products.brand_id = brands.brand_id
            WHERE products.product_id = :product_id";
  
  // Prepare query
  $stmt = $dbh->prepare($query);
  
  // Bind values
  $stmt->bindValue(':product_id', $product_id, PDO::PARAM_INT);
  
  // Execute query
  $stmt->execute();

  // fetch multiple products
  return $stmt->fetch(PDO::FETCH_ASSOC);
}

// Function to fetch all product categories from the database
/**
 * @param  [object] $dbh is database handler
 * @return [array] array with details of multiple product categories
 */
function adminCategories($dbh)
{  
  // Create query
  $query = "SELECT  * FROM categories";
  
  // Prepare query
  $stmt = $dbh->prepare($query);
  
  // Execute query
  $stmt->execute();

  // fetch multiple categories
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

// Function to fetch all suppliers from the database
/**
 * @param  [object] $dbh is database handler
 * @return [array] array with details of multiple suppliers
 */
function adminSuppliers($dbh)
{  
  // Create query
  $query = "SELECT  * FROM suppliers";
  
  // Prepare query
  $stmt = $dbh->prepare($query);
  
  // Execute query
  $stmt->execute();

  // fetch multiple suppliers
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

// Function to fetch all brands from the database
/**
 * @param  [object] $dbh is database handler
 * @return [array] array with details of multiple brands
 */
function adminBrands($dbh)
{  
  // Create query
  $query = "SELECT  * FROM brands";
  
  // Prepare query
  $stmt = $dbh->prepare($query);
  
  // Execute query
  $stmt->execute();

  // fetch multiple brands
  return $stmt->fetchAll(PDO::FETCH_ASSOC);
}


// Function to get customer details from DB to show on admin page(edit_customer.php)
/**
 * @param  [object] $dbh is database handler
 * @param  [integer] $customer_id is used to shortlist & fetch customer based on customer id
 * @return [array] returns array of customer details 
 */

/*
function adminCustomer($dbh, $product_id)
{
   // Create query
  $query = "SELECT  customer_id,
                    first_name,
                    last_name,
                    email,
                    phone_number,
                    billing_province,
                    billing_city,
                    billing_street,
                    billing_apartment_number,
                    billing_postal_code,
                    shipping_province,
                    shipping_city,
                    shipping_street,
                    shipping_apartment_number,
                    shipping_postal_code,
                    password

            FROM customers
            WHERE customer_id = :customer_id";
  
  // Prepare query
  $stmt = $dbh->prepare($query);
  
  // Bind values
  $stmt->bindValue(':customer_id', $customer_id, PDO::PARAM_INT);
  
  // Execute query
  $stmt->execute();

  // fetch multiple products
  return $stmt->fetch(PDO::FETCH_ASSOC);
}


*/

?>