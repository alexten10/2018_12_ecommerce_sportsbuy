<?php
/**
 * Configuration module
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

ini_set('display_errors', true);
ini_set('error_reporting', E_ALL);

define('CONFIG_PATH', '../includes/');

define('LOGIN_ID', '5664351');
define('API_KEY', '0069630dfa16c596d47aa53d0fced9bf');
  
define('SHIPPING_COST_RATE', 0.009);
define('GST_RATE', 0.05);

// Database credentials for localhost
define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_DSN', 'mysql:host=localhost;dbname=ecommerce');

// All database declarations should be here
$db['default']['dbdriver'] = "mysql";
$db['default']['hostname'] = "localhost";
$db['default']['username'] = DB_USER;
$db['default']['password'] = DB_PASS;
$db['default']['database'] = "ecommerce";
$db['default']['charset'] = "utf8";

define('APP', __DIR__ . DIRECTORY_SEPARATOR);
define('CONTROL_CONFIG_PATH', APP . 'controls' . DIRECTORY_SEPARATOR);
define('MODEL_CONFIG_PATH', APP . 'models' . DIRECTORY_SEPARATOR);
define('VIEW_CONFIG_PATH', APP . 'views' . DIRECTORY_SEPARATOR);

$provincePST = [
  'Ontario' => 0.09, 
  'Quebec' => 0.1, 
  'British Columbia' => 0.08, 
  'Alberta' => 0, 
  'Nova Scotia' => 0.07, 
  'Saskatchewan' => 0.08, 
  'Manitoba' => 0.09, 
  'New Bruncwick' => 0.07, 
  'Newfoundland and Labrador' => 0.08, 
  'Prince Edward Iceland' => 0.09, 
  'Yukon' => 0.07, 
  'Northwest Territories' => 0.08, 
  'Nunavut' => 0.09
];

$provinces = array_keys($provincePST);
  
// Get current page in either GET OR POST as possible
$currentpage = (isset($_REQUEST['p'])) ? $_REQUEST['p'] : '';

function autoload($className) {
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    require APP . $fileName;
}

// Setting up the database connection
try {
  // 1. connecting to database "ecommerce"
  $dbh= new PDO(DB_DSN, DB_USER, DB_PASS);
  // 2. to show errors within db if errors occur
  $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (Exception $e) {
  error_log($e->getMessage());
  die($e->getMessage());
}

spl_autoload_register('autoload');

session_start();

//this session might come from admin site
if(isset($_SESSION['logged_admin'])) {
  unset ($_SESSION['logged_admin']);
  session_regenerate_id();
}

?>