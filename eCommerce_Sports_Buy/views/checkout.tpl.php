<?php
$cardtypes = ['Visa', 'Mastercard', 'Amex'];
// List out all cardtypes
$typelist = '';

if (count($cardtypes)>0) {
  $typelist .= '<select id="cardtypeid" name="cardtype" class="form-control" >';
  foreach ($cardtypes as $cardtype) {
    $typelist .= "<option value=\"$cardtype\">$cardtype</option>";
  }
  $typelist .= '</select>';
}

// List out all provinces
$shipprovincelist = '';
$billprovincelist = '';

if (count($provinces)>0) {
  $shipprovincelist .= '<select id="shipprovince" name="shipprovince" class="form-control" >';
  $billprovincelist .= '<select id="billprovince" name="billprovince" class="form-control" onchange="reCalcPST(this.options[this.selectedIndex].value);" >';
  foreach ($provinces as $province) {
    if ($paymentinfo['shipping_province'] === $province) {
      $shipprovincelist .= "<option value=\"$province\" selected>$province</option>";
    } else {
      $shipprovincelist .= "<option value=\"$province\">$province</option>";
    }
    if ($paymentinfo['billing_province'] === $province) {
      $billprovincelist .= "<option value=\"$province\" selected>$province</option>";
    } else {
      $billprovincelist .= "<option value=\"$province\">$province</option>";
    }
  }
  $shipprovincelist .= '</select>';
  $billprovincelist .= '</select>';
}

$container = <<<EOT

        <div class="container-fluid">
            <div class="row" id='checkoutheader'>
                <div class="row"></div>
                <div id='cartsection' >
                    <div class="panel panel-warning text-center">
                        <div class="panel-heading title">Cart Checkout</div>
                        <div class="panel-body"><div class="col-md-12 alert text-success" id="cartmsg"></div></div>
                        <div class="row" id='checkoutdetail'>
                            <div class="col-md-2"><b>Image</b></div>
                            <div class="col-md-2"><b>Name</b></div>
                            <div class="col-md-2"><b>Price</b></div>
                            <div class="col-md-2"><b>Quantity</b></div>
                            <div class="col-md-2"><b>Price in $</b></div>
                            <div class="col-md-2"><b>Action</b></div>
                        </div>
                        <br>
                        <div id='cartdetail'>
                        </div>
                        <div class="panel-footer">

                        </div>
                    </div>
                </div>
                <div id='paymentsection' >
                    <div class="panel panel-warning text-center">
                        <div class="panel-heading title">Payment Information</div>
                        <div class="panel-body"><div class="col-md-12 alert text-danger" id="paymentmsg"></div></div>
                        <div class="row">
                            <div class="col-md-2"><b>First Name</b></div>
                            <div class="col-md-3"><input id='firstname' class='form-control' type='text' value='{$paymentinfo['first_name']}'/></div>
                            <div class="col-md-2"><b>Last Name</b></div>
                            <div class="col-md-4"><input id='lastname' class='form-control' type='text' value='{$paymentinfo['last_name']}'/>
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-2"><b>Phone</b></div>
                            <div class="col-md-3"><input id='phone' class='form-control' type='text' value='{$paymentinfo['phone_number']}'/></div>
                            <div class="col-md-2"><b>Email</b></div>
                            <div class="col-md-4"><input id='email' class='form-control' type='text' value='{$paymentinfo['email']}'/></div>
                            <div class="col-md-1"></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-2"><b>Shipping Province</b></div>
                            <div class="col-md-3">
                            $shipprovincelist
                            </div>
                            <div class="col-md-2"><b>Shipping City</b></div>
                            <div class="col-md-4"><input id='shipcity' class='form-control' type='text' value='{$paymentinfo['shipping_city']}'/></div>
                            <div class="col-md-1"></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-2"><b>Shipping Street</b></div>
                            <div class="col-md-3"><input id='shipstreet' class='form-control' type='text' value='{$paymentinfo['shipping_street']}' /></div>
                            <div class="col-md-2"><b>Apartment Number</b></div>
                            <div class="col-md-4"><input id='shipaptno' class='form-control' type='text' value='{$paymentinfo['shipping_apartment_number']}'/></div>
                            <div class="col-md-1"></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-2"><b>Shipping Postal Code</b></div>
                            <div class="col-md-3"><input id='shippostalcode' class='form-control' type='text' value='{$paymentinfo['shipping_postal_code']}'/></div>
                            <div class="col-md-2"></div>
                            <div class="col-md-4"></div>
                            <div class="col-md-1"></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-2"><b>Billing Province</b></div>
                            <div class="col-md-3">
                            $billprovincelist
                            </div>
                            <div class="col-md-2"><b>Billing City</b></div>
                            <div class="col-md-4"><input id='billcity' class='form-control' type='text' value='{$paymentinfo['billing_city']}'/></div>
                            <div class="col-md-1"></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-2"><b>Billing Street</b></div>
                            <div class="col-md-3"><input id='billstreet' class='form-control' type='text' value='{$paymentinfo['billing_street']}' /></div>
                            <div class="col-md-2"><b>Apartment Number</b></div>
                            <div class="col-md-4"><input id='billaptno' class='form-control' type='text' value='{$paymentinfo['billing_apartment_number']}'/></div>
                            <div class="col-md-1"></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-2"><b>Billing Postal Code</b></div>
                            <div class="col-md-3"><input id='billpostalcode' class='form-control' type='text' value='{$paymentinfo['billing_postal_code']}'/></div>
                            <div class="col-md-2"></div>
                            <div class="col-md-4"></div>
                            <div class="col-md-1"></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-2"><b>Card Number</b></div>
                            <div class="col-md-3"><input id='cardnumber' class='form-control' type='text' value='' /></div>
                            <div class="col-md-2"><b>Expiry Date (MMYY)</b></div>
                            <div class="col-md-4"><input id='expirydate' class='form-control' type='text' value=''/></div>
                            <div class="col-md-1"></div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-md-2"><b>CVV</b></div>
                            <div class="col-md-3"><input id='cvv' class='form-control' type='text' value='' /></div>
                            <div class="col-md-2"><b>Card Type</b></div>
                            <div class="col-md-4">$typelist</div>
                            <div class="col-md-1"></div>
                        </div>
                        <br/>
                        <div class="panel-footer">

                        </div>
                    </div>
                    <button class='btn btn-success btn-lg pull-right' id='checkoutbtn' data-toggle="modal" data-target="#myModal">Checkout</button>

                </div>
                <div class="col-md-2"></div>
            </div>
        </div>
        

            
    <script>
      // Implement the cart checkout
      function cartcheckout(){
          $.ajax({
              url: 'action.php',
              method: 'POST',
              data: {cartcheckout:1 $jsprovinceparam},
              success: function(data){
                // Empty Cart is found?
                if (data.indexOf('<div') < 0) {
                  $('#cartdetail').hide();
                  $('#checkoutdetail').hide();
                  $('#paymentsection').hide();
                  
                  // Put message in the cart message
                  $('#cartmsg').html(data);
                  //console.log(data);
                } else {
                  $('#cartdetail').html(data);
                  
                }
              }
          })
      }
      
      cartcheckout();

      // Remove button handler
      $('body').delegate('.remove','click',function(e){
          e.preventDefault();
          var pid=$(this).attr('data-id');
          $.ajax({
              url: 'action.php',
              method: 'POST',
              data: {removefromcart:1,pid:pid},
              success: function(data){
                  $('#cartmsg').html(data);
                  cartcheckout();
              }
          })
      })

      // Update button handler
      $('body').delegate('.update','click',function(e){
          e.preventDefault();
          var pid=$(this).attr('data-id');
          var quantity=$('#qty-'+pid).val();
          // Is numeric quantity ?
          
          var result = quantity.match(/^([0-9]+)$/);
          // Not a numeric ? forget it.
          if (!result) {
            $('#cartmsg').html('The quantity which you entered is not a number.');
            return;
          }
          
          $.ajax({
              url: 'action.php',
              method: 'POST',
              data: {updatecart:1,pid:pid,quantity:quantity},
              success: function(data){
                  $('#cartmsg').html(data);
                  cartcheckout();
              }
          })
      }) 
            
      function reCalcPST(province){
          amounttotal = $('#amounttotal').html();
          gst = $('#gst').html();
          shippingcost = $('#shippingcost').html();
          //console.log('['+amounttotal+']');
          //console.log(province);
          $.ajax({
              url: 'action.php',
              method: 'POST',
              data: {getPST:1,ammount:amounttotal,province:province},
              success: function(data){
                  //console.log(data);
                  pst = data;
                  grandtotal = parseFloat(amounttotal)+parseFloat(pst)+parseFloat(gst)+parseFloat(shippingcost);
                  console.log(grandtotal);
                  grandtotal = +grandtotal.toFixed(2);
                  
                  // Update GST and grandtotal
                  $('#pst').html(data);
                  $('#grandtotal').html(grandtotal);
                  
              }
          })
      }
              
      // Do the payment
      $('#checkoutbtn').click(function(){
          o = document.getElementById("cardtypeid");
          cardtypevalue = o.options[o.selectedIndex].value;
          //console.log(cardtypevalue);
          o = document.getElementById("shipprovince");
          shipprovincevalue = o.options[o.selectedIndex].value;
          //console.log(shipprovincevalue);
          o = document.getElementById("billprovince");
          billprovincevalue = o.options[o.selectedIndex].value;
          //console.log(billprovincevalue);
          
          console.log($('#grandtotal').val());
          $.ajax({
              url: 'action.php',
              method: 'POST',
              data: {paymentcheckout:1,
                csrf: '$csrfkey',
                grandtotal: $('#grandtotal').val(),
                first_name: $('#firstname').val(),
                last_name: $('#lastname').val(),
                phone_number: $('#phone').val(),
                email: $('#email').val(),
                shipping_province: shipprovincevalue,
                shipping_city: $('#shipcity').val(),
                shipping_street: $('#shipstreet').val(),
                shipping_apartment_number: $('#shipaptno').val(),
                shipping_postal_code: $('#shippostalcode').val(),
                billing_province: billprovincevalue,
                billing_city: $('#billcity').val(),
                billing_street: $('#billstreet').val(),
                billing_apartment_number: $('#billaptno').val(),
                billing_postal_code: $('#billpostalcode').val(),
                card_number: $('#cardnumber').val(),
                expirydate: $('#expirydate').val(),
                cvv: $('#cvv').val(),
                cardtype: cardtypevalue
                },
              success: function(data){
                  if (data.startsWith('OK')) {
                    window.location.href = "?p=checkoutcomplete.php&auth_code="+data.substring(2);
                    return;
                  }
                  //console.log(data);
                  // Is an error message, print it out
                  if (data.indexOf('<div') < 0) {
                    $('#paymentmsg').html(data);
                    $('html, body').animate({scrollTop: $("#paymentsection").offset().top}, 500);
                  } else {
                    $('#cartmsg').html('Sorry, please review the following out of stock items in your shoppinng cart:');
                    $('#cartdetail').html(data);
                    $('html, body').animate({scrollTop: $("#cartsection").offset().top}, 500);
                  }
              }
          })
      })

    </script>
    
    
</body>
</html>
EOT;
?>