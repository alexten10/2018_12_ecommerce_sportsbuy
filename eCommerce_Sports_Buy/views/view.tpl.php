<?php
// Include all structure view files: header + [specific page] + category + footer
require(VIEW_CONFIG_PATH.'header.tpl.php');
require(VIEW_CONFIG_PATH.'category.tpl.php');
require(VIEW_CONFIG_PATH.'footer.tpl.php');

$content = $header . $container . $category . $footer;
  
// Display the content of the page
echo $content;

?>