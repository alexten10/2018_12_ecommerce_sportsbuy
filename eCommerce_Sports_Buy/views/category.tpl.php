<?php
// Get all categories
$categories = getCategories();

$thecatcontent = '';

foreach($categories as $key => $value) : 
            
  $thecatcontent .= '            <li>';
  $thecatcontent .= "              <a href=\"products.php?category={$value['category_id']}\">";
  $thecatcontent .= "                <img src=\"images/category_images/{$value['category_image']}\" alt=\"{$value['category_name']}\" />";
  $thecatcontent .= '              </a>';
  $thecatcontent .= "              <p>{$value['category_name']}</p>";
  $thecatcontent .= '            </li>';

endforeach; 
          

$category = <<<EOT

      <!-- shop_by_category starts here -->
      <div id="shop_by_category">
        <h1>Shop by Category</h1>
        <ul>
        $thecatcontent          
        </ul>
      </div><!-- shop_by_category ends -->

    </div><!-- Container ends -->

EOT;
?>