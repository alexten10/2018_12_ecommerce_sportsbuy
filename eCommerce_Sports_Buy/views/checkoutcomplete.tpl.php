<?php
$container = <<<EOT
	
    <div id='checkoutcomplete'></div>
	<div class='container-fluid checkoutcomplete'>
		<div class='row'>
		  <div class='col-md-2'></div>
		  <div class='col-md-8'>
			<div class="panel panel-default">
  				<div class="panel-heading"><h1>Thank you!</h1></div>
  				<div class="panel-body">
    				Hello, the payment is processed successfully.
    				<br>Your Payment Transaction ID is $tranid 
    				<br>Have a good day on shopping.
    				<p></p>
                    <a href="index.php" class='btn btn-success btn-lg'>Back to shopping</a>
  				</div>
			</div>
		  <div class='col-md-2'></div>
	     </div>

    </div>

	<script type="text/javascript">
		
    	
    	$(".checkoutprocess").fadeOut(1000, function(){
          $(".checkoutcomplete").fadeIn(1500);        	
		}); 

	</script>
EOT;
?>