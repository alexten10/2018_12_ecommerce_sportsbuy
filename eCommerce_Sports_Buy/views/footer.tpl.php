<?php
$footer = <<<EOT
    <div class="clear_both"></div>





    <!-- ############################################################################-->
    <!-- ###########################  START footer  #################################-->
    <!-- Footer starts here -->
    <div id="footer">

      <!-- footer_up(upper part of the footer with footer navigation menu) starts here -->
      <div id="footer_up">

        <!-- Company menu links-->
        <div id="footer_company">
          <h1>Company</h1>
          <hr>
          <ul>
            <li><a href="about_us.php">About Us</a></li>
            <li><a href="contact_us.php">Contact Us</a></li>
            <li><a href="privacy.php">Privacy Policy</a></li>
            <li><a href="terms_and_conditions.php">Terms &amp; Conditions</a></li>
          </ul>
        </div><!--footer_company ends-->

        <!-- Location menu -->
        <div id="footer_location">
          <h1>Location</h1>
          <hr>
          <ul>
            <li><a href="contact_us.php"><img src="images/icons/footer_location_address.png" alt="address">431 Main St.<br />Winnipeg<br />R2R0D0</a></li>
            <li><a href="contact_us.php"><img src="images/icons/footer_location_phone.png" alt="phone">+1 (204) 000-1122</a></li>
            <li><a href="contact_us.php"><img src="images/icons/footer_location_email.png" alt="email">sports_buy@gmail.ca</a></li>
          </ul>
        </div><!-- footer_location ends-->

        <!-- Customer Service menu -->
        <div id="footer_customer_service">
          <h1>Services</h1>
          <hr>
          <ul>
            <li><a href="faq.php">FAQ</a></li>
            <li><a href="track_your_order.php">Track Your Order</a></li>
            <li><a href="delivery.php">Delivery &amp; Return</a></li>
            <li><a href="payment_methods.php">Payment Methods</a></li>
          </ul>
        </div><!-- footer_customer_service ends-->

        <!-- Stay Connected menu (socials icons) -->
        <div id="footer_stay_connected">
          <h1>Follow Us</h1>
          <hr>
          <ul>
            <li><a href="#"><img src="images/icons/footer_socials_facebook.png" alt="facebook" /></a></li>
            <li><a href="#"><img src="images/icons/footer_socials_instagram.png" alt="instagram" /></a></li>
            <li><a href="#"><img src="images/icons/footer_socials_pinterest.png" alt="printerest" /></a></li>
            <li><a href="#"><img src="images/icons/footer_socials_twitter.png" alt="twitter" /></a></li>
          </ul>
        </div><!-- footer_stay_connected ends-->
      
      </div><!-- Footer_up ends -->

      <!-- footer_bottom(bottom part of the footer) starts here -->
      <div id="footer_bottom">
        <p>Copyright &copy; Sports Buy 2018, Canada</p>
      </div><!-- Footerbottom ends -->

    </div><!-- Footer ends -->
    <!-- ###########################  END footer  #################################-->
    <!-- ############################################################################-->


  </div><!-- Wrapper ends -->
</body>
</html>
EOT;
?>