<?php

// There is no product available in our stock
if ($product['units_in_stock'] === "0") {
  $availability = '';
  // Do not show carAdd to customers
  $quantityInput = '<p style="color: #ff4c4c;">Sorry, the item is out of stock.</p>';
  $addCartButton = '';
  $checkOutButton = '';
} else {
  $availability = <<<EOT
            <p>Availability: <span id="availquantity">{$product['units_in_stock']}<span></p>
EOT;
  
  $quantityInput = <<<EOT
              <!--<label for="quantity">Quantity</label><br />-->
              <p>
                <input type="text"
                       id="quantity"
                       name="quantity"
                       value="1"
                       placeholder="Quantity" />
              </p>
EOT;
  
  $addCartButton = "              <button id='add_to_cart' type='button' data-id='$productid' class='addCart'>Add to cart</button>"; 
  $checkOutButton = "              <button id='check_out' type='button' data-id='$productid' class='checkout' onclick=\"window.location.href = 'index.php?p=checkout.php';\">Checkout</button>"; 
}

$picnamearr = "[\"images/product_full_images/{$product['full_image1']}\", \"images/product_full_images/{$product['full_image2']}\", \"images/product_full_images/{$product['full_image3']}\"]";

                
$container = <<<EOT



      <!-- ################  START product details ####################-->
      <!-- Detail starts here -->
      <div id="detail">
      
        <div class="text-danger" id="cartmsg"></div>

        <!-- detail_category starts here -->
        <div id="detail_category">
          <h1><span class="text_transform_uppercase"><strong>{$product['category_name']}</strong></span></h1>
        </div><!-- detail_category ends here -->


        <!-- detail_product starts here -->
        <div id="detail_product">
          
          <!-- detail_image starts here -->
          <div id="detail_image">
            <img id="img_to_flip" src="images/product_full_images/{$product['full_image1']}" alt="detail_product_image">
          </div> <!-- detail_image ends here -->
          
          <div id="empty_div">&nbsp;</div>
          
          <!-- detail_info starts here -->
          <div id="detail_info">
            <h1><span class="text_transform_uppercase"><strong>{$product['product_name']}</strong></span></h1>
            <p>Price: C$ {$product['price']}</p>
            $availability
            
            <!-- add to cart code -->
            <form id="add_to_cart_form" action="cart.php" method="post" novalidate="novalidate">
              <input type="hidden"
                     name="product_id" 
                     value="$productid" />
              $quantityInput       
              <div><br/></div>
              $addCartButton
              <div><br/></div>
              $checkOutButton
            </form>
            
          </div><!-- detail_info ends here -->
          
        </div><!-- detail_product ends here -->
        <div class="clear_both"></div>


        <!-- detail_description starts here -->
        <div id="detail_description">
          <h2><span class="text_transform_uppercase"><strong>Description</strong></span></h2>
          <p>$desc</p>
        </div><!-- detail_description ends here -->

      </div><!-- #detail ends -->
      
      <!-- ###############  END product details ####################-->



    
      <script>
        function updateAvailQuantity(productid) {
            $.ajax({
                url: "action.php",
                method: "POST",
                data: {getAvailQuantity:1, pid:productid},
                success: function(data){
                    $('#availquantity').html(data);
                    //console.log(data);
                }
            })
        }

        updateAvailQuantity($productid);
        
        $('body').delegate('.addCart','click',function(){
            var productid = $(this).attr('data-id');
            console.log($('#quantity').val());
            $.ajax({
                url: "action.php",
                method: "POST",
                data: {addCart:1,pid:productid,quantity:$('#quantity').val()},
                success: function(data){
                    $('#cartmsg').html(data);
                    // Update available Quantity in a product detail
                    updateAvailQuantity(productid);
                }
            })
        });

      (function() {     // function expression closure to contain variables
          var i = 0;
          var pics = $picnamearr;
          var el = document.getElementById('img_to_flip');  // el doesn't change
          function toggle() {
              el.src = pics[i];           // set the image
              i = (i + 1) % pics.length;  // update the counter
          }
          setInterval(toggle, 2000);
      })();      
      
      
      </script>


EOT;
?>