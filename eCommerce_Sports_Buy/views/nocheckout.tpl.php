<?php
$container = <<<EOT

        <div class="container-fluid">
            <div class="row" id='checkoutheader'>
                <div class="row"></div>
                <div>
                    <div class="panel panel-warning text-center">
                        <div class="panel-heading title">Cart Checkout</div>
                        <div class="panel-body"><div class="col-md-12 alert text-success" id="cartmsg"></div></div>
                        <div class="row" id='checkoutdetail'>
                            <div class="col-md-2"><b>Image</b></div>
                            <div class="col-md-2"><b>Name</b></div>
                            <div class="col-md-2"><b>Price</b></div>
                            <div class="col-md-2"><b>Quantity</b></div>
                            <div class="col-md-2"><b>Price in $</b></div>
                            <div class="col-md-2"><b>Action</b></div>
                        </div>
                        <br>
                        <div id='cartdetail'>
                        </div>
                        <div class="panel-footer">

                        </div>
                    </div>
                </div>
                    <button class='btn btn-success btn-lg pull-right' id='loginbtn' data-toggle="modal" data-target="#myModal">Please login to check out</button>

            </div>
        </div>
        

            
    <script>
      // Implement the cart checkout
      function cartcheckout(){
          $.ajax({
              url: 'action.php',
              method: 'POST',
              data: {cartcheckout:1},
              success: function(data){
                // Empty Cart is found?
                if (data.indexOf('<div') < 0) {
                  $('#cartdetail').hide();
                  $('#checkoutdetail').hide();
                  $('#paymentsection').hide();
                  
                  // Put message in the cart message
                  $('#cartmsg').html(data);
                  //console.log(data);
                } else {
                  $('#cartdetail').html(data);
                  
                  // Update subtotal and tax
                  amount = $('#amounttotal').html();
                  $('#subtotal').val(amount);
                  
                  i = parseInt(amount);
                  
                  amount = (i == NaN) ? 0 : i * 0.13;
                  
                  $('#tax').val(amount);
                }
              }
          })
      }
      
      cartcheckout();

      // Remove button handler
      $('body').delegate('.remove','click',function(e){
          e.preventDefault();
          var pid=$(this).attr('data-id');
          $.ajax({
              url: 'action.php',
              method: 'POST',
              data: {removefromcart:1,pid:pid},
              success: function(data){
                  $('#cartmsg').html(data);
                  cartcheckout();
              }
          })
      })
      
      // Update button handler
      $('body').delegate('.update','click',function(e){
          e.preventDefault();
          var pid=$(this).attr('data-id');
          var quantity=$('#qty-'+pid).val();
          // Is numeric quantity ?
          
          var result = quantity.match(/(^[0-9]+$)/);
          // Not a numeric ? forget it.
          if (!result) {
            $('#cartmsg').html('The quantity which you entered is not a number.');
            return;
          }
          
          $.ajax({
              url: 'action.php',
              method: 'POST',
              data: {updatecart:1,pid:pid,quantity:quantity},
              success: function(data){
                  $('#cartmsg').html(data);
                  cartcheckout();
              }
          })
      }) 
      
      // Login button handler
      $('body').delegate('#loginbtn','click',function(e){
          e.preventDefault();

          window.location.href = "login.php";
          return;
      })

    </script>
    
    
</body>
</html>
EOT;
?>