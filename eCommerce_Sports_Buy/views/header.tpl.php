<?php

if(!isset($_SESSION['logged_in'])) :
$utility_nav = <<< THAI
  <ul>
    <li><a href="login.php">Log In &nbsp;<span class="util_nav_pipe">|</span></a></li>
    <li><a href="register.php">Register &nbsp;<span class="util_nav_pipe">|</span></a></li>
    <li><a href="index.php?p=checkout.php"><img src="images/icons/header_utility_nav_cart.png" alt="cart" /></a></li>
  </ul>
THAI;
else :
$utility_nav = <<< THAI
  <ul>
    <li><a href="logout.php">Log Out &nbsp;<span class="util_nav_pipe">|</span></a></li>
    <li><a href="profile.php">Profile &nbsp;<span class="util_nav_pipe">|</span></a></li>
    <li><a href="index.php?p=checkout.php"><img src="images/icons/header_utility_nav_cart.png" alt="cart" /></a></li>
  </ul>
THAI;
endif;

$header = <<<EOT
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>$title</title>
  <link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="styles/bootstrap-datetimepicker.min.css"/>
  <link rel="stylesheet" type="text/css" href="styles/desktop.css">


  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- Bootstrap Date-Picker Plugin -->
  <script src="js/bootstrap-datetimepicker.js"></script>
  <style>
  $style
  </style>
</head>


<body>
  <div id="wrapper">


    <!-- ############################################################################-->
    <!-- ###########################  START header  #################################-->
    <div id="header">
      
      <div id="logo">
        <a href="index.php"><img src="images/logo/sportslogo.png" alt="Logo" /></a>
      </div><!-- logo div ends-->

      <div id="nav">
        <ul>
          <li><a href="index.php">Home</a></li>
          <li><a href="products.php">Products</a></li>
          <li><a href="about_us.php">About Us</a></li>
          <li><a href="contact_us.php">Contact Us</a></li>
        </ul>
      </div><!-- nav div ends-->


      <!-- ######### START hamburger menu (main nav menu for mobile devices) ########-->
      <!-- by Erik Terwann "pure css hamburger fold-out menu", no javascript-->
      <!-- https://codepen.io/erikterwan/pen/EVzeRP -->
      <nav><!--originally was <nav role="navigation">-->
        <div id="menuToggle">
          <input type="checkbox" /> <!--fake/hidden checkbox is used as a click reciever -->
          
          <!--lines of hamburger icon -->
          <span></span>
          <span></span>
          <span></span>
          
          <ul id="menu">
            <li><a href="index.php">Home</a></li>
            <li><a href="products.php">Products</a></li>
            <li><a href="about_us.php">About Us</a></li>
            <li><a href="contact_us.php">Contact Us</a></li>
          </ul>
        </div>
      </nav>
      <!-- ######### END hamburger menu ########-->


      <!-- search field -->
      <div id="search">
        <form action="products.php" 
              id="search_form"
              method="get" 
              novalidate="novalidate" 
              autocomplete="off">
              
          <p>
            <input type="text"
                 id="searchbox"
                 class="searchtext" 
                 name="keyword" 
                 maxlength="255" 
                 placeholder="Search" /><!-- name="keyword" is used for _GET['keyword'] -->
            <input id="search_button" type=image src="images/icons/header_search_magnifier.png" alt="search" />
          </p>
        </form>
      </div><!--END div.search -->
                  <!-- search field -->
                  <!--
                  <div id="search">
                    <p><input class="searchtext" type="text" name="search" placeholder="search">
                      <a href="#">&nbsp;<img src="images/icons/header_search_magnifier.png" alt="search"></a>
                    </p>
                  </div>--><!-- search div ends-->


      <!-- utility navigation menu -->
      <div id="utility_nav">
      $utility_nav
      </div><!-- utility div ends-->
    </div><!-- header ends -->
    <!-- ###########################  END header  #################################-->
    <!-- ############################################################################-->


    <div style="height: 10px"></div>
    
    <div id="container">

EOT;
?>