<?php
/**
 * Detail page
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Loading a validator class
use classes\utility\checker;

$validator = new checker();

$title="SportsBuy: Detail of Product";


// Load main modules which support for customer login
require(CONFIG_PATH.'helper.php');
require(MODEL_CONFIG_PATH.'database.php');
//require(VIETNAMTOUR_CONFIG_PATH.'login.cfg.php');
require(MODEL_CONFIG_PATH.'product.db.php');

if (isset($_GET['pid'])) {
  $productid = $_GET['pid'];
  // Is valid id?
  if (isnumeric($productid)) {
    // Get the tour info
    $product = retrieveProductInfo($productid);
    if (count($product)>0) {
    $unitsinstock = $product['units_in_stock'];
    
    // Deduct available units in stock if users already added their units in stock
    if (!empty($_SESSION["cartitem"])) {
        if (in_array($productid, array_keys($_SESSION["cartitem"]))) {
            if(empty($_SESSION["cartitem"][$productid]["units_in_stock"])) {
                $_SESSION["cartitem"][$productid]["units_in_stock"] = 0;
            }
            $unitsinstock -= $_SESSION["cartitem"][$productid]["units_in_stock"];
            $unitsinstock = adjustIncorrectUnitsInStock($unitsinstock);
        }
    }
      
    $desc = substr($product['description'], 0, 410);
    
    }
  }
}


require(VIEW_CONFIG_PATH.'detail.tpl.php');
require(VIEW_CONFIG_PATH.'detail.css.php');

require(VIEW_CONFIG_PATH.'view.tpl.php');

?>