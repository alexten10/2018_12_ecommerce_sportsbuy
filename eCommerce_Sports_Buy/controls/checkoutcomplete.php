<?php
/**
 * Checkout Completion of Cart
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

require(MODEL_CONFIG_PATH.'database.php');
require(MODEL_CONFIG_PATH.'product.db.php');

// Setting up for the title and activating the menu link
$title="SportsBuy: Check out completion.";

// Get auth_code and show it
$tranid = $_GET['auth_code'];

// Get the css for the treasure page
//require(VIEW_CONFIG_PATH.'checkout.css.tpl.php');
// Get the checkout page content
require(VIEW_CONFIG_PATH.'checkoutcomplete.tpl.php');

require(VIEW_CONFIG_PATH.'checkout.css.php');

require(VIEW_CONFIG_PATH.'view.tpl.php');
?>