<?php
/**
 * Checkout of Cart
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Load main modules which support for checkout 
require(CONFIG_PATH.'helper.php');
require(MODEL_CONFIG_PATH.'database.php');
require(MODEL_CONFIG_PATH.'product.db.php');

// Setting up for the title and activating the menu link
$title="SportsBuy: Check out and do payment.";

$jsprovinceparam = '';

// If customer is logged in
if (isset($_SESSION['customer_id'])) {
  $paymentinfo = retrievePaymentInfo($_SESSION['customer_id']);
  //dumpList($paymentinfo);
  
  // If customer has shipping province, use it for PST
  $jsprovinceparam = ", billing_province:'{$paymentinfo['billing_province']}'";
  
  // Get the checkout page content
  require(VIEW_CONFIG_PATH.'checkout.tpl.php');

} else {
  // Get the nocheckout page content
  require(VIEW_CONFIG_PATH.'nocheckout.tpl.php');

}

require(VIEW_CONFIG_PATH.'checkout.css.php');
require(VIEW_CONFIG_PATH.'view.tpl.php');
?>